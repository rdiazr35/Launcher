﻿using LicenciasGestion;
using NCQ_B;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Launcher
{
    public partial class frmLoginSeguro : Form
    {
         //Variables globales
        DataSet dsetConexion;
        string ArchivoXML;
        NCQ_B.AccesoDatos oAccesoDatos;
        string login = "";
        string cod_compania = "";
        string nom_compania = "";
        string version = "";
        string skin_sistema = "";
        //Para reportres
        string odbc = "";
        string usuarioBD = "";
        string contrasena = "";
        string contrasena_usuario = "";
        
        bool ok = false;
        
        public frmLoginSeguro(string pmodulo, string pversion, NCQ_B.AccesoDatos poAccesoDatos)
        {
            InitializeComponent();
            oAccesoDatos = poAccesoDatos;
           
            version = pversion;
            if (cargarXML())
            {
                DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(dsetConexion.Tables[1].Rows[0]["skin"].ToString());
                skin_sistema = dsetConexion.Tables[1].Rows[0]["skin"].ToString();
                //
                edtUsuario.Text = dsetConexion.Tables[1].Rows[0]["Usuario"].ToString();
                if (dsetConexion.Tables[1].Rows[0]["Llave"].ToString() != "")
                {
                    DataRow[] rows = dsetConexion.Tables[0].Select("Llave='" + dsetConexion.Tables[1].Rows[0]["Llave"].ToString() + "'");
                    if (rows.Length > 0)
                    {
                        cmbeCompania.EditValue = dsetConexion.Tables[1].Rows[0]["Llave"].ToString();
                        return;
                    }
                }
                if (dsetConexion.Tables[0].Rows.Count > 0)
                    cmbeCompania.EditValue = dsetConexion.Tables[0].Rows[0]["Llave"].ToString();
            }
            if (edtUsuario.Text != "")
            {
                edtPassword.Focus();
                edtPassword.SelectAll();
            }
        }
        //
        private void frmLogin_Load(object sender, EventArgs e)
        {
            if (edtUsuario.Text == "")
                edtUsuario.Select();
            else
                edtPassword.Select();
        }

        public void openRegistro()
        {
            string command = Directory.GetParent(Assembly.GetExecutingAssembly().Location) + "\\InstaladorLicencia.exe";

            Process process = new Process();
            process.StartInfo.FileName = command;
            process.Start();
        }
        
        //=========================================================================================
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            
                if (edtUsuario.Text == "")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No ha digitado el usuario.", "Error", 0, MessageBoxIcon.Error);
                    edtUsuario.Select();
                    return;
                }
                //
                if (cmbeCompania.EditValue == null)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No ha seleccionado la compañía.", "Error", 0, MessageBoxIcon.Error);
                    return;
                }
                //
                if (!conectar())
                {
                    edtPassword.Focus();
                    edtPassword.Text = "";
                    return;
                }

                NCQ_B.Seguridad oSeguridad = new NCQ_B.Seguridad(oAccesoDatos,edtUsuario.Text, edtPassword.Text);
                oSeguridad.Login();
                if (oSeguridad.IsError)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(oSeguridad.ErrorDescription, "Error", 0, MessageBoxIcon.Error);
                    edtPassword.SelectAll();
                    edtPassword.Focus();
                    if (oAccesoDatos.estado())
                        oAccesoDatos.desconectar();
                }
                else
                {
                    login = edtUsuario.Text;

                    string companiaSeleccionada = cmbeCompania.EditValue.ToString();
                    DataRow[] compania = dsetConexion.Tables[0].Select("Llave='" + companiaSeleccionada + "'");
                    cod_compania = compania[0]["Codigo"].ToString();
                    usuarioBD = compania[0]["Usuario"].ToString();
                    NCQ_B.Encriptador oEncriptador = new NCQ_B.Encriptador();
                    contrasena = oEncriptador.DesencriptarCadena(compania[0]["Password"].ToString());
                    odbc = compania[0]["ODBC"].ToString();
                    contrasena_usuario = edtPassword.Text;
                    //
                    NCQ_B.Compania oCompania = new NCQ_B.Compania(oAccesoDatos,login);
                    nom_compania = oCompania.obtenerCampania(compania[0]["Codigo"].ToString());
                    if (oCompania.IsError)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(oCompania.ErrorDescription, "Error", 0, MessageBoxIcon.Error);
                        edtPassword.SelectAll();
                        edtPassword.Focus();
                        if (oAccesoDatos.estado())
                            oAccesoDatos.desconectar();
                        return;
                    }
                    //
                    Licencia oLicencia = new Licencia(oAccesoDatos.Usuario, oAccesoDatos.Puerto, contrasena
                          , oAccesoDatos.Servidor, oAccesoDatos.Database, oAccesoDatos.Schema.Replace("\"","").Replace(".",""), "Launcher", edtUsuario.Text);
                     while (!oLicencia.validar().Equals("OK"))
                    {
                        this.WindowState =FormWindowState.Minimized;
                        if (oLicencia.IsError)
                        {
                            MessageBox.Show(oLicencia.ErrorDescripcion, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        frmLicencias owpfLicencias = new frmLicencias(oAccesoDatos.Usuario, oAccesoDatos.Puerto, contrasena
                          , oAccesoDatos.Servidor, oAccesoDatos.Database, oAccesoDatos.Schema.Replace("\"", "").Replace(".", ""), edtUsuario.Text);
                        owpfLicencias.ShowDialog();
                        if (owpfLicencias.cerrarApp)
                        {
                            this.Close();
                            Application.Exit();
                            Process.GetCurrentProcess().Kill();
                            break;
                        }
                        owpfLicencias.Close();
                        GC.Collect();
                    }
                     Program.dias = oLicencia.calcularDíasRestantes();
                        dsetConexion.Tables[1].Rows[0]["Usuario"] = edtUsuario.Text;
                        dsetConexion.Tables[1].Rows[0]["Compania"] = compania[0]["Codigo"].ToString();
                        dsetConexion.Tables[1].Rows[0]["Llave"] = cmbeCompania.EditValue.ToString();
                        Program.contrasena = edtPassword.Text;
                        Program.login = edtUsuario.Text;
                        dsetConexion.AcceptChanges();
                        escribirXML();
                        ok = true;
                        this.Close();
                   
                }
            
        }
        //
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //=========================================================================================
        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{Tab}");
                e.SuppressKeyPress = true;
            }
            //
            if (e.KeyCode == Keys.F10)
            {
                e.SuppressKeyPress = true;
                btnAceptar_Click(sender, e);
            }
            //
            if (e.KeyCode == Keys.Escape)
            {
                btnCancelar_Click(sender, e);
                e.SuppressKeyPress = true;
            }
        }

        //=========================================================================================
        private bool cargarXML()
        {
            ArchivoXML = Application.StartupPath+"\\INI.xml";
            try
            {
                dsetConexion = new DataSet();
                System.IO.FileStream fsReadXml = new System.IO.FileStream(ArchivoXML, System.IO.FileMode.Open);
                try
                {
                    dsetConexion.ReadXml(fsReadXml);
                    cmbeCompania.Properties.DataSource = dsetConexion.Tables[0];
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString());
                    ArchivoXML = "";
                }
                finally
                {
                    fsReadXml.Close();
                }
            }
            catch (Exception e)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(e.Message.ToString(), "Error", 0, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        //=========================================================================================
        private void escribirXML()
        {
            try
            {
                this.dsetConexion.WriteXml(Application.StartupPath + "\\INI.xml");
            }
            catch (Exception error)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(error.Message.ToString(),"Error",0,MessageBoxIcon.Error);
            }
        }

        //=========================================================================================
        private bool conectar()
        {
            NCQ_B.Encriptador oEncriptador = new NCQ_B.Encriptador();//Clase que encripta o desencripta un string

            try
            {
                //Procedemos a leer el tipo de conexion del archivo de configuracion, y segun ese tipo 
                //de conexion, procedemos a instanciar la clase de datos correspondiente, pasando por
                //parámetros la informacion de conexion almacenada en el archivo de configuracion.

                DataRow[] row;
                if (cmbeCompania.EditValue.ToString() != "")
                    row = dsetConexion.Tables[0].Select("Llave='" + cmbeCompania.EditValue.ToString() + "'");
                else return false;

                string schema = "";
                switch (row[0]["TipoConexion"].ToString())
                {                    
                    case "3":
                        if (row[0]["Schema"].ToString() != "")
                        {
                            schema = "\"" + row[0]["Schema"].ToString() + "\".";
                        }
                        oAccesoDatos = new NCQ_B.AccesoDatosPostgre(
                            row[0]["Usuario"].ToString(),
                            row[0]["Port"].ToString(),
                            oEncriptador.DesencriptarCadena(row[0]["Password"].ToString()),
                            row[0]["Server"].ToString(),
                            row[0]["DataBase"].ToString(),
                            schema);
                        break;
                    //Debemos establecer el default, porque obligatoriamente en alguno de todos los casos
                    //se debe instanciar la varible oAccesoDatos, de lo contrario el programa no compila

                }
                //Mensaje de error de conexión con BD.
                NCQ_B.Seguridad oSeguridad = new NCQ_B.Seguridad(oAccesoDatos, "", "");
                if (oAccesoDatos.estado() == false)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Imposible ingresar al sistema.\nEstado conexión a BD: " + oAccesoDatos.ErrorDescripcion + ".\nVerique la configuración de la conexión, o consulte con el administrador.", "Conexión a Base de datos", 0, MessageBoxIcon.Error);
                    return false;
                }
                /////
                if (oSeguridad.VerificarVersion(version))
                    return true;
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Imposible ingresar al sistema con la compañia " + cmbeCompania.Text + ".\n" + oSeguridad.ErrorDescription, "Conexión a Base de datos", 0, MessageBoxIcon.Error);
                    oAccesoDatos.desconectar();
                    return false;
                }
            }
            catch (Exception error)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(error.Message);
                return false;
            }
        }

        //=========================================================================================
        public NCQ_B.AccesoDatos OAccesoDatos
        {
            get { return oAccesoDatos; }
        }
        //
        public string Login
        {
            get { return login; }
        }
        //
        public string Cod_compania
        {
            get { return cod_compania; }
        }
        //
        public string Nom_compania
        {
            get { return nom_compania; }
        }
        //
        public string Skin_sistema
        {
            get { return skin_sistema; }
        }
        //
        public bool Ok
        {
            get { return ok; }
        }
        //
        public string UsuarioBD
        {
            get { return usuarioBD; }
        }
        //
        public string ContrasenaBD
        {
            get { return contrasena; }
        }
        //
        public string ContrasenaUsuario
        {
            get { return contrasena_usuario; }
        }
        //
        public string ODBC
        {
            get { return odbc; }
        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

    }//Finaliza la clase
}//Finaliza namespace
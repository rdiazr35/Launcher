﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using NCQ_B;
using System.Diagnostics;

namespace Launcher
{
    public partial class frmLauncherNuevo : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        Thread tOptions;
        AccesoDatos oAccesoDatos = null;
        MenuStrip MainMenu = new MenuStrip();
        Color colorPerfil3 = System.Drawing.ColorTranslator.FromHtml("#00AA00");
        Color colorPerfil2 = System.Drawing.ColorTranslator.FromHtml("#0081FC");
        Color colorPerfil1 = System.Drawing.ColorTranslator.FromHtml("#FF5B00");

        public frmLauncherNuevo(AccesoDatos poAccesoDatos)
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            InitializeComponent();

            this.oAccesoDatos = poAccesoDatos;
            if (Program.compania.Equals(""))
                this.lblCompania.Text = "Compañia de prieba";
            else
                this.lblCompania.Text = Program.nomCompania;
            this.lblScript.Text = "Versión:" + Program.version + " Script:" + Program.script;
            if (Program.dias < 10)
            {
                this.lblLicencias.Visible = true;
                this.lblLicencias.Text = "Días restantes de licencia:" + Program.dias;
            }
            else
            {
                this.lblLicencias.Visible = false;
                this.lblLicencias.Text = "Días restantes de licencia:0";
            }
            //this.CrearMenu();
            //
            this.crearModuloSecundario(Program.perfil);
            this.perfil(Program.perfil);
            //

            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            this.mmMenu.ForeColor = Color.White;
        }
        //
        private void frmLauncherNuevo_Load(object sender, EventArgs e)
        {
            this.Menu();
            this.ptBuscar.Focus();
            this.ptBuscar.Select();
        }
        //

        //====================================================================================================================
        #region Métodos miscelaneos
        //
        private void llamadasQupos(string pAcceso, string pPantalla = "0")
        {
            try
            {
                Accesos oAcceso;
                string acceso = "";
                acceso = pAcceso;

                if (acceso.Equals("VENTAS") || acceso.Equals("COMPRAS"))
                {
                    NCQ_B.Encriptador oEncriptador = new NCQ_B.Encriptador();
                    string pass = oEncriptador.DesencriptarCadena(Program.contrasena);
                    oAcceso = new Accesos(Program.login, Program.compania, Program.nomCompania, this.oAccesoDatos.TipoConexion, this.oAccesoDatos.Usuario,
                        this.oAccesoDatos.Constrasena, this.oAccesoDatos.Servidor, this.oAccesoDatos.Database, this.oAccesoDatos.Schema.Trim('.').Trim('"'),
                        this.oAccesoDatos.Puerto, "PostgreSQL", true);
                    oAcceso.addParam(pPantalla);
                    if (Program.perfil == "1")
                        oAcceso.addParam("F");
                    else if (Program.perfil == "2")
                        oAcceso.addParam("E");
                    else
                        oAcceso.addParam("L");

                    oAcceso.addParam(Program.contrasena);
                    oAcceso.extencionPantalla(pPantalla);

                }
                else
                {
                    NCQ_B.Encriptador oEncriptador = new NCQ_B.Encriptador();
                    string pass = oEncriptador.EncriptarCadena(oAccesoDatos.Constrasena);
                    oAcceso = new Accesos(Program.login, Program.compania, Program.nomCompania, this.oAccesoDatos.TipoConexion, this.oAccesoDatos.Usuario, pass,
                        this.oAccesoDatos.Servidor, this.oAccesoDatos.Database, this.oAccesoDatos.Schema.Trim('.').Trim('"'), this.oAccesoDatos.Puerto, "PostgreSQL");
                    oAcceso.addParam(pPantalla);
                    oAcceso.extencionPantalla(pPantalla);
                }
                if (oAcceso.exeCom(acceso))
                    oAcceso.Dispose();
                else
                {
                    MessageBox.Show("Error de acceso al modulo" + Environment.NewLine +
                        oAcceso.ErrorDescripcion, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    oAcceso.Dispose();
                }
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Error");

            }
            GC.Collect();
        }
        //
        private void crearModuloSecundario(string pPerfil)
        {
            //this.mmMenu = new System.Windows.Forms.MenuStrip();
            this.mmMenu.Items.Clear();
            this.mmMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);

            List<modulo> modulos;
            PerfilModulos oPerfilModulos = new PerfilModulos(pPerfil);
            modulos = oPerfilModulos.Modulos;
            ToolStripMenuItem submenuPrimario = new System.Windows.Forms.ToolStripMenuItem();
            ToolStripMenuItem submenuSecundario = new System.Windows.Forms.ToolStripMenuItem();

            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^a-zA-Z0-9 ]");

            foreach (modulo mod in modulos)
            {
                ToolStripMenuItem meNU = new System.Windows.Forms.ToolStripMenuItem();
                meNU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
                submenuPrimario.ForeColor = Color.Black;
                submenuPrimario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular);

                ToolStripMenuItem meNU2 = new System.Windows.Forms.ToolStripMenuItem();
                meNU2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular);

                meNU.Text = mod.Nombre.ToString();
                //meNU.ForeColor = Color.FromArgb(240, 240, 240);


                string pQuery = "*" + mod.Nombre.ToString();

                string modulo = "";
                string nombre_modulo = "";
                int tabI = 17;

                PantallaRapida oPantallaRapida = new PantallaRapida(pPerfil);
                List<Pantalla> seleccion;

                reg = new System.Text.RegularExpressions.Regex("[^a-zA-Z0-9 ]");
                seleccion = oPantallaRapida.lPantallas.FindAll(x =>
                    reg.Replace(x.NombreModulo.Normalize(NormalizationForm.FormD), "").ToUpper().StartsWith(reg.Replace(
                    pQuery.Normalize(NormalizationForm.FormD), "").TrimStart().ToUpper())
                    ||
                    x.NombreModulo.ToUpper().StartsWith(pQuery.ToUpper().TrimStart()));

                foreach (Pantalla ptl in seleccion)
                {
                    if (modulo != ptl.NombreModulo)
                    {
                        modulo = ptl.NombreModulo;
                        nombre_modulo = ptl.NombreModulo;
                        submenuPrimario = new System.Windows.Forms.ToolStripMenuItem();
                        submenuPrimario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular);
                        submenuPrimario.ForeColor = Color.Black;

                        String input = ptl.NombreModulo;
                        var split = input.Split(new Char[] { '-' });
                        string v = "";
                        if (split.Count() > 1)
                        {
                            v = split[1].ToString();
                            split = v.Split(new Char[] { ' ' });
                            input = split[1].ToString();
                        }

                        submenuPrimario.Text = input;


                        string n = ptl.NombreModulo.ToString();
                        foreach (Pantalla ptl2 in seleccion)
                        {
                            string m = ptl2.NombreModulo.ToString();

                            if (n == m)
                            {
                                ToolStripItem open = new System.Windows.Forms.ToolStripMenuItem();
                                open.Text = ptl2.Nombre;
                                open.Click += new EventHandler(MenuItemClickHandler);
                                submenuPrimario.DropDownItems.Add(open);
                            }
                        }
                        //submenuPrimario.DataContext = ptl.NombreModulo;
                        meNU.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
                            submenuPrimario});

                    }
                }
                this.mmMenu.Items.Add(meNU);
            }
        }
        //
        private void perfil(string pPerfil)
        {
            switch (pPerfil)
            {
                case "1":
                    this.pnlHeader.BackColor = colorPerfil1;
                    this.pnlLogo.BackColor = colorPerfil1;
                    this.pnlLogo2.BackColor = colorPerfil1;
                    this.mmMenu.BackColor = colorPerfil1;
                    this.pnlLogo.Image = Properties.Resources.Qupos;
                    break;
                case "2":
                    this.pnlHeader.BackColor = colorPerfil2;
                    this.pnlLogo.BackColor = colorPerfil2;
                    this.pnlLogo2.BackColor = colorPerfil2;
                    this.mmMenu.BackColor = colorPerfil2;
                    this.pnlLogo.Image = Properties.Resources.QuposERP;
                    break;
                case "3":
                    this.pnlHeader.BackColor = colorPerfil3;
                    this.pnlLogo.BackColor = colorPerfil3;
                    this.pnlLogo2.BackColor = colorPerfil3;
                    this.mmMenu.BackColor = colorPerfil3;
                    this.pnlLogo.Image = Properties.Resources.QuposLite;
                    break;
            }
        }
        //
        private void Menu()
        {
            this.pnlMenu.Visible = false;

            List<MenuSlider> menuPrincipal = new List<MenuSlider>();
            List<MenuSlider> menuConsultas = new List<MenuSlider>();
            //
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Ventas", "1"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Compras", "2"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Inventario", "3"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Cuentas por pagar", "4"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Administración", "5"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Bancos", "6"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Comisiones", "7"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Contabilidad", "8"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Gastos", "9"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Recursos humanos", "10"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Rutas", "11"));


            for (int i = 0; i < 1000; i++)
            {
                menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Rutas", "11"));
            }
            //
            menuConsultas.Add(FlyweightItemMenu.MakeItemMenu("Preferencias", "F3"));
            menuConsultas.Add(FlyweightItemMenu.MakeItemMenu("Soporte", "F1"));
            //
            this.CargaMenu(menuPrincipal, menuConsultas);
        }
        //
        private void CargaMenu(List<MenuSlider> menuPrincipal, List<MenuSlider> menuConsultas)
        {
            this.tOptions = new Thread(new ThreadStart(() =>
            {

                this.Invoke((MethodInvoker)delegate
                {
                    this.Refresh();
                });
                //
                this.pnlMenu.Invoke((MethodInvoker)delegate
                {
                    this.pnlMenu.Controls.AddRange(menuPrincipal.ToArray<Control>());
                });
                //
                foreach (MenuSlider m in menuConsultas)
                {
                    this.pnlConsultas.Invoke((MethodInvoker)delegate
                    {
                        this.pnlConsultas.Controls.Add(m);
                    });
                }
                //
                this.Invoke((MethodInvoker)delegate
                {
                    this.lblEspere.Visible = false;
                    this.pnlMenu.Visible = true;
                });
            }));
            this.tOptions.Start();
        }
        //
        #endregion

        //====================================================================================================================
        #region Métodos de botones
        //
        private void lblMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //
        private void lblCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //
        private void lblMaximizar_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }
        //
        private void pnlmenu_DoubleClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }
        //
        private void logopanel_DoubleClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }
        //
        private void plnHeader_MouseClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }
        //
        private void btnActualizador_Click(object sender, EventArgs e)
        {
            Accesos oAccesos = new Accesos();
            oAccesos.exeCom("RunUpdater");
        }
        //
        private void btnAnydesk_Click(object sender, EventArgs e)
        {
            Accesos oAccesos = new Accesos();
            oAccesos.exeCom("AnyDesk");
        }
        //
        private void btnDiagnostico_Click(object sender, EventArgs e)
        {
            Accesos oAccesos = new Accesos();
            oAccesos.exeCom("Diagnóstico Qupos");
        }
        //
        private void ptBuscar_Click(object sender, EventArgs e)
        {
            if (!this.edtSearch.EditValue.ToString().Equals(""))
                this.ptBuscar.Image = Properties.Resources.ic_highlight_off_white_24dp1;
            else
                this.ptBuscar.Image = Properties.Resources.ic_search_white_24dp2;
        }
        //
        #endregion

        //====================================================================================================================
        #region Métodos componentes
        //
        private void frmLauncherNuevo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        //
        private void lblCerrar_MouseHover(object sender, EventArgs e)
        {
            this.lblCerrar.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMinimizar_MouseHover(object sender, EventArgs e)
        {
            this.lblMinimizar.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMinimizar_MouseLeave(object sender, EventArgs e)
        {
            this.lblMinimizar.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void lblCerrar_MouseLeave(object sender, EventArgs e)
        {
            this.lblCerrar.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void lblMaximizar_MouseHover(object sender, EventArgs e)
        {
            this.lblMaximizar.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMaximizar_MouseLeave(object sender, EventArgs e)
        {
            this.lblMaximizar.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void pnlMenu_ControlAdded(object sender, ControlEventArgs e)
        {
            e.Control.BringToFront();
        }
        //
        private void MenuItemClickHandler(object sender, EventArgs e)
        {
            ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;
            ToolStripMenuItem t = (ToolStripMenuItem)sender;
            try
            {
                PantallaRapida oPantallaRapida = new PantallaRapida(Program.perfil);

                Pantalla seleccion = oPantallaRapida.lPantallas.Find(
                    x => x.Nombre == t.Text.ToString());
                //seleccion.Modulo, seleccion.Acceso
                llamadasQupos(seleccion.Modulo, seleccion.Acceso);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }
        //
        private void btnBotones_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }
        //
        private void btnBotones_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
        //
        private void ptBuscar_MouseHover(object sender, EventArgs e)
        {
            if (!this.edtSearch.EditValue.ToString().Equals(""))
                this.Cursor = Cursors.Hand;
            else
                this.Cursor = Cursors.Default;

        }
        //
        private void ptBuscar_MouseLeave(object sender, EventArgs e)
        {
            if (!this.edtSearch.EditValue.ToString().Equals(""))
                this.Cursor = Cursors.Hand;
            else
                this.Cursor = Cursors.Default;
        }
        //
        private void edtSearch_TextChanged(object sender, EventArgs e)
        {
            if (!this.edtSearch.EditValue.ToString().Equals(""))
                this.ptBuscar.Image = Properties.Resources.ic_highlight_off_white_24dp1;
            else
                this.ptBuscar.Image = Properties.Resources.ic_search_white_24dp2;
        }
        //
        #endregion

    }//Finaliza clase
}//Finaliza manespace
﻿namespace Launcher
{
    partial class frmLogin
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlGeneral = new System.Windows.Forms.Panel();
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.ptMinimizar = new DevExpress.XtraEditors.PictureEdit();
            this.ptMaximizar = new DevExpress.XtraEditors.PictureEdit();
            this.ptClose = new DevExpress.XtraEditors.PictureEdit();
            this.pbFondo = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlGeneral.SuspendLayout();
            this.pnlContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptMinimizar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptMaximizar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptClose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondo)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlGeneral
            // 
            this.pnlGeneral.Controls.Add(this.pnlContainer);
            this.pnlGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGeneral.Location = new System.Drawing.Point(0, 0);
            this.pnlGeneral.Name = "pnlGeneral";
            this.pnlGeneral.Size = new System.Drawing.Size(1024, 566);
            this.pnlGeneral.TabIndex = 5;
            // 
            // pnlContainer
            // 
            this.pnlContainer.BackColor = System.Drawing.Color.Black;
            this.pnlContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlContainer.Controls.Add(this.ptMinimizar);
            this.pnlContainer.Controls.Add(this.ptMaximizar);
            this.pnlContainer.Controls.Add(this.ptClose);
            this.pnlContainer.Controls.Add(this.pbFondo);
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(1024, 566);
            this.pnlContainer.TabIndex = 4;
            // 
            // ptMinimizar
            // 
            this.ptMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ptMinimizar.BackgroundImage = global::Launcher.Properties.Resources.maximize_window;
            this.ptMinimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ptMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptMinimizar.EditValue = global::Launcher.Properties.Resources.minimize_window;
            this.ptMinimizar.Location = new System.Drawing.Point(934, 5);
            this.ptMinimizar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.ptMinimizar.Name = "ptMinimizar";
            this.ptMinimizar.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ptMinimizar.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.ptMinimizar.Properties.Appearance.Options.UseBackColor = true;
            this.ptMinimizar.Properties.Appearance.Options.UseForeColor = true;
            this.ptMinimizar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ptMinimizar.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.ptMinimizar.Size = new System.Drawing.Size(24, 24);
            this.ptMinimizar.TabIndex = 12;
            this.ptMinimizar.Click += new System.EventHandler(this.ptMinimizar_Click);
            this.ptMinimizar.MouseLeave += new System.EventHandler(this.ptMinimizar_MouseLeave);
            this.ptMinimizar.MouseHover += new System.EventHandler(this.ptMinimizar_MouseHover);
            // 
            // ptMaximizar
            // 
            this.ptMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ptMaximizar.BackgroundImage = global::Launcher.Properties.Resources.maximize_window;
            this.ptMaximizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ptMaximizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptMaximizar.EditValue = global::Launcher.Properties.Resources.maximize_window;
            this.ptMaximizar.Location = new System.Drawing.Point(964, 5);
            this.ptMaximizar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.ptMaximizar.Name = "ptMaximizar";
            this.ptMaximizar.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ptMaximizar.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.ptMaximizar.Properties.Appearance.Options.UseBackColor = true;
            this.ptMaximizar.Properties.Appearance.Options.UseForeColor = true;
            this.ptMaximizar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ptMaximizar.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.ptMaximizar.Size = new System.Drawing.Size(24, 24);
            this.ptMaximizar.TabIndex = 11;
            this.ptMaximizar.Click += new System.EventHandler(this.ptMaximizar_Click);
            this.ptMaximizar.MouseLeave += new System.EventHandler(this.ptMaximizar_MouseLeave);
            this.ptMaximizar.MouseHover += new System.EventHandler(this.ptMaximizar_MouseHover);
            // 
            // ptClose
            // 
            this.ptClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ptClose.BackgroundImage = global::Launcher.Properties.Resources.close_windows;
            this.ptClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ptClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptClose.EditValue = global::Launcher.Properties.Resources.close_windows;
            this.ptClose.Location = new System.Drawing.Point(994, 5);
            this.ptClose.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.ptClose.Name = "ptClose";
            this.ptClose.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ptClose.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.ptClose.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.ptClose.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.ptClose.Properties.Appearance.Options.UseBackColor = true;
            this.ptClose.Properties.Appearance.Options.UseBorderColor = true;
            this.ptClose.Properties.Appearance.Options.UseForeColor = true;
            this.ptClose.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ptClose.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.Image;
            this.ptClose.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.ptClose.Size = new System.Drawing.Size(24, 24);
            this.ptClose.TabIndex = 10;
            this.ptClose.Click += new System.EventHandler(this.ptClose_Click);
            this.ptClose.MouseLeave += new System.EventHandler(this.ptClose_MouseLeave);
            this.ptClose.MouseHover += new System.EventHandler(this.ptClose_MouseHover);
            // 
            // pbFondo
            // 
            this.pbFondo.BackColor = System.Drawing.Color.Transparent;
            this.pbFondo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbFondo.Image = global::Launcher.Properties.Resources.imagen_busqueda_01;
            this.pbFondo.Location = new System.Drawing.Point(0, 0);
            this.pbFondo.Name = "pbFondo";
            this.pbFondo.Size = new System.Drawing.Size(1024, 566);
            this.pbFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFondo.TabIndex = 0;
            this.pbFondo.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.panel1.BackgroundImage = global::Launcher.Properties.Resources.banner;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 566);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 39);
            this.panel1.TabIndex = 7;
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1024, 605);
            this.Controls.Add(this.pnlGeneral);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "s";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.pnlGeneral.ResumeLayout(false);
            this.pnlContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptMinimizar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptMaximizar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptClose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlContainer;
        private System.Windows.Forms.Panel pnlGeneral;
        private System.Windows.Forms.PictureBox pbFondo;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.PictureEdit ptClose;
        private DevExpress.XtraEditors.PictureEdit ptMaximizar;
        private DevExpress.XtraEditors.PictureEdit ptMinimizar;
    }
}


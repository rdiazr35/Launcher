﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Launcher
{

    public class MenuSlider : Panel
    {

        protected String Number, Titulo;
        protected SolidBrush auxColor;
        protected Pen auxPen, penOriginal, penEnter;
        protected Rectangle rectangle;
        protected Color backColorOriginal, backColorEnter;
        protected SolidBrush brushOriginal, brushEnter;
        protected Font fontTitulo, fontNumber;
        protected Point pointTitulo;
        //
        //
        protected Point pointNumber;

        public MenuSlider(string titulo, string number, ref Color pBackColorOriginal, ref Color pBackColorEnter,
            ref Size pSizePanel, ref SolidBrush pBrushOriginal, ref SolidBrush pBrushEnter, ref Rectangle pRectangle,
            ref Pen pPenOriginal, ref Pen pPenEnter, ref Font pFontTitulo, ref Font pFontNumber, ref Point pPointTitulo)
            : base()
        {
            this.Number = number;
            this.Titulo = titulo;
            this.backColorOriginal = pBackColorOriginal;
            this.backColorEnter = pBackColorEnter;
            this.brushOriginal = pBrushOriginal;
            this.brushEnter = pBrushEnter;
            this.rectangle = pRectangle;
            this.penOriginal = pPenOriginal;
            this.penEnter = pPenEnter;
            this.auxColor = brushOriginal;
            this.auxPen = penOriginal;
            //
            this.fontTitulo = pFontTitulo;
            this.fontNumber = pFontNumber;
            //
            this.pointTitulo = pPointTitulo;

            this.BackColor = pBackColorOriginal;
            this.Size = pSizePanel;

            this.MouseEnter += new EventHandler(item_MouseEnter);
            this.MouseLeave += new EventHandler(item_MouseLeave);
            this.Dock = DockStyle.Top;
        }
        //
        private void item_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = backColorEnter;
            this.Cursor = Cursors.Hand;
            //
            this.auxColor = brushEnter;
            this.auxPen = penEnter;
        }
        //
        private void item_MouseLeave(object sender, EventArgs e)
        {
            this.BackColor = backColorOriginal;
            this.Cursor = Cursors.Default;
            //
            this.auxColor = brushOriginal;
            this.auxPen = penOriginal;
        }
        //
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //Draw text and number
            g.DrawString(this.Titulo, this.fontTitulo ,
                auxColor, this.pointTitulo);

            int posNX =  rectangle.Location.X + ((Number.Length > 1) ? 1 : 6);
            int posNY = rectangle.Location.Y + 4;
            
            if (this.pointNumber == null)
                this.pointNumber = new Point(posNX , posNY);
            else
            {
                this.pointNumber.X = posNX;
                this.pointNumber.Y = posNY;
            }
                //
            g.DrawString(this.Number, this.fontNumber,
                    auxColor, this.pointNumber);
            //Draw circle
            g.DrawEllipse(auxPen, rectangle);
        }

    }//Finaliza clase

    public class FlyweightItemMenu
    {
        static Color colorOriginal = Color.White;
        static Color colorEnter = ColorTranslator.FromHtml("#E57E31");
        static Color backColorOriginal = ColorTranslator.FromHtml("#293541");
        static Color backColorEnter = ColorTranslator.FromHtml("#1F2B37");
        static Size sizePanel = new System.Drawing.Size(231, 35);
        static SolidBrush brushOriginal = new SolidBrush(colorOriginal);
        static SolidBrush brushEnter = new SolidBrush(colorEnter);
        static Rectangle rectangle = new Rectangle(15, 5, 25, 25);
        static Pen penOriginal = new Pen(colorOriginal, 2.5F);
        static Pen penEnter = new Pen(colorEnter, 2.5F);
        //
        static Font fontTitulo = new Font("Century Gothic", 12F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
        static Font fontNumber = new Font("Times New Roman", 12F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
        static Point pointTitulo = new Point(50, 7);
    
        public static MenuSlider MakeItemMenu(string pTitulo, string pNumero)
        {
            return new MenuSlider(pTitulo, pNumero, ref backColorOriginal, ref backColorEnter, ref sizePanel, 
                ref brushOriginal, ref brushEnter, ref rectangle, ref penOriginal, ref penEnter, ref fontTitulo, 
                ref fontNumber, ref pointTitulo);
        }
    }

    
}//Finaliza namespace
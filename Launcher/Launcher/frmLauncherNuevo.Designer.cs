﻿namespace Launcher
{
    partial class frmLauncherNuevo
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLauncherNuevo));
            this.logopanel = new System.Windows.Forms.Panel();
            this.pnlLogo2 = new System.Windows.Forms.PictureBox();
            this.pnlLogo = new System.Windows.Forms.PictureBox();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.mmMenu = new System.Windows.Forms.MenuStrip();
            this.pnBotones = new System.Windows.Forms.Panel();
            this.pn02 = new System.Windows.Forms.Panel();
            this.lblMaximizar = new DevExpress.XtraEditors.LabelControl();
            this.lblMinimizar = new DevExpress.XtraEditors.LabelControl();
            this.lblCerrar = new DevExpress.XtraEditors.LabelControl();
            this.pn01 = new System.Windows.Forms.Panel();
            this.pnlCabecera = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDiagnostico = new System.Windows.Forms.PictureBox();
            this.btnAnydesk = new System.Windows.Forms.PictureBox();
            this.btnActualizador = new System.Windows.Forms.PictureBox();
            this.lblScript = new System.Windows.Forms.Label();
            this.lblCompania = new System.Windows.Forms.Label();
            this.lblLicencias = new System.Windows.Forms.Label();
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.ptFondo = new System.Windows.Forms.PictureBox();
            this.pnlSlide = new System.Windows.Forms.Panel();
            this.pnlIzquierda = new System.Windows.Forms.Panel();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.lblEspere = new DevExpress.XtraEditors.LabelControl();
            this.pnlConsultas = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.edtSearch = new DevExpress.XtraEditors.TextEdit();
            this.ptBuscar = new System.Windows.Forms.PictureBox();
            this.pnlGeneral = new System.Windows.Forms.Panel();
            this.logopanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLogo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLogo)).BeginInit();
            this.pnlHeader.SuspendLayout();
            this.pnBotones.SuspendLayout();
            this.pn02.SuspendLayout();
            this.pnlCabecera.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDiagnostico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAnydesk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnActualizador)).BeginInit();
            this.pnlContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptFondo)).BeginInit();
            this.pnlSlide.SuspendLayout();
            this.pnlIzquierda.SuspendLayout();
            this.pnlMenu.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptBuscar)).BeginInit();
            this.pnlGeneral.SuspendLayout();
            this.SuspendLayout();
            // 
            // logopanel
            // 
            this.logopanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.logopanel.Controls.Add(this.pnlLogo2);
            this.logopanel.Controls.Add(this.pnlLogo);
            this.logopanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.logopanel.Location = new System.Drawing.Point(0, 0);
            this.logopanel.Name = "logopanel";
            this.logopanel.Size = new System.Drawing.Size(158, 60);
            this.logopanel.TabIndex = 2;
            this.logopanel.DoubleClick += new System.EventHandler(this.logopanel_DoubleClick);
            this.logopanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncherNuevo_MouseDown);
            // 
            // pnlLogo2
            // 
            this.pnlLogo2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.pnlLogo2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLogo2.Image = global::Launcher.Properties.Resources.nombre_QUPOS;
            this.pnlLogo2.ImageLocation = "";
            this.pnlLogo2.Location = new System.Drawing.Point(61, 0);
            this.pnlLogo2.Name = "pnlLogo2";
            this.pnlLogo2.Size = new System.Drawing.Size(97, 60);
            this.pnlLogo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pnlLogo2.TabIndex = 2;
            this.pnlLogo2.TabStop = false;
            this.pnlLogo2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncherNuevo_MouseDown);
            // 
            // pnlLogo
            // 
            this.pnlLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.pnlLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLogo.Image = global::Launcher.Properties.Resources.QuposERP;
            this.pnlLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlLogo.Name = "pnlLogo";
            this.pnlLogo.Size = new System.Drawing.Size(61, 60);
            this.pnlLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pnlLogo.TabIndex = 1;
            this.pnlLogo.TabStop = false;
            this.pnlLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncherNuevo_MouseDown);
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.pnlHeader.Controls.Add(this.mmMenu);
            this.pnlHeader.Controls.Add(this.pnBotones);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHeader.Location = new System.Drawing.Point(158, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(866, 60);
            this.pnlHeader.TabIndex = 1;
            this.pnlHeader.DoubleClick += new System.EventHandler(this.plnHeader_MouseClick);
            this.pnlHeader.MouseClick += new System.Windows.Forms.MouseEventHandler(this.plnHeader_MouseClick);
            this.pnlHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncherNuevo_MouseDown);
            // 
            // mmMenu
            // 
            this.mmMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.mmMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mmMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.mmMenu.Location = new System.Drawing.Point(0, 36);
            this.mmMenu.Name = "mmMenu";
            this.mmMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.mmMenu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.mmMenu.Size = new System.Drawing.Size(866, 24);
            this.mmMenu.TabIndex = 0;
            this.mmMenu.Text = "menuStrip1";
            // 
            // pnBotones
            // 
            this.pnBotones.BackColor = System.Drawing.Color.Transparent;
            this.pnBotones.Controls.Add(this.pn02);
            this.pnBotones.Controls.Add(this.pn01);
            this.pnBotones.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnBotones.ForeColor = System.Drawing.Color.White;
            this.pnBotones.Location = new System.Drawing.Point(0, 0);
            this.pnBotones.Margin = new System.Windows.Forms.Padding(0);
            this.pnBotones.Name = "pnBotones";
            this.pnBotones.Size = new System.Drawing.Size(866, 36);
            this.pnBotones.TabIndex = 12;
            // 
            // pn02
            // 
            this.pn02.BackColor = System.Drawing.Color.Transparent;
            this.pn02.Controls.Add(this.lblMaximizar);
            this.pn02.Controls.Add(this.lblMinimizar);
            this.pn02.Controls.Add(this.lblCerrar);
            this.pn02.Dock = System.Windows.Forms.DockStyle.Right;
            this.pn02.ForeColor = System.Drawing.Color.White;
            this.pn02.Location = new System.Drawing.Point(799, 0);
            this.pn02.Margin = new System.Windows.Forms.Padding(0);
            this.pn02.Name = "pn02";
            this.pn02.Size = new System.Drawing.Size(67, 36);
            this.pn02.TabIndex = 12;
            // 
            // lblMaximizar
            // 
            this.lblMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaximizar.Appearance.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaximizar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblMaximizar.Appearance.Options.UseFont = true;
            this.lblMaximizar.Appearance.Options.UseForeColor = true;
            this.lblMaximizar.Location = new System.Drawing.Point(26, 2);
            this.lblMaximizar.Name = "lblMaximizar";
            this.lblMaximizar.Size = new System.Drawing.Size(15, 30);
            this.lblMaximizar.TabIndex = 9;
            this.lblMaximizar.Text = "□";
            this.lblMaximizar.Click += new System.EventHandler(this.lblMaximizar_Click);
            this.lblMaximizar.MouseLeave += new System.EventHandler(this.lblMaximizar_MouseLeave);
            this.lblMaximizar.MouseHover += new System.EventHandler(this.lblMaximizar_MouseHover);
            // 
            // lblMinimizar
            // 
            this.lblMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMinimizar.Appearance.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinimizar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblMinimizar.Appearance.Options.UseFont = true;
            this.lblMinimizar.Appearance.Options.UseForeColor = true;
            this.lblMinimizar.Location = new System.Drawing.Point(10, 4);
            this.lblMinimizar.Name = "lblMinimizar";
            this.lblMinimizar.Size = new System.Drawing.Size(8, 30);
            this.lblMinimizar.TabIndex = 8;
            this.lblMinimizar.Text = "-";
            this.lblMinimizar.Click += new System.EventHandler(this.lblMinimizar_Click);
            this.lblMinimizar.MouseLeave += new System.EventHandler(this.lblMinimizar_MouseLeave);
            this.lblMinimizar.MouseHover += new System.EventHandler(this.lblMinimizar_MouseHover);
            // 
            // lblCerrar
            // 
            this.lblCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCerrar.Appearance.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCerrar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCerrar.Appearance.Options.UseFont = true;
            this.lblCerrar.Appearance.Options.UseForeColor = true;
            this.lblCerrar.Location = new System.Drawing.Point(49, 10);
            this.lblCerrar.Name = "lblCerrar";
            this.lblCerrar.Size = new System.Drawing.Size(9, 21);
            this.lblCerrar.TabIndex = 7;
            this.lblCerrar.Text = "X";
            this.lblCerrar.Click += new System.EventHandler(this.lblCerrar_Click);
            this.lblCerrar.MouseLeave += new System.EventHandler(this.lblCerrar_MouseLeave);
            this.lblCerrar.MouseHover += new System.EventHandler(this.lblCerrar_MouseHover);
            // 
            // pn01
            // 
            this.pn01.BackColor = System.Drawing.Color.Transparent;
            this.pn01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn01.ForeColor = System.Drawing.Color.White;
            this.pn01.Location = new System.Drawing.Point(0, 0);
            this.pn01.Margin = new System.Windows.Forms.Padding(0);
            this.pn01.Name = "pn01";
            this.pn01.Size = new System.Drawing.Size(866, 36);
            this.pn01.TabIndex = 11;
            this.pn01.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncherNuevo_MouseDown);
            // 
            // pnlCabecera
            // 
            this.pnlCabecera.Controls.Add(this.pnlHeader);
            this.pnlCabecera.Controls.Add(this.logopanel);
            this.pnlCabecera.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCabecera.Location = new System.Drawing.Point(0, 0);
            this.pnlCabecera.Name = "pnlCabecera";
            this.pnlCabecera.Size = new System.Drawing.Size(1024, 60);
            this.pnlCabecera.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.panel1.Controls.Add(this.btnDiagnostico);
            this.panel1.Controls.Add(this.btnAnydesk);
            this.panel1.Controls.Add(this.btnActualizador);
            this.panel1.Controls.Add(this.lblScript);
            this.panel1.Controls.Add(this.lblCompania);
            this.panel1.Controls.Add(this.lblLicencias);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 687);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 39);
            this.panel1.TabIndex = 7;
            // 
            // btnDiagnostico
            // 
            this.btnDiagnostico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiagnostico.BackColor = System.Drawing.Color.Transparent;
            this.btnDiagnostico.Image = global::Launcher.Properties.Resources.diagnostic;
            this.btnDiagnostico.Location = new System.Drawing.Point(987, 9);
            this.btnDiagnostico.Name = "btnDiagnostico";
            this.btnDiagnostico.Size = new System.Drawing.Size(21, 21);
            this.btnDiagnostico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnDiagnostico.TabIndex = 9;
            this.btnDiagnostico.TabStop = false;
            this.btnDiagnostico.Click += new System.EventHandler(this.btnDiagnostico_Click);
            this.btnDiagnostico.MouseLeave += new System.EventHandler(this.btnBotones_MouseLeave);
            this.btnDiagnostico.MouseHover += new System.EventHandler(this.btnBotones_MouseHover);
            // 
            // btnAnydesk
            // 
            this.btnAnydesk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnydesk.BackColor = System.Drawing.Color.Transparent;
            this.btnAnydesk.Image = global::Launcher.Properties.Resources.AnyDesk;
            this.btnAnydesk.Location = new System.Drawing.Point(960, 9);
            this.btnAnydesk.Name = "btnAnydesk";
            this.btnAnydesk.Size = new System.Drawing.Size(21, 21);
            this.btnAnydesk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnAnydesk.TabIndex = 8;
            this.btnAnydesk.TabStop = false;
            this.btnAnydesk.Click += new System.EventHandler(this.btnAnydesk_Click);
            this.btnAnydesk.MouseLeave += new System.EventHandler(this.btnBotones_MouseLeave);
            this.btnAnydesk.MouseHover += new System.EventHandler(this.btnBotones_MouseHover);
            // 
            // btnActualizador
            // 
            this.btnActualizador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualizador.BackColor = System.Drawing.Color.Transparent;
            this.btnActualizador.Image = global::Launcher.Properties.Resources.update;
            this.btnActualizador.Location = new System.Drawing.Point(933, 9);
            this.btnActualizador.Name = "btnActualizador";
            this.btnActualizador.Size = new System.Drawing.Size(21, 21);
            this.btnActualizador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnActualizador.TabIndex = 7;
            this.btnActualizador.TabStop = false;
            this.btnActualizador.Click += new System.EventHandler(this.btnActualizador_Click);
            this.btnActualizador.MouseLeave += new System.EventHandler(this.btnBotones_MouseLeave);
            this.btnActualizador.MouseHover += new System.EventHandler(this.btnBotones_MouseHover);
            // 
            // lblScript
            // 
            this.lblScript.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblScript.AutoSize = true;
            this.lblScript.BackColor = System.Drawing.Color.Transparent;
            this.lblScript.ForeColor = System.Drawing.Color.White;
            this.lblScript.Location = new System.Drawing.Point(12, 22);
            this.lblScript.Name = "lblScript";
            this.lblScript.Size = new System.Drawing.Size(37, 13);
            this.lblScript.TabIndex = 5;
            this.lblScript.Text = "Script:";
            this.lblScript.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblCompania
            // 
            this.lblCompania.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCompania.AutoSize = true;
            this.lblCompania.BackColor = System.Drawing.Color.Transparent;
            this.lblCompania.ForeColor = System.Drawing.Color.White;
            this.lblCompania.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCompania.Location = new System.Drawing.Point(12, 4);
            this.lblCompania.Name = "lblCompania";
            this.lblCompania.Size = new System.Drawing.Size(57, 13);
            this.lblCompania.TabIndex = 4;
            this.lblCompania.Text = "Compañia:";
            // 
            // lblLicencias
            // 
            this.lblLicencias.BackColor = System.Drawing.Color.Black;
            this.lblLicencias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLicencias.ForeColor = System.Drawing.Color.White;
            this.lblLicencias.Location = new System.Drawing.Point(0, 0);
            this.lblLicencias.Margin = new System.Windows.Forms.Padding(0);
            this.lblLicencias.Name = "lblLicencias";
            this.lblLicencias.Size = new System.Drawing.Size(1024, 39);
            this.lblLicencias.TabIndex = 6;
            this.lblLicencias.Text = "Días restantes de licencia:0";
            this.lblLicencias.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlContainer
            // 
            this.pnlContainer.BackColor = System.Drawing.Color.Transparent;
            this.pnlContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlContainer.Controls.Add(this.ptFondo);
            this.pnlContainer.Controls.Add(this.pnlSlide);
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(1024, 627);
            this.pnlContainer.TabIndex = 4;
            // 
            // ptFondo
            // 
            this.ptFondo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptFondo.Image = global::Launcher.Properties.Resources.imagen_busqueda_01;
            this.ptFondo.Location = new System.Drawing.Point(319, 0);
            this.ptFondo.Name = "ptFondo";
            this.ptFondo.Size = new System.Drawing.Size(705, 627);
            this.ptFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptFondo.TabIndex = 7;
            this.ptFondo.TabStop = false;
            // 
            // pnlSlide
            // 
            this.pnlSlide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pnlSlide.Controls.Add(this.pnlIzquierda);
            this.pnlSlide.Controls.Add(this.panel14);
            this.pnlSlide.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSlide.Location = new System.Drawing.Point(0, 0);
            this.pnlSlide.Name = "pnlSlide";
            this.pnlSlide.Size = new System.Drawing.Size(319, 627);
            this.pnlSlide.TabIndex = 8;
            // 
            // pnlIzquierda
            // 
            this.pnlIzquierda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pnlIzquierda.Controls.Add(this.pnlMenu);
            this.pnlIzquierda.Controls.Add(this.pnlConsultas);
            this.pnlIzquierda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlIzquierda.Location = new System.Drawing.Point(0, 52);
            this.pnlIzquierda.Name = "pnlIzquierda";
            this.pnlIzquierda.Size = new System.Drawing.Size(319, 575);
            this.pnlIzquierda.TabIndex = 18;
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pnlMenu.Controls.Add(this.lblEspere);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(319, 504);
            this.pnlMenu.TabIndex = 18;
            this.pnlMenu.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.pnlMenu_ControlAdded);
            // 
            // lblEspere
            // 
            this.lblEspere.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.lblEspere.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEspere.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblEspere.Appearance.Options.UseBackColor = true;
            this.lblEspere.Appearance.Options.UseFont = true;
            this.lblEspere.Appearance.Options.UseForeColor = true;
            this.lblEspere.Location = new System.Drawing.Point(94, 223);
            this.lblEspere.Name = "lblEspere";
            this.lblEspere.Size = new System.Drawing.Size(109, 23);
            this.lblEspere.TabIndex = 9;
            this.lblEspere.Text = "Cargando...";
            // 
            // pnlConsultas
            // 
            this.pnlConsultas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pnlConsultas.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlConsultas.Location = new System.Drawing.Point(0, 504);
            this.pnlConsultas.Name = "pnlConsultas";
            this.pnlConsultas.Size = new System.Drawing.Size(319, 71);
            this.pnlConsultas.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(43)))), ((int)(((byte)(55)))));
            this.panel14.Controls.Add(this.edtSearch);
            this.panel14.Controls.Add(this.ptBuscar);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(319, 52);
            this.panel14.TabIndex = 4;
            // 
            // edtSearch
            // 
            this.edtSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edtSearch.EditValue = "";
            this.edtSearch.Location = new System.Drawing.Point(42, 0);
            this.edtSearch.Name = "edtSearch";
            this.edtSearch.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(43)))), ((int)(((byte)(55)))));
            this.edtSearch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.edtSearch.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.edtSearch.Properties.Appearance.Options.UseBackColor = true;
            this.edtSearch.Properties.Appearance.Options.UseFont = true;
            this.edtSearch.Properties.Appearance.Options.UseForeColor = true;
            this.edtSearch.Properties.AutoHeight = false;
            this.edtSearch.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.edtSearch.Properties.NullValuePrompt = "Search";
            this.edtSearch.Properties.NullValuePromptShowForEmptyValue = true;
            this.edtSearch.Properties.UseParentBackground = true;
            this.edtSearch.Size = new System.Drawing.Size(277, 52);
            this.edtSearch.TabIndex = 0;
            this.edtSearch.TextChanged += new System.EventHandler(this.edtSearch_TextChanged);
            // 
            // ptBuscar
            // 
            this.ptBuscar.BackColor = System.Drawing.Color.Transparent;
            this.ptBuscar.Dock = System.Windows.Forms.DockStyle.Left;
            this.ptBuscar.Image = global::Launcher.Properties.Resources.ic_search_white_24dp2;
            this.ptBuscar.Location = new System.Drawing.Point(0, 0);
            this.ptBuscar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.ptBuscar.Name = "ptBuscar";
            this.ptBuscar.Size = new System.Drawing.Size(42, 52);
            this.ptBuscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ptBuscar.TabIndex = 2;
            this.ptBuscar.TabStop = false;
            this.ptBuscar.Click += new System.EventHandler(this.ptBuscar_Click);
            this.ptBuscar.MouseLeave += new System.EventHandler(this.ptBuscar_MouseLeave);
            this.ptBuscar.MouseHover += new System.EventHandler(this.ptBuscar_MouseHover);
            // 
            // pnlGeneral
            // 
            this.pnlGeneral.Controls.Add(this.pnlContainer);
            this.pnlGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGeneral.Location = new System.Drawing.Point(0, 60);
            this.pnlGeneral.Name = "pnlGeneral";
            this.pnlGeneral.Size = new System.Drawing.Size(1024, 627);
            this.pnlGeneral.TabIndex = 5;
            // 
            // frmLauncherNuevo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1024, 726);
            this.Controls.Add(this.pnlGeneral);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlCabecera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1024, 720);
            this.Name = "frmLauncherNuevo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmLauncherNuevo_Load);
            this.logopanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlLogo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLogo)).EndInit();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnBotones.ResumeLayout(false);
            this.pn02.ResumeLayout(false);
            this.pn02.PerformLayout();
            this.pnlCabecera.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDiagnostico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAnydesk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnActualizador)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptFondo)).EndInit();
            this.pnlSlide.ResumeLayout(false);
            this.pnlIzquierda.ResumeLayout(false);
            this.pnlMenu.ResumeLayout(false);
            this.pnlMenu.PerformLayout();
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edtSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptBuscar)).EndInit();
            this.pnlGeneral.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel logopanel;
        private System.Windows.Forms.PictureBox pnlLogo;
        private DevExpress.XtraEditors.LabelControl lblMinimizar;
        private DevExpress.XtraEditors.LabelControl lblCerrar;
        private DevExpress.XtraEditors.LabelControl lblMaximizar;
        private System.Windows.Forms.Panel pnlCabecera;
        private System.Windows.Forms.PictureBox pnlLogo2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCompania;
        private System.Windows.Forms.Label lblScript;
        private System.Windows.Forms.Label lblLicencias;
        private System.Windows.Forms.Panel pnlContainer;
        private System.Windows.Forms.PictureBox ptFondo;
        private System.Windows.Forms.Panel pnlGeneral;
        private System.Windows.Forms.Panel pnBotones;
        private System.Windows.Forms.MenuStrip mmMenu;
        private System.Windows.Forms.Panel pn01;
        private System.Windows.Forms.Panel pn02;
        private System.Windows.Forms.PictureBox btnActualizador;
        private System.Windows.Forms.PictureBox btnDiagnostico;
        private System.Windows.Forms.PictureBox btnAnydesk;
        private System.Windows.Forms.Panel pnlSlide;
        private System.Windows.Forms.Panel pnlIzquierda;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel pnlConsultas;
        private System.Windows.Forms.Panel panel14;
        private DevExpress.XtraEditors.LabelControl lblEspere;
        private DevExpress.XtraEditors.TextEdit edtSearch;
        private System.Windows.Forms.PictureBox ptBuscar;
    }
}


﻿namespace Launcher
{
    partial class frmLauncher
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSlide = new System.Windows.Forms.Panel();
            this.pnlIzquierda = new System.Windows.Forms.Panel();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlConsultas = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.logopanel = new System.Windows.Forms.Panel();
            this.ptLogo2 = new System.Windows.Forms.PictureBox();
            this.ptLogo = new System.Windows.Forms.PictureBox();
            this.plnHeader = new System.Windows.Forms.Panel();
            this.lblMaximizar = new DevExpress.XtraEditors.LabelControl();
            this.lblMinimizar = new DevExpress.XtraEditors.LabelControl();
            this.lblCerrar = new DevExpress.XtraEditors.LabelControl();
            this.pnlGeneral = new System.Windows.Forms.Panel();
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.pbFondo = new System.Windows.Forms.PictureBox();
            this.pnlCabecera = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblEspere = new DevExpress.XtraEditors.LabelControl();
            this.pnlSlide.SuspendLayout();
            this.pnlIzquierda.SuspendLayout();
            this.logopanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptLogo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptLogo)).BeginInit();
            this.plnHeader.SuspendLayout();
            this.pnlGeneral.SuspendLayout();
            this.pnlContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFondo)).BeginInit();
            this.pnlCabecera.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSlide
            // 
            this.pnlSlide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pnlSlide.Controls.Add(this.pnlIzquierda);
            this.pnlSlide.Controls.Add(this.panel14);
            this.pnlSlide.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSlide.Location = new System.Drawing.Point(0, 0);
            this.pnlSlide.Name = "pnlSlide";
            this.pnlSlide.Size = new System.Drawing.Size(231, 518);
            this.pnlSlide.TabIndex = 0;
            // 
            // pnlIzquierda
            // 
            this.pnlIzquierda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pnlIzquierda.Controls.Add(this.pnlMenu);
            this.pnlIzquierda.Controls.Add(this.pnlConsultas);
            this.pnlIzquierda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlIzquierda.Location = new System.Drawing.Point(0, 43);
            this.pnlIzquierda.Name = "pnlIzquierda";
            this.pnlIzquierda.Size = new System.Drawing.Size(231, 475);
            this.pnlIzquierda.TabIndex = 18;
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(231, 404);
            this.pnlMenu.TabIndex = 18;
            this.pnlMenu.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.pnlMenu_ControlAdded);
            // 
            // pnlConsultas
            // 
            this.pnlConsultas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.pnlConsultas.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlConsultas.Location = new System.Drawing.Point(0, 404);
            this.pnlConsultas.Name = "pnlConsultas";
            this.pnlConsultas.Size = new System.Drawing.Size(231, 71);
            this.pnlConsultas.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(43)))), ((int)(((byte)(55)))));
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(231, 43);
            this.panel14.TabIndex = 4;
            // 
            // logopanel
            // 
            this.logopanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.logopanel.Controls.Add(this.ptLogo2);
            this.logopanel.Controls.Add(this.ptLogo);
            this.logopanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.logopanel.Location = new System.Drawing.Point(0, 0);
            this.logopanel.Name = "logopanel";
            this.logopanel.Size = new System.Drawing.Size(231, 48);
            this.logopanel.TabIndex = 2;
            this.logopanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncher_MouseDown);
            // 
            // ptLogo2
            // 
            this.ptLogo2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.ptLogo2.Image = global::Launcher.Properties.Resources.nombre_QUPOS;
            this.ptLogo2.ImageLocation = "";
            this.ptLogo2.Location = new System.Drawing.Point(62, 6);
            this.ptLogo2.Name = "ptLogo2";
            this.ptLogo2.Size = new System.Drawing.Size(99, 36);
            this.ptLogo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptLogo2.TabIndex = 2;
            this.ptLogo2.TabStop = false;
            this.ptLogo2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncher_MouseDown);
            // 
            // ptLogo
            // 
            this.ptLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.ptLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.ptLogo.Image = global::Launcher.Properties.Resources._64x64;
            this.ptLogo.Location = new System.Drawing.Point(0, 0);
            this.ptLogo.Name = "ptLogo";
            this.ptLogo.Size = new System.Drawing.Size(61, 48);
            this.ptLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptLogo.TabIndex = 1;
            this.ptLogo.TabStop = false;
            this.ptLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncher_MouseDown);
            // 
            // plnHeader
            // 
            this.plnHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(135)))), ((int)(((byte)(255)))));
            this.plnHeader.Controls.Add(this.lblMaximizar);
            this.plnHeader.Controls.Add(this.lblMinimizar);
            this.plnHeader.Controls.Add(this.lblCerrar);
            this.plnHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plnHeader.Location = new System.Drawing.Point(231, 0);
            this.plnHeader.Name = "plnHeader";
            this.plnHeader.Size = new System.Drawing.Size(793, 48);
            this.plnHeader.TabIndex = 1;
            this.plnHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.plnHeader_Paint);
            this.plnHeader.DoubleClick += new System.EventHandler(this.plnHeader_MouseClick);
            this.plnHeader.MouseClick += new System.Windows.Forms.MouseEventHandler(this.plnHeader_MouseClick);
            this.plnHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLauncher_MouseDown);
            // 
            // lblMaximizar
            // 
            this.lblMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaximizar.Appearance.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaximizar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblMaximizar.Appearance.Options.UseFont = true;
            this.lblMaximizar.Appearance.Options.UseForeColor = true;
            this.lblMaximizar.Location = new System.Drawing.Point(743, 2);
            this.lblMaximizar.Name = "lblMaximizar";
            this.lblMaximizar.Size = new System.Drawing.Size(15, 30);
            this.lblMaximizar.TabIndex = 9;
            this.lblMaximizar.Text = "□";
            this.lblMaximizar.Click += new System.EventHandler(this.lblMaximizar_Click);
            this.lblMaximizar.MouseLeave += new System.EventHandler(this.lblMaximizar_MouseLeave);
            this.lblMaximizar.MouseHover += new System.EventHandler(this.lblMaximizar_MouseHover);
            // 
            // lblMinimizar
            // 
            this.lblMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMinimizar.Appearance.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinimizar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblMinimizar.Appearance.Options.UseFont = true;
            this.lblMinimizar.Appearance.Options.UseForeColor = true;
            this.lblMinimizar.Location = new System.Drawing.Point(727, 4);
            this.lblMinimizar.Name = "lblMinimizar";
            this.lblMinimizar.Size = new System.Drawing.Size(8, 30);
            this.lblMinimizar.TabIndex = 8;
            this.lblMinimizar.Text = "-";
            this.lblMinimizar.Click += new System.EventHandler(this.lblMinimizar_Click);
            this.lblMinimizar.MouseLeave += new System.EventHandler(this.lblMinimizar_MouseLeave);
            this.lblMinimizar.MouseHover += new System.EventHandler(this.lblMinimizar_MouseHover);
            // 
            // lblCerrar
            // 
            this.lblCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCerrar.Appearance.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCerrar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCerrar.Appearance.Options.UseFont = true;
            this.lblCerrar.Appearance.Options.UseForeColor = true;
            this.lblCerrar.Location = new System.Drawing.Point(762, 8);
            this.lblCerrar.Name = "lblCerrar";
            this.lblCerrar.Size = new System.Drawing.Size(13, 24);
            this.lblCerrar.TabIndex = 7;
            this.lblCerrar.Text = "X";
            this.lblCerrar.Click += new System.EventHandler(this.lblCerrar_Click);
            this.lblCerrar.MouseLeave += new System.EventHandler(this.lblCerrar_MouseLeave);
            this.lblCerrar.MouseHover += new System.EventHandler(this.lblCerrar_MouseHover);
            // 
            // pnlGeneral
            // 
            this.pnlGeneral.Controls.Add(this.pnlContainer);
            this.pnlGeneral.Controls.Add(this.pnlSlide);
            this.pnlGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGeneral.Location = new System.Drawing.Point(0, 48);
            this.pnlGeneral.Name = "pnlGeneral";
            this.pnlGeneral.Size = new System.Drawing.Size(1024, 518);
            this.pnlGeneral.TabIndex = 5;
            // 
            // pnlContainer
            // 
            this.pnlContainer.BackColor = System.Drawing.Color.Black;
            this.pnlContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlContainer.Controls.Add(this.pbFondo);
            this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContainer.Location = new System.Drawing.Point(231, 0);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(793, 518);
            this.pnlContainer.TabIndex = 4;
            // 
            // pbFondo
            // 
            this.pbFondo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbFondo.Image = global::Launcher.Properties.Resources.imagen_busqueda_01;
            this.pbFondo.Location = new System.Drawing.Point(0, 0);
            this.pbFondo.Name = "pbFondo";
            this.pbFondo.Size = new System.Drawing.Size(793, 518);
            this.pbFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFondo.TabIndex = 0;
            this.pbFondo.TabStop = false;
            // 
            // pnlCabecera
            // 
            this.pnlCabecera.Controls.Add(this.plnHeader);
            this.pnlCabecera.Controls.Add(this.logopanel);
            this.pnlCabecera.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCabecera.Location = new System.Drawing.Point(0, 0);
            this.pnlCabecera.Name = "pnlCabecera";
            this.pnlCabecera.Size = new System.Drawing.Size(1024, 48);
            this.pnlCabecera.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 566);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 39);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.labelControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(134, 39);
            this.panel2.TabIndex = 8;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(102, 17);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Desarrollado por:";
            // 
            // lblEspere
            // 
            this.lblEspere.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.lblEspere.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEspere.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblEspere.Appearance.Options.UseBackColor = true;
            this.lblEspere.Appearance.Options.UseFont = true;
            this.lblEspere.Appearance.Options.UseForeColor = true;
            this.lblEspere.Location = new System.Drawing.Point(58, 277);
            this.lblEspere.Name = "lblEspere";
            this.lblEspere.Size = new System.Drawing.Size(109, 23);
            this.lblEspere.TabIndex = 0;
            this.lblEspere.Text = "Cargando...";
            // 
            // frmLauncher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1024, 605);
            this.Controls.Add(this.lblEspere);
            this.Controls.Add(this.pnlGeneral);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlCabecera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "frmLauncher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "s";
            this.Load += new System.EventHandler(this.frmLauncher_Load);
            this.pnlSlide.ResumeLayout(false);
            this.pnlIzquierda.ResumeLayout(false);
            this.logopanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptLogo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptLogo)).EndInit();
            this.plnHeader.ResumeLayout(false);
            this.plnHeader.PerformLayout();
            this.pnlGeneral.ResumeLayout(false);
            this.pnlContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFondo)).EndInit();
            this.pnlCabecera.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlSlide;
        private System.Windows.Forms.Panel plnHeader;
        private System.Windows.Forms.Panel logopanel;
        private System.Windows.Forms.PictureBox ptLogo;
        private DevExpress.XtraEditors.LabelControl lblMinimizar;
        private DevExpress.XtraEditors.LabelControl lblCerrar;
        private System.Windows.Forms.Panel pnlConsultas;
        private System.Windows.Forms.Panel pnlContainer;
        private DevExpress.XtraEditors.LabelControl lblMaximizar;
        private System.Windows.Forms.Panel pnlGeneral;
        private System.Windows.Forms.Panel pnlCabecera;
        private System.Windows.Forms.PictureBox pbFondo;
        private System.Windows.Forms.PictureBox ptLogo2;
        private System.Windows.Forms.Panel pnlIzquierda;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl lblEspere;
    }
}


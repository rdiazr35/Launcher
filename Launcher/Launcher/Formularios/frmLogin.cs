﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace Launcher
{
    public partial class frmLogin : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        Thread tOptions;

        public frmLogin()
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            InitializeComponent();
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            
        }
        //
        private void frmLogin_Load(object sender, EventArgs e)
        {
            //PictureBox pic = new PictureBox();
            //pic.SizeMode = PictureBoxSizeMode.StretchImage;
            //pic.BackgroundImage = Properties.Resources.fondoAzulLock;
            //textEdit1.Controls.Add(pic);


        }

        //====================================================================================
        #region Métodos de botones
        //
        private void ptClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //
        private void ptMaximizar_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }
        //
        private void ptMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //
        #endregion

        //====================================================================================
        #region Métodos componentes
        //
        private void frmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        //
        private void plnHeader_MouseClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }
        //
        private void pnlMenu_ControlAdded(object sender, ControlEventArgs e)
        {
            e.Control.BringToFront();
        }

        
        #endregion

        private void ptClose_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void ptMinimizar_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void ptMaximizar_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void ptClose_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void ptMaximizar_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void ptMinimizar_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void frmLogin_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }

    }//Finaliza clase
}//Finaliza manespace
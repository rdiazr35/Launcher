﻿namespace Launcher
{
    partial class frmLogin
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.ptClose = new System.Windows.Forms.PictureBox();
            this.ptMaximizar = new System.Windows.Forms.PictureBox();
            this.ptMinimizar = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbCompania = new DevExpress.XtraEditors.LookUpEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.edtContrasena = new DevExpress.XtraEditors.TextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.edtUsuario = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ptClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptMaximizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptMinimizar)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCompania.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtContrasena.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtUsuario.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ptClose
            // 
            this.ptClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ptClose.BackColor = System.Drawing.Color.Transparent;
            this.ptClose.Image = global::Launcher.Properties.Resources.ic_close_black_24dp;
            this.ptClose.Location = new System.Drawing.Point(987, 12);
            this.ptClose.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.ptClose.Name = "ptClose";
            this.ptClose.Size = new System.Drawing.Size(25, 25);
            this.ptClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptClose.TabIndex = 0;
            this.ptClose.TabStop = false;
            this.ptClose.Click += new System.EventHandler(this.ptClose_Click);
            this.ptClose.MouseLeave += new System.EventHandler(this.ptClose_MouseLeave);
            this.ptClose.MouseHover += new System.EventHandler(this.ptClose_MouseHover);
            // 
            // ptMaximizar
            // 
            this.ptMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ptMaximizar.BackColor = System.Drawing.Color.Transparent;
            this.ptMaximizar.Image = global::Launcher.Properties.Resources.ic_check_box_outline_blank_black_24dp;
            this.ptMaximizar.Location = new System.Drawing.Point(960, 12);
            this.ptMaximizar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.ptMaximizar.Name = "ptMaximizar";
            this.ptMaximizar.Size = new System.Drawing.Size(25, 25);
            this.ptMaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptMaximizar.TabIndex = 1;
            this.ptMaximizar.TabStop = false;
            this.ptMaximizar.Click += new System.EventHandler(this.ptMaximizar_Click);
            this.ptMaximizar.MouseLeave += new System.EventHandler(this.ptMaximizar_MouseLeave);
            this.ptMaximizar.MouseHover += new System.EventHandler(this.ptMaximizar_MouseHover);
            // 
            // ptMinimizar
            // 
            this.ptMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ptMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.ptMinimizar.Image = global::Launcher.Properties.Resources.ic_remove_black_24dp;
            this.ptMinimizar.Location = new System.Drawing.Point(933, 12);
            this.ptMinimizar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.ptMinimizar.Name = "ptMinimizar";
            this.ptMinimizar.Size = new System.Drawing.Size(25, 25);
            this.ptMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptMinimizar.TabIndex = 2;
            this.ptMinimizar.TabStop = false;
            this.ptMinimizar.Click += new System.EventHandler(this.ptMinimizar_Click);
            this.ptMinimizar.MouseLeave += new System.EventHandler(this.ptMinimizar_MouseLeave);
            this.ptMinimizar.MouseHover += new System.EventHandler(this.ptMinimizar_MouseHover);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::Launcher.Properties.Resources.compani_erp;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.cmbCompania);
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(322, 389);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(381, 45);
            this.panel3.TabIndex = 10;
            // 
            // cmbCompania
            // 
            this.cmbCompania.EditValue = "";
            this.cmbCompania.Location = new System.Drawing.Point(55, 9);
            this.cmbCompania.Name = "cmbCompania";
            this.cmbCompania.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.cmbCompania.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.cmbCompania.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCompania.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.cmbCompania.Properties.Appearance.Options.UseBackColor = true;
            this.cmbCompania.Properties.Appearance.Options.UseFont = true;
            this.cmbCompania.Properties.Appearance.Options.UseForeColor = true;
            this.cmbCompania.Properties.AppearanceFocused.ForeColor = System.Drawing.Color.White;
            this.cmbCompania.Properties.AppearanceFocused.Options.UseForeColor = true;
            this.cmbCompania.Properties.AutoHeight = false;
            this.cmbCompania.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            serializableAppearanceObject1.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject1.BackColor2 = System.Drawing.Color.Transparent;
            serializableAppearanceObject1.BorderColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            serializableAppearanceObject1.ForeColor = System.Drawing.Color.White;
            serializableAppearanceObject1.Options.UseBackColor = true;
            serializableAppearanceObject1.Options.UseBorderColor = true;
            serializableAppearanceObject1.Options.UseFont = true;
            serializableAppearanceObject1.Options.UseForeColor = true;
            this.cmbCompania.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.cmbCompania.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Nombre", 200, "Compañía")});
            this.cmbCompania.Properties.DisplayMember = "Nombre";
            this.cmbCompania.Properties.ImmediatePopup = true;
            this.cmbCompania.Properties.LookAndFeel.SkinName = "Black";
            this.cmbCompania.Properties.NullText = "";
            this.cmbCompania.Properties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat;
            this.cmbCompania.Properties.PopupWidth = 400;
            this.cmbCompania.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.cmbCompania.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cmbCompania.Properties.ValueMember = "Llave";
            this.cmbCompania.Size = new System.Drawing.Size(303, 26);
            this.cmbCompania.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::Launcher.Properties.Resources.compani_erp;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.edtContrasena);
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(322, 338);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(381, 45);
            this.panel2.TabIndex = 9;
            // 
            // edtContrasena
            // 
            this.edtContrasena.EditValue = "Contraseña";
            this.edtContrasena.Location = new System.Drawing.Point(55, 3);
            this.edtContrasena.Name = "edtContrasena";
            this.edtContrasena.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.edtContrasena.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.edtContrasena.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.edtContrasena.Properties.Appearance.Options.UseBackColor = true;
            this.edtContrasena.Properties.Appearance.Options.UseFont = true;
            this.edtContrasena.Properties.Appearance.Options.UseForeColor = true;
            this.edtContrasena.Properties.AutoHeight = false;
            this.edtContrasena.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.edtContrasena.Properties.NullValuePrompt = "Contraseña";
            this.edtContrasena.Properties.NullValuePromptShowForEmptyValue = true;
            this.edtContrasena.Size = new System.Drawing.Size(303, 39);
            this.edtContrasena.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::Launcher.Properties.Resources.compani_erp;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.edtUsuario);
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(322, 287);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(381, 45);
            this.panel1.TabIndex = 8;
            // 
            // edtUsuario
            // 
            this.edtUsuario.EditValue = "Usuario";
            this.edtUsuario.Location = new System.Drawing.Point(55, 3);
            this.edtUsuario.Name = "edtUsuario";
            this.edtUsuario.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.edtUsuario.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.edtUsuario.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.edtUsuario.Properties.Appearance.Options.UseBackColor = true;
            this.edtUsuario.Properties.Appearance.Options.UseFont = true;
            this.edtUsuario.Properties.Appearance.Options.UseForeColor = true;
            this.edtUsuario.Properties.AutoHeight = false;
            this.edtUsuario.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.edtUsuario.Properties.NullValuePrompt = "Usuario";
            this.edtUsuario.Properties.NullValuePromptShowForEmptyValue = true;
            this.edtUsuario.Size = new System.Drawing.Size(303, 39);
            this.edtUsuario.TabIndex = 0;
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Launcher.Properties.Resources._11;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 720);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ptMinimizar);
            this.Controls.Add(this.ptMaximizar);
            this.Controls.Add(this.ptClose);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1024, 720);
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frmLogin_MouseDoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this.ptClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptMaximizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptMinimizar)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbCompania.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edtContrasena.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edtUsuario.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ptClose;
        private System.Windows.Forms.PictureBox ptMaximizar;
        private System.Windows.Forms.PictureBox ptMinimizar;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.LookUpEdit cmbCompania;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.TextEdit edtContrasena;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.TextEdit edtUsuario;

    }
}


﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using NCQ_B; 

namespace Launcher
{
    public class PerfilB
    {
        PerfilGestora oPerfilGestora;
        AccesoDatos oAccesoDatos;
        bool isError;
        string errorDescription;

        public PerfilB(AccesoDatos poAccesoDatos)
        {
            oAccesoDatos = poAccesoDatos;
            switch (poAccesoDatos.TipoConexion)
            {
                case "3":
                    oPerfilGestora = new PerfilGestoraP(poAccesoDatos);
                    break;
                default: oPerfilGestora = new PerfilGestoraP(poAccesoDatos);
                    break;
            }
        }
        public string getCompania()
        {
            try
            {
                Compania oCompania = new Compania(oAccesoDatos);

                string cliente = oCompania.optenerCliente();
                if (oCompania.IsError)
                {
                    return "";
                }
                return cliente;
            }
            catch
            {
                this.isError = oPerfilGestora.Error;
                this.errorDescription = oPerfilGestora.ErrorMsg;
                return "";
            }
        }
        public string cargarPerfil()
        {
            try{
            DataSet dsetDatos = new DataSet();
            string pHw = LicenciasGestion.Hardwares.getHardware();
            if (LicenciasGestion.Hardwares.IsError)
            {
                this.isError = true;
                this.errorDescription = LicenciasGestion.Hardwares.ErrorDescripcion;
                return null;
            }
            Compania oCompania = new Compania(oAccesoDatos);

                string cliente = oCompania.optenerCliente();
                if (oCompania.IsError)
                {
                    return "3";
                }
            dsetDatos= oPerfilGestora.cargarPerfil("QPOS",pHw);
            if (oPerfilGestora.Error)
            {
                this.isError = oPerfilGestora.Error;
                this.errorDescription = oPerfilGestora.ErrorMsg;
                return "3";
            }
           
            string vPerfil = "3";
            try
            {
                vPerfil = dsetDatos.Tables[0].Rows[0][0].ToString();
            }
            catch
            {
               vPerfil= "3";
            }
                
                

                vPerfil = eq.dcdo(vPerfil, pHw, cliente);
                if (vPerfil == "E")
                {
                    vPerfil = "2";
                   
                }
                else if (vPerfil == "F")
                {
                    vPerfil = "1";
                   
                }
                else
                {
                    vPerfil = "3";
                   
                }
                return vPerfil;
            }
            catch
            {
                this.isError = oPerfilGestora.Error;
                this.errorDescription = oPerfilGestora.ErrorMsg;
                return "3";
            }
        }
        public string cargarLic()
        {
            try
            {
                DataSet dsetDatos = new DataSet();
                string error = "";

                string pHw = LicenciasGestion.Hardwares.getHardware();
                if (error != "")
                {
                    this.isError = true;
                    this.errorDescription = error;
                    return null;
                }
                Compania oCompania = new Compania(oAccesoDatos);

                string cliente = oCompania.optenerCliente();
                if (oCompania.IsError)
                {
                    return "";
                }
                dsetDatos = oPerfilGestora.cargarLic("QPOS", pHw);
                if (oPerfilGestora.Error)
                {
                    this.isError = oPerfilGestora.Error;
                    this.errorDescription = oPerfilGestora.ErrorMsg;
                    return "";
                }

                string vLic = "";
                try
                {
                    vLic = dsetDatos.Tables[0].Rows[0][0].ToString();
                }
                catch
                {
                    vLic = "";
                }




                return vLic;
            }
            catch
            {
                this.isError = oPerfilGestora.Error;
                this.errorDescription = oPerfilGestora.ErrorMsg;
                return "";
            }
        }
        public DataSet cargarConfiguracion()
        {
            try
            {
                DataSet dsetDatos = new DataSet();
                
                string pHw = LicenciasGestion.Hardwares.getHardware();
                if (LicenciasGestion.Hardwares.IsError)
                {
                    this.isError = true;
                    this.errorDescription = LicenciasGestion.Hardwares.ErrorDescripcion;
                    return null;
                }
                Compania oCompania = new Compania(oAccesoDatos);

                string cliente = oCompania.optenerCliente();
                if (oCompania.IsError)
                {
                    return null;
                }
                dsetDatos = oPerfilGestora.cargarConfiguracion("QPOS", pHw);
                if (oPerfilGestora.Error)
                {
                    this.isError = oPerfilGestora.Error;
                    this.errorDescription = oPerfilGestora.ErrorMsg;
                    return null;
                }

              
                return dsetDatos;
            }
            catch
            {
                this.isError = oPerfilGestora.Error;
                this.errorDescription = oPerfilGestora.ErrorMsg;
                return null;
            }
        }
        public void modificarConfig(string pTray, string pModuloVentana, string pKeyCerrar)
        {
            try
            {
                string pHw = LicenciasGestion.Hardwares.getHardware();
                if (LicenciasGestion.Hardwares.IsError)
                {
                    this.isError = true;
                    this.errorDescription = LicenciasGestion.Hardwares.ErrorDescripcion;
                    return ;
                }
                Compania oCompania = new Compania(oAccesoDatos);

                string cliente = oCompania.optenerCliente();
                if (oCompania.IsError)
                {
                    return ;
                }
                 oPerfilGestora.modificarSystemtray("QPOS", pHw,pTray,  pModuloVentana, pKeyCerrar);
                if (oPerfilGestora.Error)
                {
                    this.isError = oPerfilGestora.Error;
                    this.errorDescription = oPerfilGestora.ErrorMsg;
                    return ;
                }


                return ;
            }
            catch
            {
                this.isError = oPerfilGestora.Error;
                this.errorDescription = oPerfilGestora.ErrorMsg;
                return ;
            }
        }
        public bool IsError
        {
            get { return isError; }
        }

        public string ErrorDescription
        {
            get { return errorDescription; }
        }
#region antiguaValidación
        //private string getHardware(ref string error)
        //{
        //    string hdd = string.Empty;
        //    try
        //    {
        //        ManagementClass partionsClass = new ManagementClass("Win32_LogicalDisk");
        //        ManagementObjectCollection partions = partionsClass.GetInstances();
        //        foreach (ManagementObject partion in partions)
        //        {
        //            hdd = Convert.ToString(partion["VolumeSerialNumber"]);
        //            if (hdd != string.Empty)
        //                break;
        //        }
        //        //ManagementScope ms = new ManagementScope("\\\\" + Environment.MachineName + "\\root\\cimv2");
        //        //ms.Connect();
        //        //ManagementObject wmiClass = new ManagementObject(ms, new ManagementPath("Win32_BaseBoard.Tag=\"Base Board\""), new ObjectGetOptions());
        //        //sn = wmiClass.Properties.Cast<PropertyData>().Where(p => p.Name == "SerialNumber").FirstOrDefault().Value.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        error = e.Message;
        //        hdd = "";
        //    }
        //    return hdd;
        //}
#endregion
    }
    class eq
    {
        #region Desencriptar

        // Método para desencriptar un texto encriptado.
        public static string dcdo(string code, string pBase, string pSaltValue)
        {
            return dcdo(code, pBase, pSaltValue, "MD5",
                1, "@1B2c3D4e5F6g7H8", 64);
        }

        // Método para desencriptar un texto encriptado    
        public static string dcdo(string code, string passBase,
            string saltValue, string hashAlgorithm, int passwordIterations,
            string initVector, int keySize)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] cipherTextBytes = Convert.FromBase64String(code);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passBase,
                saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged()
            {
                Mode = CipherMode.CBC
            };
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes,
                initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor,
                CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0,
                plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            string plainText = Encoding.UTF8.GetString(plainTextBytes, 0,
                decryptedByteCount);
            return plainText;
        }
        #endregion
    }
}
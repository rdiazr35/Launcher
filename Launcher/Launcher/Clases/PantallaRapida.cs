﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Launcher
{
    public class Pantalla
    {
        string nombre;
        string modulo;
        string moduloN;
        string acceso; 

        public Pantalla(string pNombre, string pModulo,string pNombreModulo, string pAcceso)
        {
            this.nombre = pNombre;
            this.acceso = pAcceso;
            this.modulo = pModulo;
            this.moduloN = pNombreModulo;
        }
        //
        public string Nombre
        {
            get { return nombre; }
        }
        //
        public string Acceso
        {
            get { return acceso; }
        }
        //
        public string Modulo
        {
            get { return modulo; }
        }
        //
        public string NombreModulo
        {
            get { return moduloN; }
        }
        //
    }
    //Finaliza clase Pantalla
    public class PantallaRapida
    {
        List<Pantalla> pantallas = new List<Pantalla>();

        public PantallaRapida(string pPerfil)
        {
            //================================================================================================
            #region administracion
            pantallas.Add(new Pantalla("Usuarios", "Administración del sistema", "Administración - Seguridad", "A1"));
            pantallas.Add(new Pantalla("Grupos", "Administración del sistema", "Administración - Seguridad", "A2"));
            pantallas.Add(new Pantalla("Compañía", "Administración del sistema", "Administración", "A3"));
            pantallas.Add(new Pantalla("Contraseña", "Administración del sistema", "Administración", "A4"));
            pantallas.Add(new Pantalla("Parámetros bancarios y contables", "Administración del sistema", "Administración", "A5"));
            pantallas.Add(new Pantalla("Respaldo", "Administración del sistema", "Administración", "A6"));
            pantallas.Add(new Pantalla("Mi estación", "Administración del sistema", "Administración", "A7"));
            #endregion

            //================================================================================================
            #region Ventas
            pantallas.Add(new Pantalla("Control de cajas", "ventas_utils", "Ventas - Procesos", "V5"));
            //pantallas.Add(new Pantalla("Definir caja", "VENTAS", "Ventas - Administración", "V60"));
            pantallas.Add(new Pantalla("Parámetros de ventas", "VENTAS", "Ventas - Administración", "V61"));
            pantallas.Add(new Pantalla("Facturación", "VENTAS", "Ventas - Procesos", "V7"));
            pantallas.Add(new Pantalla("Cajas", "ventas_utils", "Ventas - Mantenimientos", "V14"));
            pantallas.Add(new Pantalla("Cajeros", "ventas_utils", "Ventas - Mantenimientos", "V15"));
            pantallas.Add(new Pantalla("Clientes", "ventas_utils", "Ventas - Mantenimientos", "V17"));
            pantallas.Add(new Pantalla("Consecutivos de documentos", "ventas_utils", "Ventas - Mantenimientos", "V19"));
            pantallas.Add(new Pantalla("Grupos touchscreen", "ventas_utils", "Ventas - Mantenimientos", "V20"));
            pantallas.Add(new Pantalla("Impuestos", "ventas_utils", "Ventas - Mantenimientos", "V21"));
            pantallas.Add(new Pantalla("Perfil touchscreen", "ventas_utils", "Ventas - Mantenimientos", "V23"));
            pantallas.Add(new Pantalla("Tipo de cambio", "ventas_utils", "Ventas - Mantenimientos", "V27"));
            pantallas.Add(new Pantalla("Utilitario de costos", "ventas_utils", "Ventas - Procesos", "V12"));

            pantallas.Add(new Pantalla("Detalle de ventas", "ventas_utils", "Ventas - Consultas", "V37"));
            pantallas.Add(new Pantalla("Ventas por facturas", "ventas_utils", "Ventas - Consultas", "V48"));
            pantallas.Add(new Pantalla("D-151 ventas", "ventas_utils", "Ventas - Consultas", "V42"));
            pantallas.Add(new Pantalla("Estado de cuenta cxc", "ventas_utils", "Ventas - Consultas", "V52"));
            pantallas.Add(new Pantalla("Metas de venta", "ventas_utils", "Ventas - Procesos", "V59"));
            pantallas.Add(new Pantalla("Denominaciones ", "ventas_utils", "Ventas - Mantenimientos", "V67"));

            #endregion
            //
            if (pPerfil == "3")
            {
                pantallas.Add(new Pantalla("Artículos", "Inventario", "Ventas - Mantenimientos", "I4"));
                pantallas.Add(new Pantalla("Familias", "Inventario", "Ventas - Mantenimientos", "I7"));
                pantallas.Add(new Pantalla("Marcas", "Inventario", "Ventas - Mantenimientos", "I8"));
                pantallas.Add(new Pantalla("Diseño de etiquetas", "Inventario", "Ventas - Mantenimientos", "I19"));
                pantallas.Add(new Pantalla("Imprimir etiquetas", "Inventario", "Ventas - Mantenimientos", "I20"));
              
            }
            if (pPerfil == "1" || pPerfil == "2")
            {
                #region Ventas
                //pantallas.Add(new Pantalla("Administración de precios de venta ", "VENTAS", "Ventas - Procesos", "V1"));
                pantallas.Add(new Pantalla("Apartados", "ventas_utils", "Ventas - Procesos", "V2"));
                pantallas.Add(new Pantalla("Cierre contable ", "ventas_utils", "Ventas - Procesos", "V3"));
                pantallas.Add(new Pantalla("Consolidado de mercadería para despacho", "ventas_utils", "Ventas - Procesos", "V4"));
                pantallas.Add(new Pantalla("Cuentas por cobrar ", "VENTAS", "Ventas - Procesos", "V6"));
                pantallas.Add(new Pantalla("Liquidaciones", "ventas_utils", "Ventas - Procesos", "V8"));
                pantallas.Add(new Pantalla("Notas de crédito", "ventas_utils", "Ventas - Procesos", "V9"));
                pantallas.Add(new Pantalla("Preventas", "VENTAS", "Ventas - Procesos", "V10"));
                pantallas.Add(new Pantalla("Proformas", "VENTAS", "Ventas - Procesos", "V11"));
                pantallas.Add(new Pantalla("Ofertas", "ventas_utils", "Ventas - Mantenimientos", "V53"));
                pantallas.Add(new Pantalla("Artículos relacionados", "ventas_utils", "Ventas - Mantenimientos", "V13"));
                pantallas.Add(new Pantalla("Categorías de precio", "ventas_utils", "Ventas - Mantenimientos", "V16"));
                pantallas.Add(new Pantalla("Configuración de reportes", "ventas_utils", "Ventas - Mantenimientos", "V18"));
                pantallas.Add(new Pantalla("Notas predefinidas", "ventas_utils", "Ventas - Mantenimientos", "V22"));
                pantallas.Add(new Pantalla("Periodos de pago", "ventas_utils", "Ventas - Mantenimientos", "V24"));
                //pantallas.Add(new Pantalla("Promociones", "VENTAS", "Ventas - Mantenimientos", "V25"));
                pantallas.Add(new Pantalla("Recursos", "ventas_utils", "Ventas - Mantenimientos", "V26"));
                pantallas.Add(new Pantalla("Tipos de clientes", "ventas_utils", "Ventas - Mantenimientos", "V28"));
                pantallas.Add(new Pantalla("Tipos de notas de crédito", "ventas_utils", "Ventas - Mantenimientos", "V29"));
                pantallas.Add(new Pantalla("Zonas", "ventas_utils", "Ventas - Mantenimientos", "V30"));
                pantallas.Add(new Pantalla("Abonos a apartados", "ventas_utils", "Ventas - Consultas", "V31"));
                pantallas.Add(new Pantalla("Abonos/devoluciones por ingreso anticipado", "ventas_utils", "Ventas - Consultas", "V32"));
                pantallas.Add(new Pantalla("Antigüedad de saldos", "ventas_utils", "Ventas - Consultas", "V33"));
                pantallas.Add(new Pantalla("Cobertura de clientes", "ventas_utils", "Ventas - Consultas", "V34"));
                pantallas.Add(new Pantalla("Cobertura de clientes por familia (zona)", "ventas_utils", "Ventas - Consultas", "V35"));
                pantallas.Add(new Pantalla("Cuentas por cobrar", "ventas_utils", "Ventas - Consultas", "V36"));
            
                pantallas.Add(new Pantalla("Detalle de artículos relacionados", "ventas_utils", "Ventas - Consultas", "V38"));
                pantallas.Add(new Pantalla("Documentos(contabilidad)", "ventas_utils", "Ventas - Consultas", "V39"));
                pantallas.Add(new Pantalla("Ingresos anticipados", "ventas_utils", "Ventas - Procesos", "V40"));
                pantallas.Add(new Pantalla("Utilidad", "ventas_utils", "Ventas - Consultas", "V41"));
            
                pantallas.Add(new Pantalla("Documentos de pago", "ventas_utils", "Ventas - Consultas", "V43"));
                pantallas.Add(new Pantalla("Historial de cuentas por cobrar", "ventas_utils", "Ventas - Consultas", "V44"));
                pantallas.Add(new Pantalla("Historial de documentos en custodia", "ventas_utils", "Ventas - Consultas", "V45"));
                pantallas.Add(new Pantalla("Historial de ingresos anticipados", "ventas_utils", "Ventas - Consultas", "V46"));
                pantallas.Add(new Pantalla("Listas de precios", "ventas_utils", "Ventas - Consultas", "V47"));
            
                pantallas.Add(new Pantalla("Consolidado de mercadería por facturas", "ventas_utils", "Ventas - Consultas", "V49"));
                pantallas.Add(new Pantalla("Consolidado de mercadería por preventas", "ventas_utils", "Ventas - Consultas", "V50"));
                pantallas.Add(new Pantalla("Proforma/preventa", "ventas_utils", "Ventas - Consultas", "V51"));
            
                pantallas.Add(new Pantalla("Restricciones de clientes por artículos ", "ventas_utils", "Ventas - Mantenimientos", "V54"));
                pantallas.Add(new Pantalla("Cobertura de clientes por familia (recurso)", "ventas_utils", "Ventas - Consultas", "V55"));
                pantallas.Add(new Pantalla("Administración de precios de venta", "ventas_utils", "Ventas - Procesos", "V56"));
                //pantallas.Add(new Pantalla("Revisión de mercadería", "ventas_utils", "Ventas - Procesos", "V57"));
                pantallas.Add(new Pantalla("Consultas de precios por categoría", "ventas_utils", "Ventas - Consultas", "V58"));
           

                pantallas.Add(new Pantalla("Promociones", "ventas_utils", "Ventas - Mantenimientos", "V62"));
                pantallas.Add(new Pantalla("Artículos sin ventas ", "ventas_utils", "Ventas - Consultas", "V63"));

                pantallas.Add(new Pantalla("Antigüedad de saldos resumida", "ventas_utils", "Ventas - Consultas", "V64"));

                pantallas.Add(new Pantalla("Checkout", "ventas_utils", "Ventas - Procesos", "V65"));
                pantallas.Add(new Pantalla("Consulta errores checkout ", "ventas_utils", "Ventas - Consultas", "V66"));
                //Campos que pertenecias a Comisiones
                pantallas.Add(new Pantalla("Comisión", "Comisiones", "Ventas - Procesos", "C1"));
                pantallas.Add(new Pantalla("Perfiles de comisión", "Comisiones", "Ventas - Mantenimientos", "C2"));
                //pantallas.Add(new Pantalla("Recursos ", "NCQ", "Comisiones - Mantenimientos", "R4"));
                #endregion

                //================================================================================================
                #region Compras
                pantallas.Add(new Pantalla("Aplicar precios de compras en bloque", "COMPRAS", "Compras - Procesos", "CO1"));
                pantallas.Add(new Pantalla("Cierre contable", "compras_utils", "Compras - Procesos", "CO2"));
                pantallas.Add(new Pantalla("Devoluciones", "compras_utils", "Compras - Procesos", "CO3"));
                pantallas.Add(new Pantalla("Finalizar compras", "COMPRAS", "Compras - Procesos", "CO4"));
                pantallas.Add(new Pantalla("Notas de crédito ", "compras_utils", "Compras - Procesos", "CO5"));
                pantallas.Add(new Pantalla("Órdenes de compra", "compras_utils", "Compras - Procesos", "CO6"));
                pantallas.Add(new Pantalla("Registro de compras", "COMPRAS", "Compras - Procesos", "CO7"));
                pantallas.Add(new Pantalla("Registro de compras sin detalle", "compras_utils", "Compras - Procesos", "CO8"));
                pantallas.Add(new Pantalla("Utilitario de balanzas", "Romanas", "Compras - Procesos", "CO9"));

                pantallas.Add(new Pantalla("Balanzas", "compras_utils", "Compras - Mantenimientos", "CO10"));
                pantallas.Add(new Pantalla("Formas de pago", "compras_utils", "Compras - Mantenimientos", "CO11"));
                pantallas.Add(new Pantalla("Impuestos ", "compras_utils", "Compras - Mantenimientos", "CO12"));
                pantallas.Add(new Pantalla("Proveedores   ", "compras_utils", "Compras - Mantenimientos", "CO13"));
                pantallas.Add(new Pantalla("Tipo de cambio", "compras_utils", "Compras - Mantenimientos", "CO14"));

                pantallas.Add(new Pantalla("Compras por facturas", "compras_utils", "Compras - Consultas", "CO15"));
                pantallas.Add(new Pantalla("D-151 compras", "compras_utils", "Compras - Consultas", "CO16"));
                pantallas.Add(new Pantalla("Detalle de compras ", "compras_utils", "Compras - Consultas", "CO17"));
                pantallas.Add(new Pantalla("Documentos(contabilidad)", "compras_utils", "Compras - Consultas", "CO18"));
                pantallas.Add(new Pantalla("Proveedor - artículo", "compras_utils", "Compras - Consultas", "CO19"));
                pantallas.Add(new Pantalla("Reporte de compras", "COMPRAS", "Compras - Consultas", "CO20"));
                pantallas.Add(new Pantalla("Parámetros de compras", "COMPRAS", "Compras - Administración", "CO21"));
                
                #endregion

                //================================================================================================
                #region inventario
                pantallas.Add(new Pantalla("Movimiento de inventario", "Inventario", "Inventario - Procesos", "I1"));
                pantallas.Add(new Pantalla("Toma física", "Inventario", "Inventario - Procesos", "I2"));
                pantallas.Add(new Pantalla("Saldo de inventarios", "Inventario", "Inventario - Procesos", "I3"));

                pantallas.Add(new Pantalla("Artículos", "Inventario", "Inventario - Mantenimientos", "I4"));
                pantallas.Add(new Pantalla("Bodegas", "Inventario", "Inventario - Mantenimientos", "I5"));
                pantallas.Add(new Pantalla("Departamentos", "Inventario", "Inventario - Mantenimientos", "I6"));
                pantallas.Add(new Pantalla("Familias", "Inventario", "Inventario - Mantenimientos", "I7"));
                pantallas.Add(new Pantalla("Marcas", "Inventario", "Inventario - Mantenimientos", "I8"));
                pantallas.Add(new Pantalla("Tipos de movimientos", "Inventario", "Inventario - Mantenimientos", "I9"));
                pantallas.Add(new Pantalla("Transformaciones artículos", "Inventario", "Inventario - Mantenimientos", "I10"));
                pantallas.Add(new Pantalla("Unidades de medida", "Inventario", "Inventario - Mantenimientos", "I11"));


                pantallas.Add(new Pantalla("Detalle movimiento de inventario", "Inventario", "Inventario - Consultas", "I12"));
                pantallas.Add(new Pantalla("Detalle toma física", "Inventario", "Inventario - Consultas", "I13"));
                pantallas.Add(new Pantalla("Existencias", "Inventario", "Inventario - Consultas", "I14"));
                pantallas.Add(new Pantalla("Historial de existencias", "Inventario", "Inventario - Consultas", "I15"));
                pantallas.Add(new Pantalla("Rotación de inventario", "Inventario", "Inventario - Consultas", "I16"));
                pantallas.Add(new Pantalla("Historial de costo promedio", "Inventario", "Inventario - Consultas", "I17"));

                pantallas.Add(new Pantalla("Restricciones de clientes por artículos", "ventas_utils", "Inventario - Mantenimientos", "V54"));
                pantallas.Add(new Pantalla("Parámetros de inventario", "Inventario", "Inventario - Administración", "I18"));
                pantallas.Add(new Pantalla("Diseño de etiquetas", "Inventario", "Inventario - Etiquetas", "I19"));
                pantallas.Add(new Pantalla("Imprimir etiquetas", "Inventario", "Inventario - Etiquetas", "I20"));
                pantallas.Add(new Pantalla("Tipos de papel", "Inventario", "Inventario - Etiquetas", "I21"));

                pantallas.Add(new Pantalla("Categoría artículos", "Inventario", "Inventario - Mantenimientos", "I22"));
                pantallas.Add(new Pantalla("Sub categoría artículos", "Inventario", "Inventario - Mantenimientos", "I23"));
                pantallas.Add(new Pantalla("Tipo de artículo", "Inventario", "Inventario - Mantenimientos", "I24"));
                #endregion

                //================================================================================================
                #region cxp
                pantallas.Add(new Pantalla("Cargar compras", "CxP", "Cuentas por pagar - Procesos", "P1"));
                pantallas.Add(new Pantalla("Impresión cheques", "CxP", "Cuentas por pagar - Procesos", "P2"));
                pantallas.Add(new Pantalla("Movimientos de CxP", "CxP", "Cuentas por pagar - Procesos", "P3"));
                pantallas.Add(new Pantalla("Pagos anticipados", "CxP", "Cuentas por pagar - Procesos", "P4"));
                pantallas.Add(new Pantalla("Trámites de pago", "CxP", "Cuentas por pagar - Procesos", "P5"));

                pantallas.Add(new Pantalla("Cuentas bancarias", "CxP", "Cuentas por pagar - Mantenimientos", "P6"));
                pantallas.Add(new Pantalla("Entidades financieras", "CxP", "Cuentas por pagar - Mantenimientos", "P7"));
                pantallas.Add(new Pantalla("Formas de pago", "CxP", "Cuentas por pagar - Mantenimientos", "P8"));
                pantallas.Add(new Pantalla("Proveedores", "CxP", "Cuentas por pagar - Mantenimientos", "P9"));
                pantallas.Add(new Pantalla("Tipo de cambio  ", "CxP", "Cuentas por pagar - Mantenimientos", "P10"));
                pantallas.Add(new Pantalla("Tipos de nota CxP", "CxP", "Cuentas por pagar - Mantenimientos", "P11"));
                pantallas.Add(new Pantalla("Tipos de proveedor", "CxP", "Cuentas por pagar - Mantenimientos", "P12"));

                pantallas.Add(new Pantalla("Documentos de pago", "CxP", "Cuentas por pagar - Consultas", "P13"));
                pantallas.Add(new Pantalla("Estado de cuenta CxP", "CxP", "Cuentas por pagar - Consultas", "P14"));
                pantallas.Add(new Pantalla("Historial cuentas por pagar", "CxP", "Cuentas por pagar - Consultas", "P15"));
                pantallas.Add(new Pantalla("Historial de pagos anticipados", "CxP", "Cuentas por pagar - Consultas", "P16"));
                pantallas.Add(new Pantalla("Proyección de pagos", "CxP", "Cuentas por pagar - Consultas", "P17"));
                pantallas.Add(new Pantalla("Trámites/devoluciones por pagos", "CxP", "Cuentas por pagar - Consultas", "P18"));

                pantallas.Add(new Pantalla("Parámetros de cuentas por pagar", "CxP", "Cuentas por pagar - Administración", "P19"));

                #endregion

                //================================================================================================
                #region Rutas
                pantallas.Add(new Pantalla("Rutas ", "QPOS Móvil", "Rutas - Procesos", "R1"));

                pantallas.Add(new Pantalla("Dispositivos móviles", "QPOS Móvil", "Rutas - Mantenimientos", "R2"));
                pantallas.Add(new Pantalla("Rutas", "QPOS Móvil", "Rutas - Mantenimientos", "R3"));
                pantallas.Add(new Pantalla("Recursos  ", "NCQ", "Rutas - Mantenimientos", "R4"));

                pantallas.Add(new Pantalla("Efectividad de clientes", "QPOS Móvil", "Rutas - Consultas", "R5"));
                pantallas.Add(new Pantalla("Mercadería sin facturar", "QPOS Móvil", "Rutas - Consultas", "R6"));
                pantallas.Add(new Pantalla("Tiempos de venta", "QPOS Móvil", "Rutas - Consultas", "R7"));
                pantallas.Add(new Pantalla("Parámetros de rutas", "QPOS Móvil", "Rutas - Administracíon", "R8"));
                #endregion
            }
            if (pPerfil == "2")
            {
                //================================================================================================
                #region Gastos
                pantallas.Add(new Pantalla("Gastos", "Gastos", "Gastos - Procesos", "G1"));

                pantallas.Add(new Pantalla("Monedas", "Gastos", "Gastos - Mantenimientos", "G2"));
                pantallas.Add(new Pantalla("Categorías de tipo de cambio", "Gastos", "Gastos - Mantenimientos", "G3"));
                pantallas.Add(new Pantalla("Proveedores", "Gastos", "Gastos - Mantenimientos", "G4"));
                pantallas.Add(new Pantalla("Tipo de cambio", "Gastos", "Gastos - Mantenimientos", "G5"));
                pantallas.Add(new Pantalla("Tipo de gastos", "Gastos", "Gastos - Mantenimientos", "G6"));
                pantallas.Add(new Pantalla("Tipos de proveedor ", "Gastos", "Gastos - Mantenimientos", "G7"));

                pantallas.Add(new Pantalla("Consultas gastos (tabla dinámica)", "Gastos", "Gastos - Consultas", "G8"));
                pantallas.Add(new Pantalla("D-151 compras ", "Gastos", "Gastos - Consultas", "G9"));

                #endregion

                //================================================================================================
                #region RH
                pantallas.Add(new Pantalla("Autorización de horas extra y dobles", "RH", "Recursos humanos - Procesos", "RH1"));
                pantallas.Add(new Pantalla("Control de marcas", "RH", "Recursos humanos - Procesos", "RH2"));
                pantallas.Add(new Pantalla("Generar acciones de personal", "RH", "Recursos humanos - Procesos", "RH3"));
                pantallas.Add(new Pantalla("Horarios por empleado", "RH", "Recursos humanos - Procesos", "RH4"));
                pantallas.Add(new Pantalla("Pagos de nómina", "RH", "Recursos humanos - Procesos", "RH5"));
                pantallas.Add(new Pantalla("Reasignación de horas", "RH", "Recursos humanos - Procesos", "RH6"));
                pantallas.Add(new Pantalla("Registro de labores diarias", "RH", "Recursos humanos - Procesos", "RH7"));
                pantallas.Add(new Pantalla("Reporte banco", "RH", "Recursos humanos - Procesos", "RH8"));
                pantallas.Add(new Pantalla("Reporte CCSS", "RH", "Recursos humanos - Procesos", "RH9"));
                pantallas.Add(new Pantalla("Reporte INS", "RH", "Recursos humanos - Procesos", "RH10"));
                pantallas.Add(new Pantalla("Unificación de marcas", "RH", "Recursos humanos - Procesos", "RH11"));
                pantallas.Add(new Pantalla("Unificaciones en bloque", "RH", "Recursos humanos - Procesos", "RH12"));

                pantallas.Add(new Pantalla("Centro de costo RH", "RH", "Recursos humanos - Mantenimientos", "RH13"));
                pantallas.Add(new Pantalla("Conceptos de pago", "RH", "Recursos humanos - Mantenimientos", "RH14"));
                pantallas.Add(new Pantalla("Departamentos ", "RH", "Recursos humanos - Mantenimientos", "RH15"));
                pantallas.Add(new Pantalla("Días dobles", "RH", "Recursos humanos - Mantenimientos", "RH16"));
                pantallas.Add(new Pantalla("Entidades financieras", "RH", "Recursos humanos - Mantenimientos", "RH17"));
                pantallas.Add(new Pantalla("Empleados", "RH", "Recursos humanos - Mantenimientos", "RH18"));
                pantallas.Add(new Pantalla("Esquemas de redondeo de horas", "RH", "Recursos humanos - Mantenimientos", "RH19"));
                pantallas.Add(new Pantalla("Estados de empleado", "RH", "Recursos humanos - Mantenimientos", "RH20"));
                pantallas.Add(new Pantalla("Horarios", "RH", "Recursos humanos - Mantenimientos", "RH21"));
                pantallas.Add(new Pantalla("Jornadas", "RH", "Recursos humanos - Mantenimientos", "RH22"));
                pantallas.Add(new Pantalla("Lectores de marcas", "RH", "Recursos humanos - Mantenimientos", "RH23"));
                pantallas.Add(new Pantalla("Monedas", "RH", "Recursos humanos - Mantenimientos", "RH24"));
                pantallas.Add(new Pantalla("Nóminas", "RH", "Recursos humanos - Mantenimientos", "RH25"));
                pantallas.Add(new Pantalla("Números patronales", "RH", "Recursos humanos - Mantenimientos", "RH26"));
                pantallas.Add(new Pantalla("Países", "RH", "Recursos humanos - Mantenimientos", "RH27"));
                pantallas.Add(new Pantalla("Pólizas de seguro", "RH", "Recursos humanos - Mantenimientos", "RH28"));
                pantallas.Add(new Pantalla("Puestos", "RH", "Recursos humanos - Mantenimientos", "RH29"));
                pantallas.Add(new Pantalla("Reportes configurables", "RH", "Recursos humanos - Mantenimientos", "RH30"));
                pantallas.Add(new Pantalla("Tipos de acción de personal", "RH", "Recursos humanos - Mantenimientos", "RH31"));
                pantallas.Add(new Pantalla("Tipos de cambio", "RH", "Recursos humanos - Mantenimientos", "RH32"));
                pantallas.Add(new Pantalla("Tipos de identificación", "RH", "Recursos humanos - Mantenimientos", "RH33"));
                pantallas.Add(new Pantalla("Tipos de incapacidades", "RH", "Recursos humanos - Mantenimientos", "RH34"));
                pantallas.Add(new Pantalla("Tipos de permisos", "RH", "Recursos humanos - Mantenimientos", "RH35"));

                pantallas.Add(new Pantalla("Detalle pago nómina", "RH", "Recursos humanos - Consultas", "RH36"));
                pantallas.Add(new Pantalla("Reporte configurable", "RH", "Recursos humanos - Consultas", "RH37"));
                pantallas.Add(new Pantalla("Asistencia", "RH", "Recursos humanos - Consultas", "RH38"));
                pantallas.Add(new Pantalla("Labores diarias", "RH", "Recursos humanos - Consultas", "RH39"));

                pantallas.Add(new Pantalla("Parámetros de recursos humanos", "RH", "Recursos humanos - Administración", "RH40"));
                #endregion

                //================================================================================================
                #region Conta
                pantallas.Add(new Pantalla("Asientos contables", "Contabilidad", "Contabilidad - Procesos", "CT1"));
                pantallas.Add(new Pantalla("Balances iniciales", "Contabilidad", "Contabilidad - Procesos", "CT2"));
                pantallas.Add(new Pantalla("Importación de datos de RH", "Contabilidad", "Contabilidad - Procesos", "CT3"));

                pantallas.Add(new Pantalla("Categorías tipo cambio", "Contabilidad", "Contabilidad - Mantenimientos", "CT4"));
                pantallas.Add(new Pantalla("Centros de costo", "Contabilidad", "Contabilidad - Mantenimientos", "CT5"));
                pantallas.Add(new Pantalla("Cuentas contables", "Contabilidad", "Contabilidad - Mantenimientos", "CT6"));
                pantallas.Add(new Pantalla("Periodos contables", "Contabilidad", "Contabilidad - Mantenimientos", "CT7"));
                pantallas.Add(new Pantalla("Monedas", "Contabilidad", "Contabilidad - Mantenimientos", "CT8"));
                pantallas.Add(new Pantalla("Paquetes contables", "Contabilidad", "Contabilidad - Mantenimientos", "CT9"));
                pantallas.Add(new Pantalla("Plantillas asientos contables", "Contabilidad", "Contabilidad - Mantenimientos", "CT10"));
                pantallas.Add(new Pantalla("Subtipos cuentas", "Contabilidad", "Contabilidad - Mantenimientos", "CT11"));
                pantallas.Add(new Pantalla("Tipo asientos", "Contabilidad", "Contabilidad - Mantenimientos", "CT12"));
                pantallas.Add(new Pantalla("Tipos de cambio", "Contabilidad", "Contabilidad - Mantenimientos", "CT13"));

                pantallas.Add(new Pantalla("Detalle de asientos", "Contabilidad", "Contabilidad - Consultas", "CT14"));
                //pantallas.Add(new Pantalla("Módulos auxiliares", "Contabilidad", "Contabilidad", "CT15"));
                pantallas.Add(new Pantalla("Parámetros de contabilidad ", "Contabilidad", "Contabilidad - Administración", "CT15"));
                pantallas.Add(new Pantalla("Saldos de mayor", "Contabilidad", "Contabilidad - Consultas", "CT16"));
                pantallas.Add(new Pantalla("Reportes contables", "Contabilidad", "Contabilidad - Consultas", "CT17"));
                //pantallas.Add(new Pantalla("Reporte balance de situación", "Contabilidad", "Contabilidad", "CT18"));
                //pantallas.Add(new Pantalla("Reporte estado de resultados", "Contabilidad", "Contabilidad", "CT19"));

                pantallas.Add(new Pantalla("Cierre fiscal", "Contabilidad", "Contabilidad - Procesos", "CT20"));
                pantallas.Add(new Pantalla("Conceptos de renta", "Contabilidad", "Contabilidad - Mantenimientos", "CT21"));

                #endregion

                //================================================================================================
                #region Bancos
                pantallas.Add(new Pantalla("Cheques en tránsito", "Bancos", "Bancos - Procesos", "B1"));
                pantallas.Add(new Pantalla("Conciliación bancaria ", "Bancos", "Bancos - Procesos", "B2"));
                pantallas.Add(new Pantalla("Movimientos en bancos", "Bancos", "Bancos - Procesos", "B3"));
                pantallas.Add(new Pantalla("Movimientos en libros", "Bancos", "Bancos - Procesos", "B4"));

                pantallas.Add(new Pantalla("Categoría de documento", "Bancos", "Bancos - Mantenimientos", "B5"));
                pantallas.Add(new Pantalla("Categorías de tipo de cambio", "Bancos", "Bancos - Mantenimientos", "B6"));
                pantallas.Add(new Pantalla("Cuentas bancarias", "Bancos", "Bancos - Mantenimientos", "B7"));
                pantallas.Add(new Pantalla("Entidades financieras", "Bancos", "Bancos - Mantenimientos", "B8"));
                pantallas.Add(new Pantalla("Moneda", "Bancos", "Bancos - Mantenimientos", "B9"));
                pantallas.Add(new Pantalla("Tipos de cambio", "Bancos", "Bancos - Mantenimientos", "B10"));
                pantallas.Add(new Pantalla("Tipos de documento", "Bancos", "Bancos - Mantenimientos", "B11"));

                #endregion

            }
          
            this.ordenModulos();
        }
        //
        private void ordenModulos()
        {
            this.pantallas = this.pantallas.OrderBy(o => o.NombreModulo).ThenBy(o => o.Nombre).ToList(); ;
        }
        //
        public List<Pantalla> lPantallas
        {
            get { return pantallas; }
        }
        //

    }//Finaliza clase PantallaRapida

}//Finaliza namespace
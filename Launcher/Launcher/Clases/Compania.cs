﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NCQ_B;

namespace Launcher
{

    public class Compania
    {
        AccesoDatos oAccesoDatos;
        CompaniaGestora oCompaniaGestora;
        bool isError;
        string errorDescription;
        public Compania(AccesoDatos poAccesoDatos)
        {
            oAccesoDatos = poAccesoDatos;
            oCompaniaGestora = new CompaniaGestoraP(oAccesoDatos);
        }

        public  string optenerCliente()
        {
            DataSet dsetDatos = new DataSet();
            dsetDatos = oCompaniaGestora.optenerCliente();
            if (oCompaniaGestora.IsError)
            {
                this.isError = true;
                this.errorDescription = oCompaniaGestora.ErrorDescription;
                return "";
            }

            if (dsetDatos.Tables.Count > 0)
            {
                return dsetDatos.Tables[0].Rows[0][0].ToString();
            }
            return "";

        }
        public  string optenerURlSoporte()
        {
            DataSet dsetDatos = new DataSet();
            dsetDatos = oCompaniaGestora.optenerURlSoporte();
            if (oCompaniaGestora.IsError)
            {
                this.isError = true;
                this.errorDescription = oCompaniaGestora.ErrorDescription;
                return "";
            }

            if (dsetDatos.Tables.Count > 0)
            {
                return dsetDatos.Tables[0].Rows[0][0].ToString();
            }
            return "";

        }
        public DataSet optenerParametrosDescarga()
        {
            DataSet dsetDatos = new DataSet();
            dsetDatos = oCompaniaGestora.optenerParametrosDescarga();
            if (oCompaniaGestora.IsError)
            {
                this.isError = true;
                this.errorDescription = oCompaniaGestora.ErrorDescription;
                return null;
            }
                return dsetDatos;
        }
        public  string optenerURlImagenes()
        {
           DataSet pDataSet= oCompaniaGestora.optenerURlImagenes();
            if (oCompaniaGestora.IsError)
            {
                this.isError = true;
                this.errorDescription = oCompaniaGestora.ErrorDescription;
                return null;
            }
            if (pDataSet.Tables.Count > 0)
            {
                if (pDataSet.Tables[0].Rows.Count > 0)
                {
                    return pDataSet.Tables[0].Rows[0][0].ToString();
                }
            }
            return null;

        }

        public void ModificarFecha(DateTime pFecha)
        {
             oCompaniaGestora.ModificarFecha(pFecha);
            if (oCompaniaGestora.IsError)
            {
                this.isError = true;
                this.errorDescription = oCompaniaGestora.ErrorDescription;
                return ;
            }

        }
        #region sets y gets de la clase

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public string ErrorDescription
        {
            get { return errorDescription; }
            set { errorDescription = value; }
        }

        #endregion
    }
}

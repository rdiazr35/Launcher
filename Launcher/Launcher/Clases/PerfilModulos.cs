﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Launcher
{
    public class modulo
    {
        string nombre;
        string acceso;
        int orden;
        string aShort;
        //
        public modulo() {
        }
        //
        public modulo(string pNombre,string pAcceso,string pShort,int pOrden)
        {
            this.nombre=pNombre;
            this.acceso = pAcceso;
            this.aShort = pShort;
            this.orden = pOrden;
        }
        //
        public string Nombre
        {
           get{ return nombre;}
        }
        //
        public string Acceso
        {
           get{ return acceso;}
        }
        //
        public string Short
        {
           get { return aShort; }
        }
        //
        public int Orden
        {
            get { return orden; }
        }
        //
    }
    //
    public class PerfilModulos
    {
        List<modulo> modulos=new List<modulo>();

        public PerfilModulos(string pPerfil)
        {

            this.modulos.Add(new modulo("Ventas", "VENTAS", "Ventas", 1));
            this.modulos.Add(new modulo("Administración", "Administración del sistema", "Administración", 99));
            
            if (pPerfil == "1" || pPerfil == "2")
            {
                this.modulos.Add(new modulo("Compras", "COMPRAS", "Compras", 2));
                this.modulos.Add(new modulo("Cuentas por pagar", "CxP", "CxP", 4));
                this.modulos.Add(new modulo("Inventario", "Inventario", "Inventario", 3));
                this.modulos.Add(new modulo("Rutas", "QPOS Móvil", "Rutas", 99));
            } 
            if (pPerfil == "2")
            {
                this.modulos.Add(new modulo("Gastos", "Gastos", "Gastos", 99));
                this.modulos.Add(new modulo("Recursos humanos", "RH", "RH", 99));
                this.modulos.Add(new modulo("Bancos", "Bancos", "Bancos", 99));
                this.modulos.Add(new modulo("Contabilidad", "Contabilidad", "Contabilidad", 99));
            }
            this.ordenModulos();
        }
        //
        private void ordenModulos()
        {
            this.modulos = modulos.OrderBy(o => o.Orden).ThenBy(o => o.Nombre).ToList(); ;
        }
        //
        public List<modulo> Modulos
        {
            get {return modulos; }
        }
        //
    }//Finaliza clase
}//Finaliza namespace
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Conversor;
using Launcher;

namespace Launcher
{
    public class Accesos
    {
        #region DisposedVar
        private IntPtr handle;
        private Component component = new Component();
        private bool disposed = false;
        #endregion

        string arg = "";
        public string ErrorDescripcion = "";
        //
        public Accesos()
        {
            this.ErrorDescripcion = "";
        }
        //
        public Accesos(string plogin, string pcompania, string pnomCompania,
                   string pTipoConexion, string pusuarioBD, string pcontrasena, string pServer,
                   string pdataBase, string pSchema, string pPort, string pODBC)
        {
            this.ErrorDescripcion = "";
            this.getArguments(plogin, pcompania, pnomCompania,
                    pTipoConexion, pusuarioBD, pcontrasena, pServer,
                    pdataBase, pSchema, pPort, pODBC);
        }
        //
        ~Accesos()
        {
            Dispose(false);
        }
        //
        #region Disposed
        //
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        //
        protected virtual void Dispose(bool disposing)
        {
           
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                CloseHandle(handle);
                handle = IntPtr.Zero;
                disposed = true;

            }
        }
        //
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);
        //
        #endregion
        //
        #region delphi
        //
        public Accesos(string plogin, string pcompania, string pnomCompania,
            string pTipoConexion, string pusuarioBD, string pcontrasena, string pServer,
            string pdataBase, string pSchema, string pPort, string pODBC, bool pBandera)
        {
            ErrorDescripcion = "";
            getArgumentsD(plogin, pcompania, pnomCompania,
                    pTipoConexion, pusuarioBD, pcontrasena, pServer,
                    pdataBase, pSchema, pPort, pODBC, pcontrasena);
        }
        //
        private void getArgumentsD(string plogin, string pcompania, string pnomCompania,
            string pTipoConexion, string pusuarioBD, string pcontrasena, string pServer,
            string pdataBase, string pSchema, string pPort, string pODBC, string pDelphi)
        {
            String argumentos = "\"" + plogin + "\"" + " 0 " + "\"" + pcompania + "\"" + " " + "\"" + pnomCompania + "\"" + " " + "\"" 
                + pTipoConexion + "\"" + " " + "\"" + pusuarioBD + "\"" + " " + "\"" + pDelphi + "\"" + " " + "\"" + pServer
                + "\"" + " " + "\"" + pdataBase + "\"" + " " + "\"" + pSchema + "\"" + " " + "\"" + pPort + "\"" + " " + "\"" + pODBC + "\"" + " ";
            string codigoCompania = getCodigo(pusuarioBD, pcontrasena, pServer,
                    pdataBase, pSchema, pPort);

            Token oToken = new Token();
            argumentos += oToken.EncriptarDatos("Qupos", codigoCompania);
            arg = argumentos;

        }
        //
        #endregion
        //
        public bool exeCom(string pCommand)
        {
            try
            {
                string command = Directory.GetParent(Assembly.GetExecutingAssembly().Location) + "\\" + pCommand+".exe";

                Process process = new Process();
                process.StartInfo.FileName = command;
                process.StartInfo.Arguments = arg;
                process.Start();
                return true;
            }
            catch (Exception ex)
            {
                ErrorDescripcion = ex.Message;
                log(ex.Message);
                return false;
                
            }
        }
        //
        private string getCodigo( string pusuarioBD, string pcontrasena, string pServer,
            string pdataBase, string pSchema, string pPort)
        {
            try
            {
                NCQ_B.Encriptador oEncriptador = new NCQ_B.Encriptador();
                string schema = "";

                if (pSchema != "")
                {
                    schema = "\"" + pSchema + "\".";
                }
                NCQ_B.AccesoDatos oAccesoDatos = new NCQ_B.AccesoDatosPostgre(
                   pusuarioBD,
                   pPort,
                    oEncriptador.DesencriptarCadena(pcontrasena),
                    pServer,
                    pdataBase,
                    schema);
                Compania oCompania = new Compania(oAccesoDatos);
                if(oCompania.IsError)
                {
                    log("Error:" + oCompania.ErrorDescription);
                }
                String retorno = oCompania.optenerCliente();
                return retorno;
            }
            catch(Exception er)
            {
                log("Error:"+er.Message);
                return "";
            }
        }
        //
        static void log(string estado)
        {
            StreamWriter log;

            if (!File.Exists(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\logfile.txt"))
            {
                log = new StreamWriter(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\logfile.txt");
            }
            else
            {
                log = File.AppendText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\logfile.txt");
            }


            log.WriteLine(DateTime.Now);
            log.WriteLine(estado);
            log.WriteLine("------------------------------------------------------------------------");
            log.WriteLine();


            log.Close();
        }
        //
        public void addParam(string pParam)
        {
            arg += " " + "\"" + pParam + "\"";
        }
        //
        private void getArguments(string plogin, string pcompania, string pnomCompania,
            string pTipoConexion, string pusuarioBD, string pcontrasena, string pServer,
            string pdataBase, string pSchema, string pPort, string pODBC)
        {
            String argumentos = "\"" + plogin + "\"" + " 0 " + "\"" + pcompania + "\"" + " " + "\"" + pnomCompania + "\"" + " " + "\"" + pTipoConexion + "\"" + " " + "\"" + pusuarioBD + "\"" +
                " " + "\"" + pcontrasena + "\"" + " " + "\"" + pServer
                + "\"" + " " + "\"" + pdataBase + "\"" + " " + "\"" + pSchema + "\"" + " " + "\"" + pPort + "\"" + " " + "\"" + pODBC + "\"" + " ";
            string codigoCompania = getCodigo(pusuarioBD, pcontrasena, pServer,
                    pdataBase, pSchema, pPort);

            Token oToken = new Token();
            argumentos += oToken.EncriptarDatos("Qupos", codigoCompania);
            arg = argumentos;
        }
        //
        public void extencionPantalla(string pantalla)
        {
            switch (pantalla)
            {

                case "V2":
                    addParam("S");
                    addParam("X");
                    break;
                case "CO9":
                    int i = arg.IndexOf(" ");
                    arg = arg.Remove(i, 2).Insert(i, " 1");
                    break;
                case "V59":
                    addParam(Program.login);
                    break;

            }

        }
        //

    }//Finaliza clase
}//Finaliza namespace
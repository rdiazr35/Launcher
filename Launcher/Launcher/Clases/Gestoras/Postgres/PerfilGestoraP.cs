﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NCQ_B;
using Npgsql;
using NpgsqlTypes;

namespace Launcher
{

        public class PerfilGestoraP : PerfilGestora
        {
            public PerfilGestoraP(AccesoDatos poAccesoDatos)
                : base(poAccesoDatos)
            {
            }
            //destructor
            ~PerfilGestoraP()
            {
            }
            public override DataSet cargarPerfil(string pProduct, string pHw)
            {
                string query = "";
                StringBuilder sql = new StringBuilder();
                sql.AppendLine(" select tl from " + oAccesoDatos.Schema + "estacion_trabajo ");
                if (!pProduct.Equals(""))
                {
                    query = "where pro = @product ";
                }
                if (!pHw.Equals(""))
                {
                    if (!query.Equals(""))
                        query += "and ";
                    else
                        query += "where ";

                    query += "hw = @hw ";
                }
                sql.AppendLine(" "+query);
                NpgsqlParameter[] Parametros = new NpgsqlParameter[2];

                Parametros[0] = new NpgsqlParameter();
                Parametros[0].NpgsqlDbType = NpgsqlDbType.Varchar;
                Parametros[0].ParameterName = "@product";
                Parametros[0].Value = pProduct;

                Parametros[1] = new NpgsqlParameter();
                Parametros[1].NpgsqlDbType = NpgsqlDbType.Varchar;
                Parametros[1].ParameterName = "@hw";
                Parametros[1].Value = pHw;

                DataSet oDset = new DataSet();

                oDset = oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "perfil", Parametros);
                if (oAccesoDatos.IsError)
                {
                    error = true;
                    errorMsg = oAccesoDatos.ErrorDescripcion;
                }

                return oDset;
            }
            public override DataSet cargarLic(string pProduct, string pHw)
            {
                string query = "";
                StringBuilder sql = new StringBuilder();
                sql.AppendLine(" select lic from " + oAccesoDatos.Schema + "estacion_trabajo ");
                if (!pProduct.Equals(""))
                {
                    query = "where pro = @product ";
                }
                if (!pHw.Equals(""))
                {
                    if (!query.Equals(""))
                        query += "and ";
                    else
                        query += "where ";

                    query += "hw = @hw ";
                }

                NpgsqlParameter[] Parametros = new NpgsqlParameter[2];

                Parametros[0] = new NpgsqlParameter();
                Parametros[0].NpgsqlDbType = NpgsqlDbType.Varchar;
                Parametros[0].ParameterName = "@product";
                Parametros[0].Value = pProduct;

                Parametros[1] = new NpgsqlParameter();
                Parametros[1].NpgsqlDbType = NpgsqlDbType.Varchar;
                Parametros[1].ParameterName = "@hw";
                Parametros[1].Value = pHw;

                DataSet oDset = new DataSet();

                oDset = oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "perfil", Parametros);
                if (oAccesoDatos.IsError)
                {
                    error = true;
                    errorMsg = oAccesoDatos.ErrorDescripcion;
                }

                return oDset;
            }
            public override DataSet cargarConfiguracion(string pProduct, string pHw)
            {
                string query = "";
                StringBuilder sql = new StringBuilder();
                sql.AppendLine(" select system_tray,modulo_ventana,key_cerrar from " + oAccesoDatos.Schema + "estacion_trabajo ");
                if (!pProduct.Equals(""))
                {
                    query = "where pro = @product ";
                }
                if (!pHw.Equals(""))
                {
                    if (!query.Equals(""))
                        query += "and ";
                    else
                        query += "where ";

                    query += "hw = @hw ";
                }

                NpgsqlParameter[] Parametros = new NpgsqlParameter[2];

                Parametros[0] = new NpgsqlParameter();
                Parametros[0].NpgsqlDbType = NpgsqlDbType.Varchar;
                Parametros[0].ParameterName = "@product";
                Parametros[0].Value = pProduct;

                Parametros[1] = new NpgsqlParameter();
                Parametros[1].NpgsqlDbType = NpgsqlDbType.Varchar;
                Parametros[1].ParameterName = "@hw";
                Parametros[1].Value = pHw;

                DataSet oDset = new DataSet();

                oDset = oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "perfil", Parametros);
                if (oAccesoDatos.IsError)
                {
                    error = true;
                    errorMsg = oAccesoDatos.ErrorDescripcion;
                }

                return oDset;
            }
            public override void modificarSystemtray(string pProduct, string pHw,string tray,string pModuloVentana,string pKeyCerrar)
            {
                string filtro = "";
                if (pHw != "")
                {
                    if (filtro != "")
                        filtro += " and ";
                    filtro += " hw = @hw ";
                }
                if (pProduct != "")
                {
                    if (filtro != "")
                        filtro += " and ";
                    filtro += " pro = @product ";
                }

                StringBuilder sql = new StringBuilder();
                sql.AppendLine(" update " + oAccesoDatos.Schema + "estacion_trabajo set system_tray=@tray, ");
                sql.AppendLine(" modulo_ventana = @modulo_ventana, ");
                sql.AppendLine(" key_cerrar = @key_cerrar ");
                if (!filtro.Equals(""))
                    sql.AppendLine(" where " + filtro);
                
                ParametrosPostgres oParametros = new ParametrosPostgres();
                oParametros.AddNull(NpgsqlDbType.Varchar, "@product", pProduct);
                oParametros.AddNull(NpgsqlDbType.Varchar, "@hw", pHw);
                oParametros.AddNull(NpgsqlDbType.Varchar, "@tray", tray);
                oParametros.AddNull(NpgsqlDbType.Varchar, "@modulo_ventana", pModuloVentana);
                oParametros.AddNull(NpgsqlDbType.Varchar, "@key_cerrar", pKeyCerrar);
                oAccesoDatos.ejecutarSQL(sql.ToString(), oParametros.GetParameters());
                if (oAccesoDatos.IsError)
                {
                    error = true;
                    errorMsg = oAccesoDatos.ErrorDescripcion;
                }
                return ;
            }
        }
    }



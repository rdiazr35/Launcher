﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NCQ_B;
using NpgsqlTypes;

namespace Launcher
{
       
    public class CompaniaGestoraP:CompaniaGestora
    {
         
        public CompaniaGestoraP(AccesoDatos poAccesoDatos)
            : base(poAccesoDatos)
        {
            
        }

        public override DataSet optenerCliente()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select coalesce(cl,' ') ");
            sql.Append("from " + OAccesoDatos.Schema + "compania ");
          
            DataSet oFacturas = OAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "cliente");
            if (OAccesoDatos.IsError)
            {
                this.IsError = true;
                this.ErrorDescription = OAccesoDatos.ErrorDescripcion;
            }
            return oFacturas;
        }
        public override DataSet optenerURlSoporte()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select url_soporte ");
            sql.Append("from " + OAccesoDatos.Schema + "compania ");

            DataSet oFacturas = OAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "compania");
            if (OAccesoDatos.IsError)
            {
                this.IsError = true;
                this.ErrorDescription = OAccesoDatos.ErrorDescripcion;
            }
            return oFacturas;
        }
        public override DataSet optenerParametrosDescarga()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select url_imagenes,to_char(fecha_imagen, 'dd/MM/yyyy HH24:MI:SS') as fecha_imagen ");
            sql.Append("from " + OAccesoDatos.Schema + "compania ");

            DataSet oFacturas = OAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "compania");
            if (OAccesoDatos.IsError)
            {
                this.IsError = true;
                this.ErrorDescription = OAccesoDatos.ErrorDescripcion;
            }
            return oFacturas;
        }
        public override void ModificarFecha(DateTime pFecha)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("update " + OAccesoDatos.Schema + "compania set ");
            sql.Append("fecha_imagen=@Fecha");
            ParametrosPostgres oParametrosPostgres = new ParametrosPostgres();
            oParametrosPostgres.AddNull(NpgsqlDbType.Timestamp, "@Fecha", pFecha);

            OAccesoDatos.ejecutarSQL(sql.ToString(), oParametrosPostgres.GetParameters());
            if (OAccesoDatos.IsError)
            {
                this.IsError = true;
                this.ErrorDescription = OAccesoDatos.ErrorDescripcion;
            }
           
        }
        public override DataSet optenerURlImagenes()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("select url_imagenes_s ");
            sql.Append("from " + OAccesoDatos.Schema + "compania ");

            DataSet oFacturas = OAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "compania");
            if (OAccesoDatos.IsError)
            {
                this.IsError = true;
                this.ErrorDescription = OAccesoDatos.ErrorDescripcion;
            }
            return oFacturas;
        }
    }
}

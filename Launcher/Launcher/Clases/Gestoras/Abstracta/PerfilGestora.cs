﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NCQ_B;

namespace Launcher
{
    public abstract class PerfilGestora
    {
        protected AccesoDatos oAccesoDatos;
        protected bool error;
        protected string errorMsg;


        //constructor
        public PerfilGestora(AccesoDatos poAccesoDato)
        {
            //Inicializacion de variables
            oAccesoDatos = poAccesoDato;

        }
        public abstract DataSet cargarConfiguracion(string pProduct, string pHw);
        public abstract DataSet cargarLic(string pProduct, string pHw);
        public abstract DataSet cargarPerfil(string pProduct, string pHw);
        public abstract void modificarSystemtray(string pProduct, string pHw, string tray, string pModuloVentana, string pKeyCerrar);

        public bool Error
        {
            get { return error; }
        }

        public string ErrorMsg
        {
            get { return errorMsg; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NCQ_B;

namespace Launcher
{
    abstract public class CompaniaGestora
    {
        AccesoDatos oAccesoDatos;
        
        bool isError;
        string errorDescription;
        public CompaniaGestora(AccesoDatos poAccesoDatos)
        {
            oAccesoDatos = poAccesoDatos;
        }
        public abstract DataSet optenerCliente();
        public abstract DataSet optenerURlSoporte();
        public abstract DataSet optenerParametrosDescarga();
        public abstract void ModificarFecha(DateTime pFecha);
        public abstract DataSet optenerURlImagenes();
        #region sets y gets de la clase

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public string ErrorDescription
        {
            get { return errorDescription; }
            set { errorDescription = value; }
        }

        public AccesoDatos OAccesoDatos
        {
            get { return oAccesoDatos; }
            set { oAccesoDatos = value; }
        }
        #endregion
    }
}

﻿namespace Launcher
{
    partial class frmLicencias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLicencias));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.edtHardware = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.edtCompania = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.edtLicencias = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.edtKey = new DevExpress.XtraEditors.TextEdit();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnEnlinea = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtHardware.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtCompania.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtLicencias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtKey.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(91)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(422, 62);
            this.panel1.TabIndex = 15;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::Launcher.Properties.Resources._64x64;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.ImageLocation = "";
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(422, 62);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(37, 71);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(56, 13);
            this.lblUsuario.TabIndex = 17;
            this.lblUsuario.Text = "Hardware:";
            // 
            // edtHardware
            // 
            this.edtHardware.Location = new System.Drawing.Point(99, 68);
            this.edtHardware.Name = "edtHardware";
            this.edtHardware.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.edtHardware.Properties.Appearance.Options.UseBackColor = true;
            this.edtHardware.Properties.LookAndFeel.SkinName = "Black";
            this.edtHardware.Size = new System.Drawing.Size(251, 20);
            this.edtHardware.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Compania:";
            // 
            // edtCompania
            // 
            this.edtCompania.Location = new System.Drawing.Point(99, 94);
            this.edtCompania.Name = "edtCompania";
            this.edtCompania.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.edtCompania.Properties.Appearance.Options.UseBackColor = true;
            this.edtCompania.Properties.LookAndFeel.SkinName = "Black";
            this.edtCompania.Size = new System.Drawing.Size(251, 20);
            this.edtCompania.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Licencias:";
            // 
            // edtLicencias
            // 
            this.edtLicencias.Location = new System.Drawing.Point(98, 120);
            this.edtLicencias.Name = "edtLicencias";
            this.edtLicencias.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.edtLicencias.Properties.Appearance.Options.UseBackColor = true;
            this.edtLicencias.Properties.LookAndFeel.SkinName = "Black";
            this.edtLicencias.Size = new System.Drawing.Size(252, 20);
            this.edtLicencias.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Key:";
            // 
            // edtKey
            // 
            this.edtKey.Location = new System.Drawing.Point(98, 146);
            this.edtKey.Name = "edtKey";
            this.edtKey.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.edtKey.Properties.Appearance.Options.UseBackColor = true;
            this.edtKey.Properties.LookAndFeel.SkinName = "Black";
            this.edtKey.Size = new System.Drawing.Size(252, 20);
            this.edtKey.TabIndex = 22;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.Location = new System.Drawing.Point(270, 172);
            this.btnCancelar.LookAndFeel.SkinName = "Black";
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(80, 28);
            this.btnCancelar.TabIndex = 25;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAceptar.Location = new System.Drawing.Point(98, 172);
            this.btnAceptar.LookAndFeel.SkinName = "Black";
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(80, 28);
            this.btnAceptar.TabIndex = 24;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnEnlinea
            // 
            this.btnEnlinea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnlinea.Location = new System.Drawing.Point(184, 172);
            this.btnEnlinea.LookAndFeel.SkinName = "Black";
            this.btnEnlinea.Name = "btnEnlinea";
            this.btnEnlinea.Size = new System.Drawing.Size(80, 28);
            this.btnEnlinea.TabIndex = 26;
            this.btnEnlinea.Text = "En-Linea";
            this.btnEnlinea.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // frmLicencias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 221);
            this.Controls.Add(this.btnEnlinea);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edtKey);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edtLicencias);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edtCompania);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.edtHardware);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmLicencias";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registro de licencias";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtHardware.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtCompania.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtLicencias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtKey.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblUsuario;
        private DevExpress.XtraEditors.TextEdit edtHardware;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit edtCompania;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit edtLicencias;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit edtKey;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.SimpleButton btnEnlinea;

    }
}
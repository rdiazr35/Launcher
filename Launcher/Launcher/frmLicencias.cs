﻿using LicenciasGestion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Launcher
{
    public partial class frmLicencias : Form
    {
        Licencia oLicencia;
        Boolean existiaCompania = false;
        public Boolean cerrarApp = false;
        string user = "";
        public frmLicencias(string  usuario, string port,string password,string server, string database, string schema
            ,string puser)
        {
            InitializeComponent();
            oLicencia = new Licencia(usuario,port,password,server,
                                      database,schema, "Licencias", puser);
            user = puser;
            optenerParametros();
        }
        private void optenerParametros()
        {
            try
            {
                DataSet dsetValores = oLicencia.optenertParametros();
                if (oLicencia.IsError)
                {
                    MessageBox.Show(oLicencia.ErrorDescripcion, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return;
                }
                string hardware = (dsetValores.Tables[0].Rows[0]["hw"] ?? "").ToString();
                string licencia = (dsetValores.Tables[0].Rows[0]["licencia"] ?? "").ToString();
                string compania = (dsetValores.Tables[0].Rows[0]["compania"] ?? "").ToString();
                edtHardware.Text = String.IsNullOrEmpty(hardware) ? "Hardware" : hardware;
                edtCompania.Text = String.IsNullOrEmpty(compania) ? "Compania" : compania;
                edtLicencias.Text = String.IsNullOrEmpty(licencia) ? "Licencia" : licencia;
                if (String.IsNullOrEmpty(compania))
                {
                    existiaCompania = true;
                    edtCompania.Text = "";
                    edtCompania.Focus();
                    edtCompania.SelectAll();
                }
                else
                {
                    edtCompania.Enabled = false;
                    if (edtLicencias.Text.Equals("Licencia"))
                    {
                        edtLicencias.Text = "";
                        edtLicencias.Focus();
                        edtLicencias.SelectAll();
                    }
                }
                GC.Collect();
            }
            catch (Exception er)
            {
               
                MessageBox.Show(er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
               
                GC.Collect();
                return;
            }
        }

        private void registroLicencia()
        {
            #region datos completos
            if (String.IsNullOrEmpty(edtCompania.Text) || edtCompania.Text.Equals("Compania"))
            {
                
                MessageBox.Show("Falta el código del cliente.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
               
                GC.Collect();
                return;
            }
            if (String.IsNullOrEmpty(edtLicencias.Text) || edtLicencias.Text.Equals("Licencia"))
            {
                
                MessageBox.Show("Falta la licencia.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
               
                GC.Collect();
                return;
            }
            if (String.IsNullOrEmpty(edtKey.Text) || edtKey.Text.Equals("Key"))
            {
                MessageBox.Show("Falta el código de activación.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GC.Collect();
                return;
            }
            if (String.IsNullOrEmpty(edtHardware.Text) || edtKey.Text.Equals("Hardware"))
            {
               
                MessageBox.Show("Error en el código hardware.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                GC.Collect();
                return;
            }
            #endregion

            if (existiaCompania)
            {
                oLicencia.insertCliente(edtCompania.Text);
                if (oLicencia.IsError)
                {
                   
                    MessageBox.Show(oLicencia.ErrorDescripcion, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                    GC.Collect();
                    return;
                }
                GC.Collect();
            }
            oLicencia.registroManual(edtKey.Text, edtHardware.Text, edtLicencias.Text, edtCompania.Text);
            if (oLicencia.IsError)
            {
               
                MessageBox.Show(oLicencia.ErrorDescripcion, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                GC.Collect();
                return;
            }
            else
            {

                MessageBox.Show("La licencia se registró de forma correcta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                GC.Collect();
                this.Close();
                return;
            }
            GC.Collect();
        }
        private void registroLicenciaEnlinea()
        {
            #region datos completos
            if (String.IsNullOrEmpty(edtCompania.Text) || edtCompania.Text.Equals("Compania"))
            {
               
                MessageBox.Show("Falta el código del cliente.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
              
                GC.Collect();
                return;
            }
            if (String.IsNullOrEmpty(edtHardware.Text) || edtKey.Text.Equals("Hardware"))
            {
               
                MessageBox.Show("Error en el código hardware.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                GC.Collect();
                return;
            }
            #endregion

            if (existiaCompania)
            {
                oLicencia.insertCliente(edtCompania.Text);
                if (oLicencia.IsError)
                {
                    MessageBox.Show(oLicencia.ErrorDescripcion, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                  
                    GC.Collect();
                    return;
                }
                GC.Collect();
            }
            string compania = edtCompania.Text;
            string licencia = edtLicencias.Text;
            string hw = edtHardware.Text;
            string key = edtKey.Text;
            DataSet dset = oLicencia.getValoresLicencia(compania, hw, licencia.Equals("Licencia") ? "" : licencia, user);
            if (oLicencia.IsError)
            {
                
                MessageBox.Show(oLicencia.ErrorDescripcion, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GC.Collect();
                return;
            }
            try
            {
                key = (dset.Tables["parametros"].Rows[0]["key"] ?? key).ToString();
                licencia = dset.Tables["parametros"].Rows[0]["licencia"].ToString();
            }
            catch (Exception er)
            {
                
                MessageBox.Show(er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GC.Collect();
                return;
            }
            oLicencia.registroManual(key, hw, licencia, compania);
            if (oLicencia.IsError)
            {
               
                MessageBox.Show(oLicencia.ErrorDescripcion, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                GC.Collect();
                return;
            }
            else
            {
                MessageBox.Show("La licencia se registró de forma correcta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GC.Collect();
                this.Close();
                return;
            }
            GC.Collect();
        }
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            registroLicencia();
            edtKey.Focus();
            edtKey.SelectAll();
            GC.Collect();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            registroLicenciaEnlinea();
            GC.Collect();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            cerrarApp = true;
            this.Close();
        }
    }
}

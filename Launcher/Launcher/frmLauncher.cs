﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace Launcher
{
    public partial class frmLauncher : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        Thread tOptions;

        public frmLauncher()
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            InitializeComponent();
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }
        //
        private void frmLauncher_Load(object sender, EventArgs e)
        {
            this.pnlMenu.Visible = false;
            
            List<MenuSlider> menuPrincipal = new List<MenuSlider>();
            List<MenuSlider> menuConsultas = new List<MenuSlider>();
            //
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Ventas", "1"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Compras", "2"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Inventario", "3"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Cuentas por pagar", "4"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Administración", "5"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Bancos", "6"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Comisiones", "7"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Contabilidad", "8"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Gastos", "9"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Recursos humanos", "10"));
            menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Rutas", "11"));


            /*for (int i = 0; i < 1000; i++)
            {
                menuPrincipal.Add(FlyweightItemMenu.MakeItemMenu("Rutas", "11"));
            }*/
            //
            menuConsultas.Add(FlyweightItemMenu.MakeItemMenu("Preferencias", "F3"));
            menuConsultas.Add(FlyweightItemMenu.MakeItemMenu("Soporte", "F1"));
            //
            //
            CargaMenu(menuPrincipal, menuConsultas);
        }

        private void CargaMenu(List<MenuSlider> menuPrincipal, List<MenuSlider> menuConsultas)
        {
            this.tOptions = new Thread(new ThreadStart(() =>
            {

                this.Invoke((MethodInvoker)delegate
                {
                    this.Refresh();
                });
                //
                foreach (MenuSlider m in menuPrincipal)
                {
                    this.pnlMenu.Invoke((MethodInvoker)delegate
                    {
                        this.pnlMenu.Controls.Add(m);
                    });
                }
                //
                foreach (MenuSlider m in menuConsultas)
                {
                    this.pnlConsultas.Invoke((MethodInvoker)delegate
                    {
                        this.pnlConsultas.Controls.Add(m);
                    });
                }
                //
                this.Invoke((MethodInvoker)delegate
                {
                    this.lblEspere.Visible = false;
                    this.pnlMenu.Visible = true;
                });
            }));
            this.tOptions.Start();
        }

        #region Métodos de botones
        //
        private void lblMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //
        private void lblCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //
        private void lblMaximizar_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }
        //
        #endregion

        #region Métodos componentes
        //
        private void frmLauncher_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        //
        private void lblCerrar_MouseHover(object sender, EventArgs e)
        {
            this.lblCerrar.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMinimizar_MouseHover(object sender, EventArgs e)
        {
            this.lblMinimizar.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMinimizar_MouseLeave(object sender, EventArgs e)
        {
            this.lblMinimizar.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void lblCerrar_MouseLeave(object sender, EventArgs e)
        {
            this.lblCerrar.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void lblMaximizar_MouseHover(object sender, EventArgs e)
        {
            this.lblMaximizar.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMaximizar_MouseLeave(object sender, EventArgs e)
        {
            this.lblMaximizar.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void plnHeader_MouseClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else if (WindowState == FormWindowState.Normal)
                this.WindowState = FormWindowState.Maximized;
        }
        //
        private void pnlMenu_ControlAdded(object sender, ControlEventArgs e)
        {
            e.Control.BringToFront();
        }
        //
        #endregion

    }//Finaliza clase
}//Finaliza manespace
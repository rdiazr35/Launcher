﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace LauncherQupos
{
    public class ImageBackGround
    {
        private Image[] fondos = new Image[8];

        public ImageBackGround()
        {
            Fondos[0] = global::LauncherQupos.Properties.Resources.QUPOS_01;
            Fondos[1] = global::LauncherQupos.Properties.Resources.QUPOS_02;
            Fondos[2] = global::LauncherQupos.Properties.Resources.QUPOS_03;
            Fondos[3] = global::LauncherQupos.Properties.Resources.QUPOS_04;
            Fondos[4] = global::LauncherQupos.Properties.Resources.QUPOS_05;
            Fondos[5] = global::LauncherQupos.Properties.Resources.QUPOS_06;
            Fondos[6] = global::LauncherQupos.Properties.Resources.QUPOS_07;
            Fondos[7] = global::LauncherQupos.Properties.Resources.QUPOS_08;
        }

        public Image[] Fondos { get { return fondos; } set { fondos = value; } }
    }
}
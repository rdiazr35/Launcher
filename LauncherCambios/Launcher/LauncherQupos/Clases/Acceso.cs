﻿using NCQ_B;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LauncherQupos
{
    public class Acceso : ErrorHandler
    {
        private string arg = "";
        private IntPtr handle;
        private Component component = new Component();
        private bool disposed = false;
        //
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);
        //
        public Acceso()
        {
            this.ErrorDescripcion = "";
        }
        //
        ~Acceso()
        {
            Dispose(false);
        }
        //
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        //
        public bool exeCom(string pCommand)
        {
            try
            {
                string command = Directory.GetParent(Assembly.GetExecutingAssembly().Location) + "\\" + pCommand + ".exe";

                Process process = new Process();
                process.StartInfo.FileName = command;
                process.StartInfo.Arguments = arg;
                process.Start();
                return true;
            }
            catch (Exception ex)
            {
                this.ErrorDescripcion = ex.Message;
                log(ex.Message);
                return false;
            }
        }
        //
        static void log(string estado)
        {
            StreamWriter log;

            if (!File.Exists(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\logfile.txt"))
                log = new StreamWriter(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\logfile.txt");
            else
                log = File.AppendText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\logfile.txt");

            log.WriteLine(DateTime.Now);
            log.WriteLine(estado);
            log.WriteLine("------------------------------------------------------------------------");
            log.WriteLine();

            log.Close();
        }
        //
        protected virtual void Dispose(bool disposing)
        {

            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                CloseHandle(handle);
                handle = IntPtr.Zero;
                disposed = true;

            }
        }
        //

    }//Finaliza clase
}//Finaliza namespace
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NCQ_B;
using System.Data;
using System.Security.Cryptography;
using System.IO;

namespace LauncherQupos
{
    public class Perfil : ErrorHandler
    {
        private AccesoDatos oAccesoDatos;
        private string login;
        private PerfilGestoraP oPerfilGestora;
        //
        public Perfil(AccesoDatos poAccesoDatos, string plogin)
        {
            this.oAccesoDatos = poAccesoDatos;
            this.login = plogin;
            this.oPerfilGestora = new PerfilGestoraP(this.oAccesoDatos, plogin);
        }

        //============================================================================================
        #region Mantenimientos
        //
        public void ModificarConfig(string pTray, string pModuloVentana, string pKeyCerrar)
        {
            try
            {
                string pHw = LicenciasGestion.Hardwares.getHardware();
                if (LicenciasGestion.Hardwares.IsError)
                {
                    this.isError = true;
                    this.errorDescripcion = LicenciasGestion.Hardwares.ErrorDescripcion;
                    return;
                }
                Compania oCompania = new Compania(oAccesoDatos, "");
                string cliente = OptenerCliente();
                if (oCompania.IsError)
                {
                    return;
                }
                oPerfilGestora.ModificarSystemtray("QPOS", pHw, pTray, pModuloVentana, pKeyCerrar);
                if (oPerfilGestora.IsError)
                {
                    this.isError = oPerfilGestora.IsError;
                    this.errorDescripcion = oPerfilGestora.ErrorDescripcion;
                    return;
                }
            }
            catch
            {
                this.isError = oPerfilGestora.IsError;
                this.errorDescripcion = oPerfilGestora.ErrorDescripcion;
                return;
            }
        }
        //
        #endregion

        //============================================================================================
        #region Consultas
        //
        public DataSet TraerDatosModulo(string perfil, string tag)
        {
            DataSet dsetDatos = this.oPerfilGestora.TraerDatosModulo(perfil, tag);
            this.isError = this.oPerfilGestora.IsError;
            this.errorDescripcion = this.oPerfilGestora.ErrorDescripcion;
            return dsetDatos;
        }
        //
        public DataSet TraerDatosSubModulo(string modulo)
        {
            DataSet dsetDatos = this.oPerfilGestora.TraerDatosSubModulo(modulo);
            if (this.oPerfilGestora.IsError)
            {
                this.isError = this.oPerfilGestora.IsError;
                this.errorDescripcion = this.oPerfilGestora.ErrorDescripcion;
                return null;
            }
            return dsetDatos;
        }
        //
        public DataSet TraerDatosPantallas(string modulo)
        {
            DataSet dsetDatos = this.oPerfilGestora.TraerDatosPantallas(modulo,"");
            this.isError = this.oPerfilGestora.IsError;
            this.errorDescripcion = this.oPerfilGestora.ErrorDescripcion;
            return dsetDatos;
        }
        //
        public string TraerPerfil()
        {
            try
            {
                DataSet dsetDatos = new DataSet();
                string pHw = LicenciasGestion.Hardwares.getHardware();
                if (LicenciasGestion.Hardwares.IsError)
                {
                    this.isError = true;
                    this.errorDescripcion = LicenciasGestion.Hardwares.ErrorDescripcion;
                    return null;
                }
                //
                string cliente = OptenerCliente();
                if (IsError)
                    return "3";
                //
                dsetDatos = oPerfilGestora.TraerPerfil("QPOS", pHw);
                if (oPerfilGestora.IsError)
                {
                    this.isError = oPerfilGestora.IsError;
                    this.errorDescripcion = oPerfilGestora.ErrorDescripcion;
                    return "3";
                }
                string vPerfil = "3";
                try
                {
                    vPerfil = dsetDatos.Tables[0].Rows[0][0].ToString();
                }
                catch
                {
                    vPerfil = "3";
                }
                vPerfil = eq.dcdo(vPerfil, pHw, cliente);
                if (vPerfil == "E")
                    vPerfil = "2";
                else if (vPerfil == "F")
                    vPerfil = "1";
                else
                    vPerfil = "3";
                return vPerfil;
            }
            catch (Exception ex)
            {
                this.isError = true;
                this.errorDescripcion = ex.Message;
                return "3";
            }
        }
        //
        public string TraerLicencia()
        {
            try
            {
                DataSet dsetDatos = new DataSet();
                string pHw = LicenciasGestion.Hardwares.getHardware();
                if (LicenciasGestion.Hardwares.IsError)
                {
                    this.isError = true;
                    this.errorDescripcion = LicenciasGestion.Hardwares.ErrorDescripcion;
                    return null;
                }
                string cliente = OptenerCliente();
                if (IsError)
                    return "";
                dsetDatos = oPerfilGestora.TraerLicencia("QPOS", pHw);
                if (oPerfilGestora.IsError)
                {
                    this.isError = oPerfilGestora.IsError;
                    this.errorDescripcion = oPerfilGestora.ErrorDescripcion;
                    return "";
                }
                string vLic = "";
                try
                {
                    vLic = dsetDatos.Tables[0].Rows[0][0].ToString();
                }
                catch{
                    vLic = "";
                }
                return vLic;
            }
            catch (Exception ex)
            {
                this.isError = true;
                this.errorDescripcion = ex.Message;
                return "";
            }
        }
        //
        public DataSet TraerConfiguracion()
        {
            try
            {
                DataSet dsetDatos = new DataSet();
                string pHw = LicenciasGestion.Hardwares.getHardware();
                if (LicenciasGestion.Hardwares.IsError)
                {
                    this.isError = true;
                    this.errorDescripcion = LicenciasGestion.Hardwares.ErrorDescripcion;
                    return null;
                }
                dsetDatos = oPerfilGestora.TraerConfiguracion("QPOS", pHw);
                if (oPerfilGestora.IsError)
                {
                    this.isError = oPerfilGestora.IsError;
                    this.errorDescripcion = oPerfilGestora.ErrorDescripcion;
                    return null;
                }
                return dsetDatos;
            }
            catch (Exception ex)
            {
                this.isError = true;
                this.errorDescripcion = ex.Message;
                return null;
            }
        }
        //
        #endregion

        //============================================================================================
        #region Métodos miscelaneos
        //
        private class eq
        {
            #region Desencriptar

            // Método para desencriptar un texto encriptado.
            public static string dcdo(string code, string pBase, string pSaltValue)
            {
                return dcdo(code, pBase, pSaltValue, "MD5",
                    1, "@1B2c3D4e5F6g7H8", 64);
            }

            // Método para desencriptar un texto encriptado    
            public static string dcdo(string code, string passBase,
                string saltValue, string hashAlgorithm, int passwordIterations,
                string initVector, int keySize)
            {
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
                byte[] cipherTextBytes = Convert.FromBase64String(code);
                PasswordDeriveBytes password = new PasswordDeriveBytes(passBase,
                    saltValueBytes, hashAlgorithm, passwordIterations);
                byte[] keyBytes = password.GetBytes(keySize / 8);
                RijndaelManaged symmetricKey = new RijndaelManaged()
                {
                    Mode = CipherMode.CBC
                };
                ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes,
                    initVectorBytes);
                MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
                CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor,
                    CryptoStreamMode.Read);
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0,
                    plainTextBytes.Length);
                memoryStream.Close();
                cryptoStream.Close();
                string plainText = Encoding.UTF8.GetString(plainTextBytes, 0,
                    decryptedByteCount);
                return plainText;
            }
            #endregion
        }
        //
        private string OptenerCliente()
        {
            DataSet dsetDatos = new DataSet();
            NCQ_B.Compania oCompania = new NCQ_B.Compania(this.oAccesoDatos, this.login);
            dsetDatos = oCompania.OptenerCliente();
            if (oCompania.IsError)
            {
                this.isError = true;
                this.errorDescripcion = oCompania.ErrorDescription;
                return "";
            }
            if (dsetDatos.Tables.Count > 0)
            {
                return dsetDatos.Tables[0].Rows[0][0].ToString();
            }
            return "";
        }
        //
        #endregion

    }//END CLASS
}//END NAMESCAPCE          
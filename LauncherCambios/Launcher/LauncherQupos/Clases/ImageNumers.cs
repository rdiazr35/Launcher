﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace LauncherQupos
{
    public class ImageNumers
    {
        public ImageNumers()
        { }
        //
        public Image GetImageGray(string number)
        {
            switch (number)
            {
                case "0":
                    return global::LauncherQupos.Properties.Resources.zero_gray;
                case "1":
                    return global::LauncherQupos.Properties.Resources.one_gray;
                case "2":
                    return global::LauncherQupos.Properties.Resources.two_gray;
                case "3":
                    return global::LauncherQupos.Properties.Resources.three_gray;
                case "4":
                    return global::LauncherQupos.Properties.Resources.four_gray;
                case "5":
                    return global::LauncherQupos.Properties.Resources.five_gray;
                case "6":
                    return global::LauncherQupos.Properties.Resources.six_gray;
                case "7":
                    return global::LauncherQupos.Properties.Resources.seven_gray;
                case "8":
                    return global::LauncherQupos.Properties.Resources.eight_gray; 
                case "9":
                    return global::LauncherQupos.Properties.Resources.nine_gray;
                case "10":
                    return global::LauncherQupos.Properties.Resources.ten_gray;
                case "11":
                    return global::LauncherQupos.Properties.Resources.eleven_gray;
                default:
                    return global::LauncherQupos.Properties.Resources.zero_gray;
            }
        }
        //
        public Image GetImageWhite(string number)
        {
            switch (number)
            {
                case "0":
                    return Properties.Resources.zero;
                case "1":
                    return Properties.Resources.one;
                case "2":
                    return Properties.Resources.two;
                case "3":
                    return Properties.Resources.three;
                case "4":
                    return Properties.Resources.four;
                case "5":
                    return Properties.Resources.five;
                case "6":
                    return Properties.Resources.six;
                case "7":
                    return Properties.Resources.seven;
                case "8":
                    return Properties.Resources.eight;
                case "9":
                    return Properties.Resources.nine;
                case "10":
                    return Properties.Resources.ten;
                case "11":
                    return Properties.Resources.eleven;
                default:
                    return Properties.Resources.zero;
            }
        }

    }//END CLASS
}//END NAMESPACE
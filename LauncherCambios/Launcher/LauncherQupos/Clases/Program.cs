﻿using LauncherQupos;
using NCQ_B;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace LauncherQupos
{
    public static class Program
    {
        private static AccesoDatos oAccesoDatos;
        public static string login = "";
        public static string compania = "";
        public static string pathHH = "";
        public static string pathOrigenPC = "";
        public static string pathDestinoPC = "";
        public static string contrasena = "";
        public static string nomCompania = "";
        public static string usuarioBD = "";
        public static string ODBC = "";
        public static string dataBase = "";
        public static string perfilQupos = "3";

        public static DataSet dsetParams = null;

        public static DataSet dsetMensajes;
        public static string modulo = "Qupos";
        public static string version = "3.3.0";
        public static string built = "3.3.0.0";
        public static string fechaInstalacion = "";
        public static string fechaLiberacion = "";
        //
        public static string urlSoporte = "http://ncqservices.com:82/wsSoporte/wsColaSoporte.asmx?WSDL";
        public static string licencia = "";
        public static string perfil = "3";

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //=========================================================================================
            #region Acceso viejo
            NCQ_V.frmLogin ofrmLogin = new NCQ_V.frmLogin(Program.modulo, Program.version, oAccesoDatos);
            ofrmLogin.ShowDialog();
            if (ofrmLogin.Ok)
            {
                oAccesoDatos = ofrmLogin.OAccesoDatos;
                DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(ofrmLogin.Skin_sistema);
                Program.login = ofrmLogin.Login;
                Program.compania = ofrmLogin.Cod_compania;
                Program.nomCompania = ofrmLogin.Nom_compania;
                Program.usuarioBD = ofrmLogin.UsuarioBD;
                Program.contrasena = ofrmLogin.ContrasenaBD;
                Program.ODBC = ofrmLogin.ODBC;
                //
                NCQ_V.Program.login = Program.login;
                NCQ_V.Program.oAccesoDatos = oAccesoDatos;
                //Application.Run(new Form1());

                Application.Run(new frmPrincipal(oAccesoDatos));
                //frmPrincipal ofrm = new frmPrincipal(oAccesoDatos);
                //ofrm.ShowDialog();
            }
            else
            {
                try
                {
                    if (oAccesoDatos.estado())
                        oAccesoDatos.desconectar();
                }
                catch { }
                try
                {
                    Application.Exit();
                }
                catch { }
            }
            //
            #endregion
        }
    }
}

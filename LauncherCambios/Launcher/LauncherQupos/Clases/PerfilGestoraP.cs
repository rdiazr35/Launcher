﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NCQ_B;
using System.Data;
using NpgsqlTypes;

namespace LauncherQupos
{
    public class PerfilGestoraP : ErrorHandler
    {
        private NCQ_B.AccesoDatos oAccesoDatos;
        private string login;

        public PerfilGestoraP(AccesoDatos poAccesoDatos, string plogin)
        {
            this.oAccesoDatos = poAccesoDatos;
            this.login = plogin;
        }

        //============================================================================================
        #region Mantenimientos
        //
        public void ModificarSystemtray(string pProduct, string pHw, string tray, string pModuloVentana, string pKeyCerrar)
        {
            string filtro = "";
            if (pHw != "")
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += " hw = @hw ";
            }
            if (pProduct != "")
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += " pro = @product ";
            }

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("UPDATE " + this.oAccesoDatos.Schema + "estacion_trabajo SET system_tray=@tray, ");
            sql.AppendLine(" modulo_ventana = @modulo_ventana, ");
            sql.AppendLine(" key_cerrar = @key_cerrar ");
            if (!filtro.Equals(""))
                sql.AppendLine(" WHERE " + filtro);

            ParametrosPostgres oParametros = new ParametrosPostgres();
            oParametros.AddNull(NpgsqlDbType.Varchar, "@product", pProduct);
            oParametros.AddNull(NpgsqlDbType.Varchar, "@hw", pHw);
            oParametros.AddNull(NpgsqlDbType.Varchar, "@tray", tray);
            oParametros.AddNull(NpgsqlDbType.Varchar, "@modulo_ventana", pModuloVentana);
            oParametros.AddNull(NpgsqlDbType.Varchar, "@key_cerrar", pKeyCerrar);

            this.oAccesoDatos.ejecutarSQL(sql.ToString(), oParametros.GetParameters());
            if (this.oAccesoDatos.IsError)
            {
                this.isError = true;
                this.errorDescripcion = this.oAccesoDatos.ErrorDescripcion;
            }
            return;
        }
        //
        #endregion

        //============================================================================================
        #region Consultas
        //
        public DataSet TraerDatos()
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT ");
            sql.AppendLine("    *, ");
            sql.AppendLine("    '' as aux_bd ");
            sql.AppendLine("FROM ");
            sql.AppendLine("    " + this.oAccesoDatos.Schema + "launcher_perfil ");
            sql.AppendLine("ORDER BY ");
            sql.AppendLine("    descripcion");

            DataSet dsetDatos = this.oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "launcher_perfil");
            this.isError = this.oAccesoDatos.IsError;
            this.errorDescripcion = this.oAccesoDatos.ErrorDescripcion;
            return dsetDatos;
        }
        //
        public DataSet TraerDatosModulo(string perfil, string tag)
        {
            string filtro = "";
            if (!perfil.Equals(""))
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += "p.perfil = @perfil ";
            }
            //
            if (!tag.Equals(""))
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += "m.tag = @tag ";
            }

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT ");
            sql.AppendLine("    m.*, ");
            sql.AppendLine("    '' as aux_bd ");
            sql.AppendLine("FROM ");
            sql.AppendLine("    " + this.oAccesoDatos.Schema + "launcher_modulo m ");
            sql.AppendLine("    LEFT OUTER JOIN " + this.oAccesoDatos.Schema + "launcher_perfil_modulo p ON(m.modulo = p.modulo) ");
            if (!filtro.Equals(""))
                sql.AppendLine("WHERE " + filtro);
            sql.AppendLine("ORDER BY ");
            sql.AppendLine("    m.orden asc");

            ParametrosPostgres oParametros = new ParametrosPostgres();
            oParametros.AddNull(NpgsqlDbType.Numeric, "@perfil", perfil.Trim());
            oParametros.AddNull(NpgsqlDbType.Varchar, "@tag", tag.Trim());

            DataSet dsetDatos = this.oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "launcher_modulo", oParametros.GetParameters());
            this.isError = this.oAccesoDatos.IsError;
            this.errorDescripcion = this.oAccesoDatos.ErrorDescripcion;
            return dsetDatos;
        }
        //
        public DataSet TraerDatosSubModulo(string modulo)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT ");
            sql.AppendLine("    s.*, ");
            sql.AppendLine("    m.tag, ");
            sql.AppendLine("    '' as aux_bd ");
            sql.AppendLine("FROM ");
            sql.AppendLine("    " + this.oAccesoDatos.Schema + "launcher_modulo_submodulo s ");
            sql.AppendLine("    LEFT OUTER JOIN " + this.oAccesoDatos.Schema + "launcher_modulo m ON(s.modulo = m.modulo)");
            if (!modulo.Equals(""))
            {
                sql.AppendLine("WHERE ");
                sql.AppendLine("    s.modulo in (" + modulo.Trim() + ") ");
            }
            sql.AppendLine("ORDER BY ");
            sql.AppendLine("    s.orden asc");

            ParametrosPostgres oParametros = new ParametrosPostgres();
            //oParametros.AddNull(NpgsqlDbType.Numeric, "@modulo", modulo.Trim());

            DataSet dsetDatos = this.oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "launcher_modulo_submodulo", oParametros.GetParameters());
            this.isError = this.oAccesoDatos.IsError;
            this.errorDescripcion = this.oAccesoDatos.ErrorDescripcion;
            return dsetDatos;
        }
        //
        public DataSet TraerDatosPantallas(string modulo, string submodulo)
        {
            string filtro = "";
            if (!modulo.Equals(""))
                filtro += "AND lmsp.modulo = @modulo ";
            if (!submodulo.Equals(""))
                filtro += "AND lmsp.submodulo = @submodulo ";
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT  ");
            sql.AppendLine("    lmsp.*, lsm.orden as orden_submodulo, lm.orden as orden_modulo, ");
            sql.AppendLine("    lsm.descripcion as descripcion_submodulo, lm.descripcion as descripcion_modulo ");
            sql.AppendLine("FROM ");
            sql.AppendLine("	" + this.oAccesoDatos.Schema + "launcher_modulo_submodulo_pantalla lmsp ");
            sql.AppendLine("    LEFT JOIN " + this.oAccesoDatos.Schema + "launcher_modulo_submodulo lsm ON lsm.submodulo = lmsp.submodulo");
            sql.AppendLine("    LEFT JOIN " + this.oAccesoDatos.Schema + "launcher_modulo lm ON lm.modulo = lmsp.modulo ");
            sql.AppendLine("WHERE  ");
            sql.AppendLine("	lmsp.activo = 'S' ");
            if (!filtro.Equals(""))
                sql.AppendLine(filtro);
            //
            sql.AppendLine("ORDER BY lm.orden, lsm.orden, lmsp.descripcion");

            ParametrosPostgres oParametros = new ParametrosPostgres();
            oParametros.AddNull(NpgsqlDbType.Numeric, "@modulo", modulo.Trim());
            oParametros.AddNull(NpgsqlDbType.Numeric, "@submodulo", submodulo.Trim());

            DataSet dsetDatos = this.oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "pantallas", oParametros.GetParameters());
            this.isError = this.oAccesoDatos.IsError;
            this.errorDescripcion = this.oAccesoDatos.ErrorDescripcion;
            return dsetDatos;
        }
        //
        public DataSet TraerPerfil(string pProduct, string pHw)
        {
            string filtro = "";
            if (!pProduct.Equals(""))
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += "pro = @product ";
            }
            //
            if (!pHw.Equals(""))
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += "hw = @hw ";
            }

            StringBuilder sql = new StringBuilder();
            sql.AppendLine(" select tl from " + this.oAccesoDatos.Schema + "estacion_trabajo ");
            if (!filtro.Equals(""))
                sql.AppendLine(" where " + filtro);

            ParametrosPostgres oParametros = new ParametrosPostgres();
            oParametros.AddNull(NpgsqlDbType.Varchar, "@product", pProduct.Trim());
            oParametros.AddNull(NpgsqlDbType.Varchar, "@hw", pHw.Trim());

            DataSet oDset = new DataSet();
            oDset = this.oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "perfil", oParametros.GetParameters());
            if (this.oAccesoDatos.IsError)
            {
                this.isError = true;
                this.errorDescripcion = this.oAccesoDatos.ErrorDescripcion;
            }
            return oDset;
        }
        //
        public DataSet TraerLicencia(string pProduct, string pHw)
        {
            string filtro = "";
            if (!pProduct.Equals(""))
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += "pro = @product ";
            }
            if (!pHw.Equals(""))
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += "hw = @hw ";
            }

            StringBuilder sql = new StringBuilder();
            sql.AppendLine(" select lic from " + this.oAccesoDatos.Schema + "estacion_trabajo ");
            if (!filtro.Equals(""))
                sql.AppendLine(" where " + filtro);

            ParametrosPostgres oParametros = new ParametrosPostgres();
            oParametros.AddNull(NpgsqlDbType.Varchar, "@product", pProduct.Trim());
            oParametros.AddNull(NpgsqlDbType.Varchar, "@hw", pHw.Trim());

            DataSet oDset = new DataSet();
            oDset = this.oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "perfil", oParametros.GetParameters());
            if (this.oAccesoDatos.IsError)
            {
                this.isError = true;
                this.errorDescripcion = this.oAccesoDatos.ErrorDescripcion;
            }

            return oDset;
        }
        //
        public DataSet TraerConfiguracion(string pProduct, string pHw)
        {
            string filtro = "";
            if (!pProduct.Equals(""))
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += "pro = @product ";
            }
            if (!pHw.Equals(""))
            {
                if (filtro != "")
                    filtro += " and ";
                filtro += "hw = @hw ";
            }

            StringBuilder sql = new StringBuilder();
            sql.AppendLine(" select * from " + this.oAccesoDatos.Schema + "estacion_trabajo ");
            if (!filtro.Equals(""))
                sql.AppendLine(" where " + filtro);

            ParametrosPostgres oParametros = new ParametrosPostgres();
            oParametros.AddNull(NpgsqlDbType.Varchar, "@product", pProduct.Trim());
            oParametros.AddNull(NpgsqlDbType.Varchar, "@hw", pHw.Trim());

            DataSet oDset = new DataSet();
            oDset = this.oAccesoDatos.ejecutarConsultaSQL(sql.ToString(), "perfil", oParametros.GetParameters());
            if (this.oAccesoDatos.IsError)
            {
                this.isError = true;
                this.errorDescripcion = this.oAccesoDatos.ErrorDescripcion;
            }

            return oDset;
        }
        //
        #endregion

    }//END CLASS
}//END NAMESCAPCE    
﻿namespace LauncherQupos
{
    partial class frmSoporte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSoporte));
            this.lblSoporte = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.chkEmergencia = new Bunifu.Framework.UI.BunifuCheckbox();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lblClose = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.mmMensaje = new DevExpress.XtraEditors.MemoEdit();
            this.btnEnviar = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.mmMensaje.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSoporte
            // 
            this.lblSoporte.AutoSize = true;
            this.lblSoporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoporte.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblSoporte.Location = new System.Drawing.Point(13, 13);
            this.lblSoporte.Name = "lblSoporte";
            this.lblSoporte.Size = new System.Drawing.Size(66, 20);
            this.lblSoporte.TabIndex = 0;
            this.lblSoporte.Text = "Soporte";
            // 
            // chkEmergencia
            // 
            this.chkEmergencia.BackColor = System.Drawing.SystemColors.Highlight;
            this.chkEmergencia.ChechedOffColor = System.Drawing.SystemColors.Highlight;
            this.chkEmergencia.Checked = false;
            this.chkEmergencia.CheckedOnColor = System.Drawing.SystemColors.Highlight;
            this.chkEmergencia.ForeColor = System.Drawing.Color.White;
            this.chkEmergencia.Location = new System.Drawing.Point(17, 51);
            this.chkEmergencia.Name = "chkEmergencia";
            this.chkEmergencia.Size = new System.Drawing.Size(20, 20);
            this.chkEmergencia.TabIndex = 1;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(42, 52);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(127, 16);
            this.bunifuCustomLabel1.TabIndex = 2;
            this.bunifuCustomLabel1.Text = "Es una emergencia.";
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(12, 23);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(601, 35);
            this.bunifuSeparator1.TabIndex = 4;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // lblClose
            // 
            this.lblClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblClose.BackColor = System.Drawing.Color.White;
            this.lblClose.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClose.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblClose.Location = new System.Drawing.Point(592, -3);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(20, 30);
            this.lblClose.TabIndex = 6;
            this.lblClose.Text = "x";
            this.lblClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblClose.Click += new System.EventHandler(this.lblClose_Click);
            this.lblClose.MouseLeave += new System.EventHandler(this.lblClose_MouseLeave);
            this.lblClose.MouseHover += new System.EventHandler(this.lblClose_MouseHover);
            // 
            // mmMensaje
            // 
            this.mmMensaje.Location = new System.Drawing.Point(17, 77);
            this.mmMensaje.Name = "mmMensaje";
            this.mmMensaje.Properties.Appearance.BorderColor = System.Drawing.SystemColors.Highlight;
            this.mmMensaje.Properties.Appearance.Options.UseBorderColor = true;
            this.mmMensaje.Properties.Appearance.Options.UseTextOptions = true;
            this.mmMensaje.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.mmMensaje.Size = new System.Drawing.Size(595, 77);
            this.mmMensaje.TabIndex = 7;
            // 
            // btnEnviar
            // 
            this.btnEnviar.ActiveBorderThickness = 1;
            this.btnEnviar.ActiveCornerRadius = 20;
            this.btnEnviar.ActiveFillColor = System.Drawing.SystemColors.Highlight;
            this.btnEnviar.ActiveForecolor = System.Drawing.Color.White;
            this.btnEnviar.ActiveLineColor = System.Drawing.SystemColors.Highlight;
            this.btnEnviar.BackColor = System.Drawing.Color.White;
            this.btnEnviar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEnviar.BackgroundImage")));
            this.btnEnviar.ButtonText = "Enviar";
            this.btnEnviar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEnviar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviar.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnEnviar.IdleBorderThickness = 1;
            this.btnEnviar.IdleCornerRadius = 20;
            this.btnEnviar.IdleFillColor = System.Drawing.Color.White;
            this.btnEnviar.IdleForecolor = System.Drawing.SystemColors.Highlight;
            this.btnEnviar.IdleLineColor = System.Drawing.SystemColors.Highlight;
            this.btnEnviar.Location = new System.Drawing.Point(495, 162);
            this.btnEnviar.Margin = new System.Windows.Forms.Padding(5);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(118, 35);
            this.btnEnviar.TabIndex = 9;
            this.btnEnviar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // frmSoporte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(625, 206);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.mmMensaje);
            this.Controls.Add(this.lblClose);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.chkEmergencia);
            this.Controls.Add(this.lblSoporte);
            this.Controls.Add(this.bunifuSeparator1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSoporte";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmSoporte";
            this.Load += new System.EventHandler(this.frmSoporte_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mmMensaje.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomLabel lblSoporte;
        private Bunifu.Framework.UI.BunifuCheckbox chkEmergencia;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private Bunifu.Framework.UI.BunifuCustomLabel lblClose;
        private DevExpress.XtraEditors.MemoEdit mmMensaje;
        private Bunifu.Framework.UI.BunifuThinButton2 btnEnviar;
    }
}
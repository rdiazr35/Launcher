﻿using Bunifu.Framework.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NCQ_B;
using System.Diagnostics;
using System.Threading;

namespace LauncherQupos
{
    public partial class frmPrincipal : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private DataSet dsetModulos;
        private DataSet dsetSubModulos;
        private DataSet dsetPantallas;
        private NCQ_B.AccesoDatos oAccesoDatos;
        Thread tOptions;
        MenuStrip MainMenu = new MenuStrip();
        Color colorPerfil3 = System.Drawing.ColorTranslator.FromHtml("#00AA00");
        Color colorPerfil2 = System.Drawing.ColorTranslator.FromHtml("#0081FC");
        Color colorPerfil1 = System.Drawing.ColorTranslator.FromHtml("#FF5B00");
        //
        List<Panel> menuPrincipal = new List<Panel>();

        public frmPrincipal(AccesoDatos poAccesoDatso)
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            //
            InitializeComponent();
            this.oAccesoDatos = poAccesoDatso;                                         
            LauncherQupos.Perfil oPerfil = new LauncherQupos.Perfil(this.oAccesoDatos, Program.login);
            string perfil = oPerfil.TraerPerfil();
            Program.perfil = perfil;
            string licencia = oPerfil.TraerLicencia();
            Program.licencia = licencia;
            //this.CargarModulos(Program.perfil);
            //this.CreateMenuPrincipal();
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.CargarBackgroud();
            
        }           
        //
        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            new ToolTip().SetToolTip(btnAnydesk, "Anydesk");
            new ToolTip().SetToolTip(btnAyudaQupos, "Ayuda Qupos");
            new ToolTip().SetToolTip(btnActualizador, "Actualizador");
            new ToolTip().SetToolTip(btnDiagnostico, "Diagnóstico");
            new ToolTip().SetToolTip(btnPreferencias, "Prefrencias");
            new ToolTip().SetToolTip(btnSoporte, "Soporte");
            this.Menu();
            this.ptBuscar.Focus();
            this.ptBuscar.Select();
        }
        //


        //====================================================================
        #region Métodos miscelaneos
        //
        private void CargarModulos(string perfil)
        {
            this.dsetModulos = new DataSet();
            LauncherQupos.Perfil oPerfil = new LauncherQupos.Perfil(this.oAccesoDatos, Program.login);
            this.dsetModulos = oPerfil.TraerDatosModulo(perfil, "");
            if (oPerfil.IsError)
            {
                new NCQ_V.frmErrores(oPerfil.ErrorDescripcion, "Error cargando los datos.");
                return;
            }
        }
        //
        private void CargarSubModulos(string modulo)
        {
            LauncherQupos.Perfil oPerfil = new LauncherQupos.Perfil(this.oAccesoDatos, Program.login);
            this.dsetSubModulos = oPerfil.TraerDatosSubModulo(modulo);
            if (oPerfil.IsError)
            {
                new NCQ_V.frmErrores(oPerfil.ErrorDescripcion, "Error cargando los datos.");
                return;
            }
        }
        //
        private void CargarPantallas()
        {
            this.dsetPantallas = new DataSet();
            LauncherQupos.Perfil oPerfil = new LauncherQupos.Perfil(this.oAccesoDatos, Program.login);
            this.dsetPantallas = oPerfil.TraerDatosPantallas("");
            if (oPerfil.IsError)
            {
                new NCQ_V.frmErrores(oPerfil.ErrorDescripcion, "Error cargando los datos.");
                return;
            }
        }
        //
        //private void CreateMenuPrincipal()
        //{
        //    foreach (DataRow item in this.dsetModulos.Tables[0].Rows)
        //    {
        //        Bunifu.Framework.UI.BunifuFlatButton btn = new Bunifu.Framework.UI.BunifuFlatButton();
        //        btn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
        //        btn.BackColor = System.Drawing.Color.White;
        //        btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
        //        btn.BorderRadius = 0;
        //        btn.ButtonText = item["descripcion"].ToString();
        //        btn.Cursor = System.Windows.Forms.Cursors.Hand;
        //        btn.DisabledColor = System.Drawing.Color.Gray;
        //        btn.Dock = System.Windows.Forms.DockStyle.Top;
        //        btn.Font = new System.Drawing.Font("Montserrat", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //        btn.Iconcolor = System.Drawing.Color.Transparent;
        //        //
        //        ImageNumers oImage = new ImageNumers();
        //        btn.Iconimage = oImage.GetImageGray(item["orden"].ToString());
        //        //
        //        btn.Iconimage_right = null;
        //        btn.Iconimage_right_Selected = null;
        //        btn.Iconimage_Selected = null;
        //        btn.IconMarginLeft = 0;
        //        btn.IconMarginRight = 0;
        //        btn.IconRightVisible = true;
        //        btn.IconRightZoom = 0D;
        //        btn.IconVisible = true;
        //        btn.IconZoom = 100D;
        //        btn.IsTab = false;
        //        btn.Location = new System.Drawing.Point(0, 225);
        //        btn.Margin = new System.Windows.Forms.Padding(4, 10, 4, 10);
        //        btn.Name = "btn" + item["tag"].ToString();
        //        btn.Normalcolor = System.Drawing.Color.White;
        //        btn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
        //        btn.OnHoverTextColor = System.Drawing.Color.White;
        //        btn.selected = false;
        //        btn.Size = new System.Drawing.Size(280, 45);
        //        btn.TabIndex = Convert.ToInt32(item["orden"].ToString());
        //        btn.Tag = item["tag"].ToString();
        //        btn.Text = item["descripcion"].ToString();
        //        btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        //        btn.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //        btn.TextFont = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //        btn.IconZoom = 100D;
        //        btn.Click += new System.EventHandler(this.btnModulos_Click);
        //    }
        //}
        //
        private void CargarBackgroud()
        {
            int index = 0;
            ImageBackGround oImage = new ImageBackGround();
            Image [] fotos = oImage.Fondos;
            timer.Tick += (sender, args) => {
                ptFondos.Image = fotos[index];
                index++;
                if (index.Equals(8))
                    index = 0;
            };
            timer.Start();
        }
        //
        //
        public delegate void OutSelect<T1, T2>(T1 a, out T2 b);
        private void Menu()
        {
            //this.pnlMenu.Visible = false;
            this.pnlMenu.Controls.Clear();
            //
            if (dsetModulos == null)
            {
                this.CargarModulos(Program.perfil);
                //
                string pModulo = "";

                OutSelect<string, DataRow[]> selAction = SelectSubModulo;
                OutSelect<string, DataRow[]> selPantalla = SelectPantalla;

                foreach (DataRow dr in dsetModulos.Tables[0].Rows)
                {
                    menuPrincipal.Add(FlyweightItemMenu.MakeItemModulo(dr["descripcion"].ToString(), dr["orden"].ToString(), Menu,
                        selAction, dr["modulo"].ToString(), CargaMenu, selPantalla, EjecutarPantalla));
                    //
                    pModulo += dr["modulo"].ToString()+ ",";
                }
                //
                if (pModulo != "")
                    pModulo = pModulo.Substring(0, pModulo.Length - 1);
                //
                this.CargarSubModulos(pModulo);
                this.CargarPantallas();
            }
            this.CargaMenu(menuPrincipal);
        }
        //
        private void SelectSubModulo(string select, out DataRow[] dr)
        {
            dr = this.dsetSubModulos.Tables[0].Select(select);
        }
        //
        private void SelectPantalla(string select, out DataRow[] dr)
        {
            dr = this.dsetPantallas.Tables[0].Select(select);
        }
        //
        private void EjecutarPantalla(string tag)
        {

        }
        //
        private void CargaMenu(List<Panel> menuPrincipal)
        {
            this.tOptions = new Thread(new ThreadStart(() =>
            {
                this.CargaMenuNoThread(menuPrincipal);
            }));
            this.tOptions.Start();
        }
        //
        private void CargaMenuNoThread(List<Panel> menuPrincipal)
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.Refresh();
            });
            //
            this.pnlMenu.Invoke((MethodInvoker)delegate
            {
                this.pnlMenu.Controls.AddRange(menuPrincipal.ToArray<Control>());
            });
            //
            this.Invoke((MethodInvoker)delegate
            {
                this.lblEspere.Visible = false;
                //this.pnlMenu.Visible = true;
            });
        }
        //
        #endregion

        //====================================================================
        #region Métodos componentes
        //
        private void pnlMenu_ControlAdded(object sender, ControlEventArgs e)
        {
           e.Control.BringToFront();
        }
        //
        private void edtBuscar_KeyDown(object sender, EventArgs e)
        {
            /*
            Bunifu.Framework.UI.BunifuTextbox txt = (BunifuTextbox)sender;
            if (!edtBuscar.text.Equals(""))
            {
                Perfil oPerfil = new Perfil(this.oAccesoDatos, Program.login);
                dsetPantallas = oPerfil.TraerDatosPantallas();
                if (oPerfil.IsError)
                {
                    new NCQ_V.frmErrores(oPerfil.ErrorDescripcion, "Error cargando los datos.");
                    return;
                }
            }  
            */
        }
        //
        private void lblClose_MouseHover(object sender, EventArgs e)
        {
            this.lblClose.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMaximizar_MouseHover(object sender, EventArgs e)
        {
            this.lblMaximizar.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMinimizar_MouseHover(object sender, EventArgs e)
        {
            this.lblMinimizar.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblMinimizar_MouseLeave(object sender, EventArgs e)
        {
            this.lblMinimizar.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void lblMaximizar_MouseLeave(object sender, EventArgs e)
        {
            this.lblMaximizar.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void lblClose_MouseLeave(object sender, EventArgs e)
        {
            this.lblClose.ForeColor = Color.White;
            this.Cursor = Cursors.Default;
        }
        //
        private void picture_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }
        //
        private void picture_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
        //
        private void frmPrincipal_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        //
        private void edtSearch_TextChanged(object sender, EventArgs e)
        {
            if (this.edtSearch.Text.ToString().Equals(""))
            {
                this.Menu();
                return;
            }
            //
            //Thread t = new Thread(new ThreadStart(() => {
            try
            {
                string auxBusqueda = "";
                //this.BeginInvoke((MethodInvoker) delegate{
                    auxBusqueda = this.edtSearch.Text.ToString();
                    this.pnlMenu.Controls.Clear();
                //});
                //
                
                String busqueda = "%" + auxBusqueda + "%";
                DataRow[] result = dsetPantallas.Tables[0].Select("descripcion like ('" + busqueda + "')", "descripcion asc");
                //
                //
                List<Panel> paneles = new List<Panel>();
                string modulo = null;
                string subModulo = null;
                //
                string textModulo = null;
                string textSubModulo = null;
                //
                foreach (DataRow dr in result)
                {
                    if (modulo != dr["modulo"].ToString())
                    {
                        modulo = dr["modulo"].ToString();
                        //DataRow[] mod = dsetModulos.Tables[0].Select("modulo = " + modulo + "");
                        //textModulo = mod[0]["descripcion"].ToString();
                        textModulo = dr["descripcion_modulo"].ToString();
                    }
                    //
                    if (subModulo != dr["submodulo"].ToString())
                    {
                        subModulo = dr["submodulo"].ToString();
                        //DataRow[] sub = dsetSubModulos.Tables[0].Select("submodulo = " + subModulo + "");
                        //textSubModulo = sub[0]["descripcion"].ToString();
                        textSubModulo = dr["descripcion_submodulo"].ToString();
                        paneles.Add(FlyweightItemMenu.MakeItemSubModulo(textModulo + " - " + textSubModulo, "", Menu));
                    }
                    //
                    paneles.Add(FlyweightItemMenu.MakeItemPantalla(dr["descripcion"].ToString(), "", EjecutarPantalla));
                }
                //
                if (paneles.Count > 0)
                    this.CargaMenu(paneles);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            /*}));
            t.Start();*/
            //
            
            
        }
        //
        #endregion

        //====================================================================
        #region Métodos botones
        //
        private void lblMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //
        private void lblMaximizar_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                this.Size = new Size(900, 600);
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                this.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;
                this.WindowState = FormWindowState.Maximized;
            }
        }
        //
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //
        private void pnTop_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                this.Size = new Size(900, 600);
            }
            else if (WindowState == FormWindowState.Normal)
            {
                this.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size;
                this.WindowState = FormWindowState.Maximized;
            }
        }
        //
        private void btnAyudaQupos_Click(object sender, EventArgs e)
        {
            Process.Start(new ProcessStartInfo("http://www.qupos.com/ayudaQupos/"));
        }
        //
        private void btnActualizador_Click(object sender, EventArgs e)
        {
            Acceso oAccesos = new Acceso();
            oAccesos.exeCom("RunUpdater");
        }
        //
        private void btnDiagnostico_Click(object sender, EventArgs e)
        {
            Acceso oAccesos = new Acceso();
            oAccesos.exeCom("Diagnóstico Qupos");
        }
        //
        private void btnAnydesk_Click(object sender, EventArgs e)
        {
            Acceso oAccesos = new Acceso();
            oAccesos.exeCom("AnyDesk");
        }
        //
        private void btnPreferencias_Click(object sender, EventArgs e)
        {
            frmPreferencias ofrmPreferencias = new frmPreferencias(this.oAccesoDatos);
            ofrmPreferencias.Show();
            //ofrmPreferencias.Dispose();
        }
        //
        private void btnSoporte_Click(object sender, EventArgs e)
        {
            frmSoporte ofrmSoporte = new frmSoporte();
            ofrmSoporte.ShowDialog();
            //ofrmSoporte.Dispose();
        }
        //
        String tag = "";
        private void btnModulos_Click(object sender, EventArgs e)
        {
        }
        //
        private void btnSubModulos_Click(object sender, EventArgs e)
        {
        }
        //
        #endregion

        
    }
}   
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LauncherQupos
{
    public partial class frmSoporte : Form
    {
        public frmSoporte()
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            InitializeComponent();
        }
        //
        private void frmSoporte_Load(object sender, EventArgs e)
        {
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Blue");
        }
        //
        //=========================================================================
        #region Métodos botones
        //
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //
        private void btnEnviar_Click(object sender, EventArgs e)
        {
            string emergencia = "N";
            if (chkEmergencia.Checked)
                emergencia = "S";
            string message = "";
            message = mmMensaje.Text.Trim();
            if (message.Equals(""))
                return;

            wsSoporte.wsColaSoporte oControlSoporte = new wsSoporte.wsColaSoporte();
            oControlSoporte.Url = Program.urlSoporte;
            string respuesta = oControlSoporte.reporte_atencion(Program.login, "CLI0000", Program.licencia, message, DateTime.Now, "",
                 null, "", "W", Program.login, emergencia);
            System.Data.DataSet dsRespuesta = ConvertXMLToDataSet(respuesta);
            if (dsRespuesta.Tables[0].Rows[0]["error"].ToString().Equals("S"))
            {
                MessageBox.Show("El reporte no pudo ser enviado." + Environment.NewLine + dsRespuesta.Tables[0].Rows[0]["detalle_error"].ToString(),
                    "Error", 0, MessageBoxIcon.Error);
                return;
            }
            //
            MessageBox.Show("El reporte se envio correctamente." + Environment.NewLine + dsRespuesta.Tables[0].Rows[0]["detalle_error"].ToString(),
                "Error", 0, MessageBoxIcon.Error);
            this.Close();
        }
        //
        #endregion

        //=========================================================================
        #region Métodos miscelaneos
        //
        private System.Data.DataSet ConvertXMLToDataSet(string xmlData)
        {
            StringReader stream = null;
            System.Xml.XmlTextReader reader = null;
            try
            {
                System.Data.DataSet xmlDS = new System.Data.DataSet();
                stream = new StringReader(xmlData);
                // Load the XmlTextReader from the stream
                reader = new System.Xml.XmlTextReader(stream);
                xmlDS.ReadXml(reader, System.Data.XmlReadMode.ReadSchema);
                return xmlDS;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }
        //
        #endregion

        //=========================================================================
        #region Métodos componentes
        //
        private void lblClose_MouseHover(object sender, EventArgs e)
        {
            this.lblClose.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblClose_MouseLeave(object sender, EventArgs e)
        {
            this.lblClose.ForeColor = System.Drawing.SystemColors.Highlight;
            this.Cursor = Cursors.Default;
        }
        //
        #endregion

    }//EndClass
}//End namespace
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LauncherQupos
{
    public partial class frmPreferencias : Form
    {
        NCQ_B.AccesoDatos oAccesoDatos;
        public frmPreferencias(NCQ_B.AccesoDatos oAccesoDatos)
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            InitializeComponent();
            this.oAccesoDatos = oAccesoDatos;
            this.RefrescarDatos();
        }
        //
        private void frmPreferencias_Load(object sender, EventArgs e)
        {
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Blue");
        }
        //
        //=========================================================================
        #region Métodos botones
        //
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //
        #endregion

        //=========================================================================
        #region Métodos miscelaneos
        //
        private void RefrescarDatos()
        {
            DataSet dsetDatos = new DataSet();
            LauncherQupos.Perfil oPerfil = new LauncherQupos.Perfil(this.oAccesoDatos, Program.login);
            dsetDatos = oPerfil.TraerConfiguracion();
            if (oPerfil.IsError)
            {
                MessageBox.Show(oPerfil.ErrorDescripcion);
                return;
            }
            bool modulo = false;
            if (dsetDatos.Tables[0].Rows[0]["modulo_ventana"].ToString().Equals("S"))
                modulo = true;
            this.chkMenuSecundario.Checked = modulo;
        }
        //
        #endregion

        //=========================================================================
        #region Métodos componentes
        //
        private void lblClose_MouseHover(object sender, EventArgs e)
        {
            this.lblClose.ForeColor = ColorTranslator.FromHtml("#E57E31");
            this.Cursor = Cursors.Hand;
        }
        //
        private void lblClose_MouseLeave(object sender, EventArgs e)
        {
            this.lblClose.ForeColor = System.Drawing.SystemColors.Highlight;
            this.Cursor = Cursors.Default;
        }
        //
        private void chkMenuSecundario_OnChange(object sender, EventArgs e)
        {
            string menu = "N";
            if (chkMenuSecundario.Checked)
                menu = "S";
            LauncherQupos.Perfil oPerfil = new LauncherQupos.Perfil(this.oAccesoDatos, Program.login);
            oPerfil.ModificarConfig("S", menu, "N");
            if (oPerfil.IsError)
            {
                MessageBox.Show(oPerfil.ErrorDescripcion);
                return;
            }

        }
        //
        #endregion


    }//EndClass
}//End namespace
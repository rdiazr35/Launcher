﻿namespace Activos
{
    partial class frmPerfil
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPerfil));
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repTipoCedula = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repCedula = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repTipoPago = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repBanco = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repDescBanco = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repActivo = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repFecha = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.pnlEncabezado = new System.Windows.Forms.Panel();
            this.imgIcono = new System.Windows.Forms.PictureBox();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.imgNCQ = new System.Windows.Forms.PictureBox();
            this.tlbarAcciones = new System.Windows.Forms.ToolStrip();
            this.btnNuevo = new System.Windows.Forms.ToolStripButton();
            this.btnSalvar = new System.Windows.Forms.ToolStripButton();
            this.btnBorrar = new System.Windows.Forms.ToolStripButton();
            this.btnRefrescar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimero = new System.Windows.Forms.ToolStripButton();
            this.btnAnterior = new System.Windows.Forms.ToolStripButton();
            this.btnSiguiente = new System.Windows.Forms.ToolStripButton();
            this.btnUltimo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExcel = new System.Windows.Forms.ToolStripSplitButton();
            this.btnExcelUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExcelSubUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnImprimir = new System.Windows.Forms.ToolStripSplitButton();
            this.btnImprimirUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnImprimirSubUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPreferencias = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCargarUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCargarSubUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnGuargarUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnGuardarSubUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnGuardarPrefenciaUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnGuardarPreferenciasSubUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnVistaPredefinidaUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnVistaPredefinidaSubUbicaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPanel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.splTipo = new DevExpress.XtraEditors.SplitContainerControl();
            this.dtgPerfil = new NCQ_V.MyGridControl();
            this.viewPerfil = new NCQ_V.MyGridView();
            this.colUbicaciones = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescripcionUbicacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActivo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repActivoBodega = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreadoPor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fecha_creacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repFechaBodega = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.modificado_por = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fecha_modificacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabNota = new NCQ_V.MyXtraTabControl();
            this.tabGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.tabGrid = new NCQ_V.MyXtraTabControl();
            this.tabModulos = new DevExpress.XtraTab.XtraTabPage();
            this.dtgModulos = new NCQ_V.MyGridControl();
            this.viewModulos = new NCQ_V.MyGridView();
            this.colUbicacionSubUbicacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubUbicacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescripcion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActivoUbicacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repchk = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreadoporUbicacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaCreacionUbicacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colModificadoporUbicacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaModificacionBodega = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repBodega = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repTipoLote = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.tlbarVehiculos = new System.Windows.Forms.ToolStrip();
            this.btnEditarSubUbicacion = new System.Windows.Forms.ToolStripButton();
            this.btnBorrarSubUbicacion = new System.Windows.Forms.ToolStripButton();
            this.grpDatos = new DevExpress.XtraEditors.GroupControl();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.chkActivo = new DevExpress.XtraEditors.CheckEdit();
            this.edtDescripcion = new DevExpress.XtraEditors.TextEdit();
            this.lblBodega = new DevExpress.XtraEditors.LabelControl();
            this.edtUbicaciones = new DevExpress.XtraEditors.TextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.dlgSave = new System.Windows.Forms.SaveFileDialog();
            this.Validacion = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.dlgAbrirPrefs = new System.Windows.Forms.OpenFileDialog();
            this.dlgGuardarPrefs = new System.Windows.Forms.SaveFileDialog();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTipoCedula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCedula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTipoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDescBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFecha.VistaTimeProperties)).BeginInit();
            this.pnlEncabezado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNCQ)).BeginInit();
            this.tlbarAcciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splTipo)).BeginInit();
            this.splTipo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPerfil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPerfil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repActivoBodega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFechaBodega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFechaBodega.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabNota)).BeginInit();
            this.tabNota.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabGrid)).BeginInit();
            this.tabGrid.SuspendLayout();
            this.tabModulos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgModulos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewModulos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repchk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDate.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBodega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTipoLote)).BeginInit();
            this.tlbarVehiculos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpDatos)).BeginInit();
            this.grpDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtDescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtUbicaciones.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Validacion)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 300;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repTipoCedula
            // 
            this.repTipoCedula.AutoHeight = false;
            this.repTipoCedula.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTipoCedula.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cédula de identidad", "I", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dimex", "R", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cédula juridica", "J", -1)});
            this.repTipoCedula.Name = "repTipoCedula";
            // 
            // repCedula
            // 
            this.repCedula.AutoHeight = false;
            this.repCedula.MaxLength = 100;
            this.repCedula.Name = "repCedula";
            // 
            // repTipoPago
            // 
            this.repTipoPago.AutoHeight = false;
            this.repTipoPago.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTipoPago.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Efectivo", "E", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Cheque", "C", -1)});
            this.repTipoPago.Name = "repTipoPago";
            // 
            // repBanco
            // 
            this.repBanco.AutoHeight = false;
            this.repBanco.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repBanco.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("cod_banco", "Banco"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripción")});
            this.repBanco.DisplayMember = "cod_banco";
            this.repBanco.Name = "repBanco";
            this.repBanco.NullText = "";
            this.repBanco.PopupWidth = 100;
            this.repBanco.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.repBanco.ValueMember = "cod_banco";
            // 
            // repDescBanco
            // 
            this.repDescBanco.AutoHeight = false;
            this.repDescBanco.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repDescBanco.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("descripcion", "Descripción"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("cod_banco", "Banco")});
            this.repDescBanco.DisplayMember = "descripcion";
            this.repDescBanco.Name = "repDescBanco";
            this.repDescBanco.NullText = "";
            this.repDescBanco.PopupWidth = 300;
            this.repDescBanco.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.repDescBanco.ValueMember = "cod_banco";
            // 
            // repActivo
            // 
            this.repActivo.AutoHeight = false;
            this.repActivo.DisplayValueChecked = "S";
            this.repActivo.DisplayValueUnchecked = "N";
            this.repActivo.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repActivo.Name = "repActivo";
            this.repActivo.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repActivo.ValueChecked = "S";
            this.repActivo.ValueUnchecked = "N";
            // 
            // repFecha
            // 
            this.repFecha.AutoHeight = false;
            this.repFecha.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repFecha.Mask.EditMask = "g";
            this.repFecha.Mask.UseMaskAsDisplayFormat = true;
            this.repFecha.Name = "repFecha";
            this.repFecha.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // pnlEncabezado
            // 
            this.pnlEncabezado.BackColor = System.Drawing.Color.White;
            this.pnlEncabezado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlEncabezado.Controls.Add(this.imgIcono);
            this.pnlEncabezado.Controls.Add(this.lblTitulo);
            this.pnlEncabezado.Controls.Add(this.imgNCQ);
            this.pnlEncabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlEncabezado.Location = new System.Drawing.Point(0, 0);
            this.pnlEncabezado.Name = "pnlEncabezado";
            this.pnlEncabezado.Size = new System.Drawing.Size(919, 51);
            this.pnlEncabezado.TabIndex = 0;
            // 
            // imgIcono
            // 
            this.imgIcono.Dock = System.Windows.Forms.DockStyle.Left;
            this.imgIcono.Location = new System.Drawing.Point(0, 0);
            this.imgIcono.Name = "imgIcono";
            this.imgIcono.Size = new System.Drawing.Size(53, 47);
            this.imgIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgIcono.TabIndex = 3;
            this.imgIcono.TabStop = false;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(57, 10);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(73, 29);
            this.lblTitulo.TabIndex = 2;
            this.lblTitulo.Text = "Perfil";
            // 
            // imgNCQ
            // 
            this.imgNCQ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgNCQ.Dock = System.Windows.Forms.DockStyle.Right;
            this.imgNCQ.Image = ((System.Drawing.Image)(resources.GetObject("imgNCQ.Image")));
            this.imgNCQ.Location = new System.Drawing.Point(805, 0);
            this.imgNCQ.Name = "imgNCQ";
            this.imgNCQ.Size = new System.Drawing.Size(110, 47);
            this.imgNCQ.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgNCQ.TabIndex = 1;
            this.imgNCQ.TabStop = false;
            // 
            // tlbarAcciones
            // 
            this.tlbarAcciones.AutoSize = false;
            this.tlbarAcciones.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tlbarAcciones.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tlbarAcciones.BackgroundImage")));
            this.tlbarAcciones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tlbarAcciones.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tlbarAcciones.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tlbarAcciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevo,
            this.btnSalvar,
            this.btnBorrar,
            this.btnRefrescar,
            this.toolStripSeparator1,
            this.btnPrimero,
            this.btnAnterior,
            this.btnSiguiente,
            this.btnUltimo,
            this.toolStripSeparator2,
            this.btnExcel,
            this.btnImprimir,
            this.btnPreferencias,
            this.btnPanel,
            this.toolStripSeparator3,
            this.btnSalir});
            this.tlbarAcciones.Location = new System.Drawing.Point(0, 51);
            this.tlbarAcciones.Name = "tlbarAcciones";
            this.tlbarAcciones.Size = new System.Drawing.Size(919, 43);
            this.tlbarAcciones.TabIndex = 1;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(46, 40);
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNuevo.ToolTipText = "Nuevo (Alt + N)";
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Image = ((System.Drawing.Image)(resources.GetObject("btnSalvar.Image")));
            this.btnSalvar.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(42, 40);
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalvar.ToolTipText = "Salvar  (Alt + S)";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Image = ((System.Drawing.Image)(resources.GetObject("btnBorrar.Image")));
            this.btnBorrar.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(46, 40);
            this.btnBorrar.Text = "&Borrar ";
            this.btnBorrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBorrar.ToolTipText = "Borrar (Alt + B)";
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // btnRefrescar
            // 
            this.btnRefrescar.Image = ((System.Drawing.Image)(resources.GetObject("btnRefrescar.Image")));
            this.btnRefrescar.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnRefrescar.Name = "btnRefrescar";
            this.btnRefrescar.Size = new System.Drawing.Size(59, 40);
            this.btnRefrescar.Text = "&Refrescar";
            this.btnRefrescar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnRefrescar.ToolTipText = "Refrescar  (Alt + R)";
            this.btnRefrescar.Click += new System.EventHandler(this.btnRefrescar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 43);
            // 
            // btnPrimero
            // 
            this.btnPrimero.Image = ((System.Drawing.Image)(resources.GetObject("btnPrimero.Image")));
            this.btnPrimero.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnPrimero.Name = "btnPrimero";
            this.btnPrimero.Size = new System.Drawing.Size(53, 40);
            this.btnPrimero.Text = "Primero";
            this.btnPrimero.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPrimero.Click += new System.EventHandler(this.btnPrimero_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.Image = ((System.Drawing.Image)(resources.GetObject("btnAnterior.Image")));
            this.btnAnterior.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(54, 40);
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.Image = ((System.Drawing.Image)(resources.GetObject("btnSiguiente.Image")));
            this.btnSiguiente.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(60, 40);
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // btnUltimo
            // 
            this.btnUltimo.Image = ((System.Drawing.Image)(resources.GetObject("btnUltimo.Image")));
            this.btnUltimo.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnUltimo.Name = "btnUltimo";
            this.btnUltimo.Size = new System.Drawing.Size(47, 40);
            this.btnUltimo.Text = "Último";
            this.btnUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnUltimo.Click += new System.EventHandler(this.btnUltimo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 43);
            // 
            // btnExcel
            // 
            this.btnExcel.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnExcelUbicaciones,
            this.btnExcelSubUbicaciones});
            this.btnExcel.Image = global::LauncherQupos.Properties.Resources._101;
            this.btnExcel.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(49, 40);
            this.btnExcel.Text = "Excel";
            this.btnExcel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnExcel.ButtonClick += new System.EventHandler(this.btnExcel_ButtonClick);
            // 
            // btnExcelUbicaciones
            // 
            this.btnExcelUbicaciones.Name = "btnExcelUbicaciones";
            this.btnExcelUbicaciones.Size = new System.Drawing.Size(163, 22);
            this.btnExcelUbicaciones.Text = "Ubicaciones";
            this.btnExcelUbicaciones.Click += new System.EventHandler(this.btnExcelUbicaciones_Click);
            // 
            // btnExcelSubUbicaciones
            // 
            this.btnExcelSubUbicaciones.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnExcelSubUbicaciones.Name = "btnExcelSubUbicaciones";
            this.btnExcelSubUbicaciones.Size = new System.Drawing.Size(163, 22);
            this.btnExcelSubUbicaciones.Text = "Sub-Ubicaciones";
            this.btnExcelSubUbicaciones.Click += new System.EventHandler(this.btnExcelSubUbicaciones_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnImprimirUbicaciones,
            this.btnImprimirSubUbicaciones});
            this.btnImprimir.Image = global::LauncherQupos.Properties.Resources._009;
            this.btnImprimir.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(69, 40);
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnImprimir.ButtonClick += new System.EventHandler(this.btnImprimir_ButtonClick);
            // 
            // btnImprimirUbicaciones
            // 
            this.btnImprimirUbicaciones.Name = "btnImprimirUbicaciones";
            this.btnImprimirUbicaciones.Size = new System.Drawing.Size(163, 22);
            this.btnImprimirUbicaciones.Text = "Ubicaciones";
            this.btnImprimirUbicaciones.Click += new System.EventHandler(this.btnImprimirUbicaciones_Click);
            // 
            // btnImprimirSubUbicaciones
            // 
            this.btnImprimirSubUbicaciones.Name = "btnImprimirSubUbicaciones";
            this.btnImprimirSubUbicaciones.Size = new System.Drawing.Size(163, 22);
            this.btnImprimirSubUbicaciones.Text = "Sub-Ubicaciones";
            this.btnImprimirSubUbicaciones.Click += new System.EventHandler(this.btnImprimirSubUbicaciones_Click);
            // 
            // btnPreferencias
            // 
            this.btnPreferencias.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator5,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.btnPreferencias.Image = ((System.Drawing.Image)(resources.GetObject("btnPreferencias.Image")));
            this.btnPreferencias.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnPreferencias.Name = "btnPreferencias";
            this.btnPreferencias.Size = new System.Drawing.Size(87, 40);
            this.btnPreferencias.Text = "Preferencias";
            this.btnPreferencias.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPreferencias.ButtonClick += new System.EventHandler(this.btnPreferencias_ButtonClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCargarUbicaciones,
            this.btnCargarSubUbicaciones});
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(235, 30);
            this.toolStripMenuItem1.Text = "Cargar";
            // 
            // btnCargarUbicaciones
            // 
            this.btnCargarUbicaciones.Name = "btnCargarUbicaciones";
            this.btnCargarUbicaciones.Size = new System.Drawing.Size(162, 22);
            this.btnCargarUbicaciones.Text = "Ubicaciones";
            this.btnCargarUbicaciones.Click += new System.EventHandler(this.btnCargarUbicaciones_Click);
            // 
            // btnCargarSubUbicaciones
            // 
            this.btnCargarSubUbicaciones.Name = "btnCargarSubUbicaciones";
            this.btnCargarSubUbicaciones.Size = new System.Drawing.Size(162, 22);
            this.btnCargarSubUbicaciones.Text = "Sub-ubicaciones";
            this.btnCargarSubUbicaciones.Click += new System.EventHandler(this.btnCargarSubUbicaciones_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(232, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnGuargarUbicaciones,
            this.btnGuardarSubUbicaciones});
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem2.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(235, 30);
            this.toolStripMenuItem2.Text = "Guardar";
            // 
            // btnGuargarUbicaciones
            // 
            this.btnGuargarUbicaciones.Name = "btnGuargarUbicaciones";
            this.btnGuargarUbicaciones.Size = new System.Drawing.Size(162, 22);
            this.btnGuargarUbicaciones.Text = "Ubicaciones";
            this.btnGuargarUbicaciones.Click += new System.EventHandler(this.btnGuargarUbicaciones_Click);
            // 
            // btnGuardarSubUbicaciones
            // 
            this.btnGuardarSubUbicaciones.Name = "btnGuardarSubUbicaciones";
            this.btnGuardarSubUbicaciones.Size = new System.Drawing.Size(162, 22);
            this.btnGuardarSubUbicaciones.Text = "Sub-ubicaciones";
            this.btnGuardarSubUbicaciones.Click += new System.EventHandler(this.btnGuardarSubUbicaciones_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnGuardarPrefenciaUbicaciones,
            this.btnGuardarPreferenciasSubUbicaciones});
            this.toolStripMenuItem3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem3.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(235, 30);
            this.toolStripMenuItem3.Text = "Guardar preferencia existente";
            // 
            // btnGuardarPrefenciaUbicaciones
            // 
            this.btnGuardarPrefenciaUbicaciones.Name = "btnGuardarPrefenciaUbicaciones";
            this.btnGuardarPrefenciaUbicaciones.Size = new System.Drawing.Size(162, 22);
            this.btnGuardarPrefenciaUbicaciones.Text = "Ubicaciones";
            this.btnGuardarPrefenciaUbicaciones.Click += new System.EventHandler(this.btnGuardarPrefenciaUbicaciones_Click);
            // 
            // btnGuardarPreferenciasSubUbicaciones
            // 
            this.btnGuardarPreferenciasSubUbicaciones.Name = "btnGuardarPreferenciasSubUbicaciones";
            this.btnGuardarPreferenciasSubUbicaciones.Size = new System.Drawing.Size(162, 22);
            this.btnGuardarPreferenciasSubUbicaciones.Text = "Sub-ubicaciones";
            this.btnGuardarPreferenciasSubUbicaciones.Click += new System.EventHandler(this.btnGuardarPreferenciasSubUbicaciones_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnVistaPredefinidaUbicaciones,
            this.btnVistaPredefinidaSubUbicaciones});
            this.toolStripMenuItem4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem4.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(235, 30);
            this.toolStripMenuItem4.Text = "Vista predefinida";
            // 
            // btnVistaPredefinidaUbicaciones
            // 
            this.btnVistaPredefinidaUbicaciones.Name = "btnVistaPredefinidaUbicaciones";
            this.btnVistaPredefinidaUbicaciones.Size = new System.Drawing.Size(162, 22);
            this.btnVistaPredefinidaUbicaciones.Text = "Ubicaciones";
            this.btnVistaPredefinidaUbicaciones.Click += new System.EventHandler(this.btnVistaPredefinidaBodega_Click);
            // 
            // btnVistaPredefinidaSubUbicaciones
            // 
            this.btnVistaPredefinidaSubUbicaciones.Name = "btnVistaPredefinidaSubUbicaciones";
            this.btnVistaPredefinidaSubUbicaciones.Size = new System.Drawing.Size(162, 22);
            this.btnVistaPredefinidaSubUbicaciones.Text = "Sub-ubicaciones";
            this.btnVistaPredefinidaSubUbicaciones.Click += new System.EventHandler(this.btnVistaPredefinidaUbicaciones_Click);
            // 
            // btnPanel
            // 
            this.btnPanel.Checked = true;
            this.btnPanel.CheckOnClick = true;
            this.btnPanel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnPanel.Image = ((System.Drawing.Image)(resources.GetObject("btnPanel.Image")));
            this.btnPanel.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnPanel.Name = "btnPanel";
            this.btnPanel.Size = new System.Drawing.Size(40, 40);
            this.btnPanel.Text = "Panel";
            this.btnPanel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPanel.Click += new System.EventHandler(this.btnPanel_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 43);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(33, 40);
            this.btnSalir.Text = "Sa&lir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.ToolTipText = "Salir (Alt + L)";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // splTipo
            // 
            this.splTipo.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splTipo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splTipo.Location = new System.Drawing.Point(0, 94);
            this.splTipo.Name = "splTipo";
            this.splTipo.Panel1.Controls.Add(this.dtgPerfil);
            this.splTipo.Panel1.Text = "Panel1";
            this.splTipo.Panel2.Controls.Add(this.tabNota);
            this.splTipo.Panel2.Text = "Panel2";
            this.splTipo.Size = new System.Drawing.Size(919, 350);
            this.splTipo.SplitterPosition = 457;
            this.splTipo.TabIndex = 2;
            // 
            // dtgPerfil
            // 
            this.dtgPerfil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPerfil.Location = new System.Drawing.Point(0, 0);
            this.dtgPerfil.LookAndFeel.SkinName = "Black";
            this.dtgPerfil.MainView = this.viewPerfil;
            this.dtgPerfil.Name = "dtgPerfil";
            this.dtgPerfil.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repFechaBodega,
            this.repActivoBodega});
            this.dtgPerfil.Size = new System.Drawing.Size(457, 350);
            this.dtgPerfil.TabIndex = 1;
            this.dtgPerfil.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewPerfil});
            // 
            // viewPerfil
            // 
            this.viewPerfil.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.viewPerfil.Appearance.EvenRow.Options.UseBackColor = true;
            this.viewPerfil.Appearance.FixedLine.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewPerfil.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewPerfil.Appearance.FocusedCell.Options.UseFont = true;
            this.viewPerfil.Appearance.FocusedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewPerfil.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.SteelBlue;
            this.viewPerfil.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewPerfil.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.viewPerfil.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewPerfil.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewPerfil.Appearance.FocusedRow.Options.UseFont = true;
            this.viewPerfil.Appearance.FocusedRow.Options.UseForeColor = true;
            this.viewPerfil.Appearance.GroupPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.viewPerfil.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightBlue;
            this.viewPerfil.Appearance.GroupPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewPerfil.Appearance.GroupPanel.Options.UseBackColor = true;
            this.viewPerfil.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewPerfil.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewPerfil.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.viewPerfil.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.viewPerfil.Appearance.OddRow.Options.UseBackColor = true;
            this.viewPerfil.Appearance.SelectedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewPerfil.Appearance.SelectedRow.Options.UseBackColor = true;
            this.viewPerfil.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.viewPerfil.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUbicaciones,
            this.colDescripcionUbicacion,
            this.colActivo,
            this.colCreadoPor,
            this.fecha_creacion,
            this.modificado_por,
            this.fecha_modificacion});
            this.viewPerfil.CustomizationFormBounds = new System.Drawing.Rectangle(215, 543, 208, 170);
            this.viewPerfil.GridControl = this.dtgPerfil;
            this.viewPerfil.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.viewPerfil.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "tipo_documento", null, "")});
            this.viewPerfil.IndicatorWidth = 55;
            this.viewPerfil.Name = "viewPerfil";
            this.viewPerfil.OptionsBehavior.AllowIncrementalSearch = true;
            this.viewPerfil.OptionsDetail.EnableMasterViewMode = false;
            this.viewPerfil.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.viewPerfil.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.viewPerfil.OptionsFilter.UseNewCustomFilterDialog = true;
            this.viewPerfil.OptionsLayout.Columns.StoreAllOptions = true;
            this.viewPerfil.OptionsLayout.Columns.StoreAppearance = true;
            this.viewPerfil.OptionsLayout.Columns.StoreLayout = false;
            this.viewPerfil.OptionsLayout.StoreAllOptions = true;
            this.viewPerfil.OptionsLayout.StoreAppearance = true;
            this.viewPerfil.OptionsPrint.AutoWidth = false;
            this.viewPerfil.OptionsPrint.ExpandAllGroups = false;
            this.viewPerfil.OptionsSelection.MultiSelect = true;
            this.viewPerfil.OptionsView.ColumnAutoWidth = false;
            this.viewPerfil.OptionsView.ShowAutoFilterRow = true;
            this.viewPerfil.OptionsView.ShowFooter = true;
            this.viewPerfil.PaintStyleName = "Skin";
            this.viewPerfil.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.view_CustomDrawRowIndicator);
            this.viewPerfil.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.view_CustomDrawCell);
            this.viewPerfil.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.view_CustomDrawGroupRow);
            this.viewPerfil.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.view_CellValueChanged);
            this.viewPerfil.MouseDown += new System.Windows.Forms.MouseEventHandler(this.view_MouseDown);
            // 
            // colUbicaciones
            // 
            this.colUbicaciones.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colUbicaciones.AppearanceHeader.Options.UseFont = true;
            this.colUbicaciones.AppearanceHeader.Options.UseTextOptions = true;
            this.colUbicaciones.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUbicaciones.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colUbicaciones.Caption = "Ubicaciones";
            this.colUbicaciones.FieldName = "activo_ubicacion";
            this.colUbicaciones.Name = "colUbicaciones";
            this.colUbicaciones.OptionsColumn.ReadOnly = true;
            this.colUbicaciones.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colUbicaciones.Width = 120;
            // 
            // colDescripcionUbicacion
            // 
            this.colDescripcionUbicacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colDescripcionUbicacion.AppearanceHeader.Options.UseFont = true;
            this.colDescripcionUbicacion.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescripcionUbicacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescripcionUbicacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDescripcionUbicacion.Caption = "Descripción";
            this.colDescripcionUbicacion.FieldName = "descripcion";
            this.colDescripcionUbicacion.Name = "colDescripcionUbicacion";
            this.colDescripcionUbicacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDescripcionUbicacion.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.colDescripcionUbicacion.Visible = true;
            this.colDescripcionUbicacion.VisibleIndex = 0;
            this.colDescripcionUbicacion.Width = 250;
            // 
            // colActivo
            // 
            this.colActivo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colActivo.AppearanceHeader.Options.UseFont = true;
            this.colActivo.AppearanceHeader.Options.UseTextOptions = true;
            this.colActivo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActivo.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colActivo.Caption = "Activo";
            this.colActivo.ColumnEdit = this.repActivoBodega;
            this.colActivo.FieldName = "activo";
            this.colActivo.Name = "colActivo";
            this.colActivo.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActivo.Visible = true;
            this.colActivo.VisibleIndex = 1;
            this.colActivo.Width = 150;
            // 
            // repActivoBodega
            // 
            this.repActivoBodega.AutoHeight = false;
            this.repActivoBodega.DisplayValueChecked = "S";
            this.repActivoBodega.DisplayValueUnchecked = "N";
            this.repActivoBodega.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repActivoBodega.Name = "repActivoBodega";
            this.repActivoBodega.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repActivoBodega.ValueChecked = "S";
            this.repActivoBodega.ValueGrayed = "";
            this.repActivoBodega.ValueUnchecked = "N";
            // 
            // colCreadoPor
            // 
            this.colCreadoPor.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colCreadoPor.AppearanceHeader.Options.UseFont = true;
            this.colCreadoPor.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreadoPor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreadoPor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCreadoPor.Caption = "Creado por";
            this.colCreadoPor.FieldName = "creado_por";
            this.colCreadoPor.Name = "colCreadoPor";
            this.colCreadoPor.OptionsColumn.ReadOnly = true;
            this.colCreadoPor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCreadoPor.Width = 130;
            // 
            // fecha_creacion
            // 
            this.fecha_creacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.fecha_creacion.AppearanceHeader.Options.UseFont = true;
            this.fecha_creacion.AppearanceHeader.Options.UseTextOptions = true;
            this.fecha_creacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fecha_creacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.fecha_creacion.Caption = "Fecha creación";
            this.fecha_creacion.ColumnEdit = this.repFechaBodega;
            this.fecha_creacion.FieldName = "fecha_creacion";
            this.fecha_creacion.Name = "fecha_creacion";
            this.fecha_creacion.OptionsColumn.ReadOnly = true;
            this.fecha_creacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.fecha_creacion.Width = 165;
            // 
            // repFechaBodega
            // 
            this.repFechaBodega.AutoHeight = false;
            this.repFechaBodega.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repFechaBodega.DisplayFormat.FormatString = "G";
            this.repFechaBodega.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repFechaBodega.EditFormat.FormatString = "G";
            this.repFechaBodega.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repFechaBodega.Mask.EditMask = "G";
            this.repFechaBodega.Mask.UseMaskAsDisplayFormat = true;
            this.repFechaBodega.Name = "repFechaBodega";
            this.repFechaBodega.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // modificado_por
            // 
            this.modificado_por.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.modificado_por.AppearanceHeader.Options.UseFont = true;
            this.modificado_por.AppearanceHeader.Options.UseTextOptions = true;
            this.modificado_por.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.modificado_por.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.modificado_por.Caption = "Modificado por";
            this.modificado_por.FieldName = "modificado_por";
            this.modificado_por.Name = "modificado_por";
            this.modificado_por.OptionsColumn.ReadOnly = true;
            this.modificado_por.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.modificado_por.Width = 130;
            // 
            // fecha_modificacion
            // 
            this.fecha_modificacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.fecha_modificacion.AppearanceHeader.Options.UseFont = true;
            this.fecha_modificacion.AppearanceHeader.Options.UseTextOptions = true;
            this.fecha_modificacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fecha_modificacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.fecha_modificacion.Caption = "Fecha modificación";
            this.fecha_modificacion.ColumnEdit = this.repFechaBodega;
            this.fecha_modificacion.FieldName = "fecha_modificacion";
            this.fecha_modificacion.Name = "fecha_modificacion";
            this.fecha_modificacion.OptionsColumn.ReadOnly = true;
            this.fecha_modificacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.fecha_modificacion.Width = 165;
            // 
            // tabNota
            // 
            this.tabNota.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabNota.HeaderButtons = ((DevExpress.XtraTab.TabButtons)((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next)));
            this.tabNota.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.Never;
            this.tabNota.Location = new System.Drawing.Point(0, 0);
            this.tabNota.LookAndFeel.SkinName = "Black";
            this.tabNota.Name = "tabNota";
            this.tabNota.PaintStyleName = "MyStyle";
            this.tabNota.SelectedTabPage = this.tabGeneral;
            this.tabNota.Size = new System.Drawing.Size(456, 350);
            this.tabNota.TabIndex = 0;
            this.tabNota.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabGeneral});
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.tabGrid);
            this.tabGeneral.Controls.Add(this.grpDatos);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Size = new System.Drawing.Size(449, 322);
            this.tabGeneral.Text = "Perfil";
            // 
            // tabGrid
            // 
            this.tabGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabGrid.HeaderButtons = ((DevExpress.XtraTab.TabButtons)((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next)));
            this.tabGrid.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.Never;
            this.tabGrid.Location = new System.Drawing.Point(0, 57);
            this.tabGrid.LookAndFeel.SkinName = "Black";
            this.tabGrid.Name = "tabGrid";
            this.tabGrid.PaintStyleName = "MyStyle";
            this.tabGrid.SelectedTabPage = this.tabModulos;
            this.tabGrid.Size = new System.Drawing.Size(449, 265);
            this.tabGrid.TabIndex = 16;
            this.tabGrid.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabModulos});
            // 
            // tabModulos
            // 
            this.tabModulos.Controls.Add(this.dtgModulos);
            this.tabModulos.Controls.Add(this.tlbarVehiculos);
            this.tabModulos.Name = "tabModulos";
            this.tabModulos.Size = new System.Drawing.Size(442, 237);
            this.tabModulos.Text = "Módulos";
            // 
            // dtgModulos
            // 
            this.dtgModulos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgModulos.Location = new System.Drawing.Point(0, 43);
            this.dtgModulos.LookAndFeel.SkinName = "Black";
            this.dtgModulos.MainView = this.viewModulos;
            this.dtgModulos.Name = "dtgModulos";
            this.dtgModulos.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repDate,
            this.repchk,
            this.repBodega,
            this.repTipoLote});
            this.dtgModulos.Size = new System.Drawing.Size(442, 194);
            this.dtgModulos.TabIndex = 119;
            this.dtgModulos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewModulos});
            // 
            // viewModulos
            // 
            this.viewModulos.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.viewModulos.Appearance.EvenRow.Options.UseBackColor = true;
            this.viewModulos.Appearance.FixedLine.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewModulos.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewModulos.Appearance.FocusedCell.Options.UseFont = true;
            this.viewModulos.Appearance.FocusedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewModulos.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.SteelBlue;
            this.viewModulos.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewModulos.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.viewModulos.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewModulos.Appearance.FocusedRow.Options.UseBackColor = true;
            this.viewModulos.Appearance.FocusedRow.Options.UseFont = true;
            this.viewModulos.Appearance.FocusedRow.Options.UseForeColor = true;
            this.viewModulos.Appearance.GroupPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.viewModulos.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightBlue;
            this.viewModulos.Appearance.GroupPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.viewModulos.Appearance.GroupPanel.Options.UseBackColor = true;
            this.viewModulos.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.viewModulos.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewModulos.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.viewModulos.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.viewModulos.Appearance.OddRow.Options.UseBackColor = true;
            this.viewModulos.Appearance.SelectedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.viewModulos.Appearance.SelectedRow.Options.UseBackColor = true;
            this.viewModulos.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.viewModulos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUbicacionSubUbicacion,
            this.colSubUbicacion,
            this.colDescripcion,
            this.colActivoUbicacion,
            this.colCreadoporUbicacion,
            this.colFechaCreacionUbicacion,
            this.colModificadoporUbicacion,
            this.colFechaModificacionBodega});
            this.viewModulos.CustomizationFormBounds = new System.Drawing.Rectangle(215, 543, 208, 170);
            this.viewModulos.GridControl = this.dtgModulos;
            this.viewModulos.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.viewModulos.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "tipo_documento", null, "")});
            this.viewModulos.IndicatorWidth = 55;
            this.viewModulos.Name = "viewModulos";
            this.viewModulos.OptionsBehavior.AllowIncrementalSearch = true;
            this.viewModulos.OptionsDetail.EnableMasterViewMode = false;
            this.viewModulos.OptionsFilter.MaxCheckedListItemCount = 10000;
            this.viewModulos.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.viewModulos.OptionsFilter.UseNewCustomFilterDialog = true;
            this.viewModulos.OptionsLayout.Columns.StoreAllOptions = true;
            this.viewModulos.OptionsLayout.Columns.StoreAppearance = true;
            this.viewModulos.OptionsLayout.Columns.StoreLayout = false;
            this.viewModulos.OptionsLayout.StoreAllOptions = true;
            this.viewModulos.OptionsLayout.StoreAppearance = true;
            this.viewModulos.OptionsPrint.AutoWidth = false;
            this.viewModulos.OptionsPrint.ExpandAllGroups = false;
            this.viewModulos.OptionsSelection.MultiSelect = true;
            this.viewModulos.OptionsView.ColumnAutoWidth = false;
            this.viewModulos.OptionsView.ShowAutoFilterRow = true;
            this.viewModulos.OptionsView.ShowFooter = true;
            this.viewModulos.PaintStyleName = "Skin";
            this.viewModulos.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.view_CustomDrawRowIndicator);
            this.viewModulos.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.view_CustomDrawCell);
            this.viewModulos.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.view_CustomDrawGroupRow);
            this.viewModulos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.view_MouseDown);
            // 
            // colUbicacionSubUbicacion
            // 
            this.colUbicacionSubUbicacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colUbicacionSubUbicacion.AppearanceHeader.Options.UseFont = true;
            this.colUbicacionSubUbicacion.AppearanceHeader.Options.UseTextOptions = true;
            this.colUbicacionSubUbicacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUbicacionSubUbicacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colUbicacionSubUbicacion.Caption = "Ubicación";
            this.colUbicacionSubUbicacion.FieldName = "descripcion_ubicacion";
            this.colUbicacionSubUbicacion.Name = "colUbicacionSubUbicacion";
            this.colUbicacionSubUbicacion.OptionsColumn.ReadOnly = true;
            this.colUbicacionSubUbicacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colUbicacionSubUbicacion.Width = 200;
            // 
            // colSubUbicacion
            // 
            this.colSubUbicacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colSubUbicacion.AppearanceHeader.Options.UseFont = true;
            this.colSubUbicacion.AppearanceHeader.Options.UseTextOptions = true;
            this.colSubUbicacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSubUbicacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSubUbicacion.Caption = "Sub-ubicación";
            this.colSubUbicacion.FieldName = "activo_sub_ubicacion";
            this.colSubUbicacion.Name = "colSubUbicacion";
            this.colSubUbicacion.OptionsColumn.ReadOnly = true;
            this.colSubUbicacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colSubUbicacion.Width = 120;
            // 
            // colDescripcion
            // 
            this.colDescripcion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colDescripcion.AppearanceHeader.Options.UseFont = true;
            this.colDescripcion.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescripcion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescripcion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDescripcion.Caption = "Descripción";
            this.colDescripcion.FieldName = "descripcion";
            this.colDescripcion.Name = "colDescripcion";
            this.colDescripcion.OptionsColumn.ReadOnly = true;
            this.colDescripcion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDescripcion.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.colDescripcion.Visible = true;
            this.colDescripcion.VisibleIndex = 0;
            this.colDescripcion.Width = 250;
            // 
            // colActivoUbicacion
            // 
            this.colActivoUbicacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colActivoUbicacion.AppearanceHeader.Options.UseFont = true;
            this.colActivoUbicacion.AppearanceHeader.Options.UseTextOptions = true;
            this.colActivoUbicacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActivoUbicacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colActivoUbicacion.Caption = "Activo";
            this.colActivoUbicacion.ColumnEdit = this.repchk;
            this.colActivoUbicacion.FieldName = "activo";
            this.colActivoUbicacion.Name = "colActivoUbicacion";
            this.colActivoUbicacion.OptionsColumn.ReadOnly = true;
            this.colActivoUbicacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colActivoUbicacion.Visible = true;
            this.colActivoUbicacion.VisibleIndex = 1;
            this.colActivoUbicacion.Width = 200;
            // 
            // repchk
            // 
            this.repchk.AutoHeight = false;
            this.repchk.DisplayValueChecked = "S";
            this.repchk.DisplayValueUnchecked = "N";
            this.repchk.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repchk.Name = "repchk";
            this.repchk.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repchk.ValueChecked = "S";
            this.repchk.ValueGrayed = "";
            this.repchk.ValueUnchecked = "N";
            // 
            // colCreadoporUbicacion
            // 
            this.colCreadoporUbicacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colCreadoporUbicacion.AppearanceHeader.Options.UseFont = true;
            this.colCreadoporUbicacion.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreadoporUbicacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreadoporUbicacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCreadoporUbicacion.Caption = "Creado por";
            this.colCreadoporUbicacion.FieldName = "creado_por";
            this.colCreadoporUbicacion.Name = "colCreadoporUbicacion";
            this.colCreadoporUbicacion.OptionsColumn.ReadOnly = true;
            this.colCreadoporUbicacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colCreadoporUbicacion.Width = 130;
            // 
            // colFechaCreacionUbicacion
            // 
            this.colFechaCreacionUbicacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colFechaCreacionUbicacion.AppearanceHeader.Options.UseFont = true;
            this.colFechaCreacionUbicacion.AppearanceHeader.Options.UseTextOptions = true;
            this.colFechaCreacionUbicacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFechaCreacionUbicacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFechaCreacionUbicacion.Caption = "Fecha creación";
            this.colFechaCreacionUbicacion.ColumnEdit = this.repDate;
            this.colFechaCreacionUbicacion.FieldName = "fecha_creacion";
            this.colFechaCreacionUbicacion.Name = "colFechaCreacionUbicacion";
            this.colFechaCreacionUbicacion.OptionsColumn.ReadOnly = true;
            this.colFechaCreacionUbicacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colFechaCreacionUbicacion.Width = 165;
            // 
            // repDate
            // 
            this.repDate.AutoHeight = false;
            this.repDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repDate.DisplayFormat.FormatString = "G";
            this.repDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repDate.EditFormat.FormatString = "G";
            this.repDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repDate.Mask.EditMask = "G";
            this.repDate.Mask.UseMaskAsDisplayFormat = true;
            this.repDate.Name = "repDate";
            this.repDate.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // colModificadoporUbicacion
            // 
            this.colModificadoporUbicacion.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colModificadoporUbicacion.AppearanceHeader.Options.UseFont = true;
            this.colModificadoporUbicacion.AppearanceHeader.Options.UseTextOptions = true;
            this.colModificadoporUbicacion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colModificadoporUbicacion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colModificadoporUbicacion.Caption = "Modificado por";
            this.colModificadoporUbicacion.FieldName = "modificado_por";
            this.colModificadoporUbicacion.Name = "colModificadoporUbicacion";
            this.colModificadoporUbicacion.OptionsColumn.ReadOnly = true;
            this.colModificadoporUbicacion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colModificadoporUbicacion.Width = 130;
            // 
            // colFechaModificacionBodega
            // 
            this.colFechaModificacionBodega.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colFechaModificacionBodega.AppearanceHeader.Options.UseFont = true;
            this.colFechaModificacionBodega.AppearanceHeader.Options.UseTextOptions = true;
            this.colFechaModificacionBodega.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFechaModificacionBodega.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFechaModificacionBodega.Caption = "Fecha modificación";
            this.colFechaModificacionBodega.ColumnEdit = this.repDate;
            this.colFechaModificacionBodega.FieldName = "fecha_modificacion";
            this.colFechaModificacionBodega.Name = "colFechaModificacionBodega";
            this.colFechaModificacionBodega.OptionsColumn.ReadOnly = true;
            this.colFechaModificacionBodega.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colFechaModificacionBodega.Width = 165;
            // 
            // repBodega
            // 
            this.repBodega.AutoHeight = false;
            this.repBodega.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repBodega.DisplayMember = "descripcion";
            this.repBodega.Name = "repBodega";
            this.repBodega.NullText = "";
            this.repBodega.PopupWidth = 400;
            this.repBodega.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.repBodega.ValueMember = "bodega";
            // 
            // repTipoLote
            // 
            this.repTipoLote.AutoHeight = false;
            this.repTipoLote.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTipoLote.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Inicial", "I", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Sublote proceso", "SP", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Sublote carga a silo", "SC", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Granel pergamino", "GP", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Granel oro", "GO", -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Unidades", "U", -1)});
            this.repTipoLote.Name = "repTipoLote";
            // 
            // tlbarVehiculos
            // 
            this.tlbarVehiculos.AutoSize = false;
            this.tlbarVehiculos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tlbarVehiculos.BackgroundImage")));
            this.tlbarVehiculos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tlbarVehiculos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tlbarVehiculos.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tlbarVehiculos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnEditarSubUbicacion,
            this.btnBorrarSubUbicacion});
            this.tlbarVehiculos.Location = new System.Drawing.Point(0, 0);
            this.tlbarVehiculos.Name = "tlbarVehiculos";
            this.tlbarVehiculos.Size = new System.Drawing.Size(442, 43);
            this.tlbarVehiculos.TabIndex = 118;
            // 
            // btnEditarSubUbicacion
            // 
            this.btnEditarSubUbicacion.Image = global::LauncherQupos.Properties.Resources._037;
            this.btnEditarSubUbicacion.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnEditarSubUbicacion.Name = "btnEditarSubUbicacion";
            this.btnEditarSubUbicacion.Size = new System.Drawing.Size(41, 40);
            this.btnEditarSubUbicacion.Text = "Editar";
            this.btnEditarSubUbicacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditarSubUbicacion.ToolTipText = "Editar";
            this.btnEditarSubUbicacion.Click += new System.EventHandler(this.btnEditarSubUbicacion_Click);
            // 
            // btnBorrarSubUbicacion
            // 
            this.btnBorrarSubUbicacion.Image = ((System.Drawing.Image)(resources.GetObject("btnBorrarSubUbicacion.Image")));
            this.btnBorrarSubUbicacion.ImageTransparentColor = System.Drawing.Color.Black;
            this.btnBorrarSubUbicacion.Name = "btnBorrarSubUbicacion";
            this.btnBorrarSubUbicacion.Size = new System.Drawing.Size(43, 40);
            this.btnBorrarSubUbicacion.Text = "Borrar";
            this.btnBorrarSubUbicacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBorrarSubUbicacion.ToolTipText = "Borrar";
            this.btnBorrarSubUbicacion.Click += new System.EventHandler(this.btnBorrarSubUbicacion_Click);
            // 
            // grpDatos
            // 
            this.grpDatos.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.grpDatos.Controls.Add(this.lblDescripcion);
            this.grpDatos.Controls.Add(this.chkActivo);
            this.grpDatos.Controls.Add(this.edtDescripcion);
            this.grpDatos.Controls.Add(this.lblBodega);
            this.grpDatos.Controls.Add(this.edtUbicaciones);
            this.grpDatos.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDatos.Location = new System.Drawing.Point(0, 0);
            this.grpDatos.Name = "grpDatos";
            this.grpDatos.ShowCaption = false;
            this.grpDatos.Size = new System.Drawing.Size(449, 57);
            this.grpDatos.TabIndex = 17;
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.AutoSize = true;
            this.lblDescripcion.Location = new System.Drawing.Point(9, 31);
            this.lblDescripcion.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(66, 13);
            this.lblDescripcion.TabIndex = 8;
            this.lblDescripcion.Text = "Descripción:";
            // 
            // chkActivo
            // 
            this.chkActivo.EditValue = true;
            this.chkActivo.Location = new System.Drawing.Point(196, 7);
            this.chkActivo.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Properties.Caption = "Activo";
            this.chkActivo.Properties.DisplayValueChecked = "S";
            this.chkActivo.Properties.DisplayValueUnchecked = "N";
            this.chkActivo.Properties.LookAndFeel.SkinName = "Black";
            this.chkActivo.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chkActivo.Properties.ValueChecked = "S";
            this.chkActivo.Properties.ValueUnchecked = "N";
            this.chkActivo.Size = new System.Drawing.Size(56, 18);
            this.chkActivo.TabIndex = 7;
            // 
            // edtDescripcion
            // 
            this.edtDescripcion.Location = new System.Drawing.Point(81, 28);
            this.edtDescripcion.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.edtDescripcion.Name = "edtDescripcion";
            this.edtDescripcion.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(254)))), ((int)(((byte)(225)))));
            this.edtDescripcion.Properties.Appearance.Options.UseBackColor = true;
            this.edtDescripcion.Properties.LookAndFeel.SkinName = "Black";
            this.edtDescripcion.Properties.MaxLength = 300;
            this.edtDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.edtDescripcion.Size = new System.Drawing.Size(291, 20);
            this.edtDescripcion.TabIndex = 9;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Este valor no puede ser nulo";
            conditionValidationRule1.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
            this.Validacion.SetValidationRule(this.edtDescripcion, conditionValidationRule1);
            // 
            // lblBodega
            // 
            this.lblBodega.Location = new System.Drawing.Point(12, 10);
            this.lblBodega.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.lblBodega.Name = "lblBodega";
            this.lblBodega.Size = new System.Drawing.Size(24, 13);
            this.lblBodega.TabIndex = 5;
            this.lblBodega.Text = "Perfil";
            // 
            // edtUbicaciones
            // 
            this.edtUbicaciones.Location = new System.Drawing.Point(81, 7);
            this.edtUbicaciones.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.edtUbicaciones.Name = "edtUbicaciones";
            this.edtUbicaciones.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.edtUbicaciones.Properties.Appearance.Options.UseBackColor = true;
            this.edtUbicaciones.Properties.LookAndFeel.SkinName = "Black";
            this.edtUbicaciones.Properties.MaxLength = 20;
            this.edtUbicaciones.Properties.ReadOnly = true;
            this.edtUbicaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.edtUbicaciones.Size = new System.Drawing.Size(109, 20);
            this.edtUbicaciones.TabIndex = 6;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.MaxLength = 30;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.DisplayFormat.FormatString = "G";
            this.repositoryItemDateEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit3.EditFormat.FormatString = "G";
            this.repositoryItemDateEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit3.Mask.EditMask = "G";
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            this.repositoryItemDateEdit3.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // dlgSave
            // 
            this.dlgSave.DefaultExt = "xls";
            this.dlgSave.Filter = "Archivos de Excel(*.xls)|*.xls";
            this.dlgSave.FilterIndex = 0;
            // 
            // Validacion
            // 
            this.Validacion.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual;
            // 
            // dlgAbrirPrefs
            // 
            this.dlgAbrirPrefs.DefaultExt = "xml";
            this.dlgAbrirPrefs.Filter = "Archivo XML|*.xml";
            this.dlgAbrirPrefs.Title = "Abrir";
            // 
            // dlgGuardarPrefs
            // 
            this.dlgGuardarPrefs.DefaultExt = "xml";
            this.dlgGuardarPrefs.Filter = "Archivo XML|*.xml";
            this.dlgGuardarPrefs.Title = "Guardar como";
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Black";
            // 
            // frmPerfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 444);
            this.Controls.Add(this.splTipo);
            this.Controls.Add(this.tlbarAcciones);
            this.Controls.Add(this.pnlEncabezado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmPerfil";
            this.Text = "Perfil";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPerfil_FormClosing);
            this.Load += new System.EventHandler(this.frmPerfil_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPerfil_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTipoCedula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCedula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTipoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDescBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFecha.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFecha)).EndInit();
            this.pnlEncabezado.ResumeLayout(false);
            this.pnlEncabezado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgIcono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNCQ)).EndInit();
            this.tlbarAcciones.ResumeLayout(false);
            this.tlbarAcciones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splTipo)).EndInit();
            this.splTipo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPerfil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPerfil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repActivoBodega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFechaBodega.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFechaBodega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabNota)).EndInit();
            this.tabNota.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabGrid)).EndInit();
            this.tabGrid.ResumeLayout(false);
            this.tabModulos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgModulos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewModulos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repchk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDate.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBodega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTipoLote)).EndInit();
            this.tlbarVehiculos.ResumeLayout(false);
            this.tlbarVehiculos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpDatos)).EndInit();
            this.grpDatos.ResumeLayout(false);
            this.grpDatos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtDescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtUbicaciones.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Validacion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlEncabezado;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.PictureBox imgNCQ;
        private System.Windows.Forms.ToolStrip tlbarAcciones;
        private System.Windows.Forms.ToolStripButton btnNuevo;
        private System.Windows.Forms.ToolStripButton btnSalvar;
        private System.Windows.Forms.ToolStripButton btnBorrar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnPrimero;
        private System.Windows.Forms.ToolStripButton btnAnterior;
        private System.Windows.Forms.ToolStripButton btnSiguiente;
        private System.Windows.Forms.ToolStripButton btnUltimo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnPanel;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private DevExpress.XtraEditors.SplitContainerControl splTipo;
        private NCQ_V.MyXtraTabControl tabNota;
        private DevExpress.XtraTab.XtraTabPage tabGeneral;
        private System.Windows.Forms.SaveFileDialog dlgSave;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider Validacion;
        private System.Windows.Forms.OpenFileDialog dlgAbrirPrefs;
        private System.Windows.Forms.SaveFileDialog dlgGuardarPrefs;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private System.Windows.Forms.ToolStripButton btnRefrescar;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repTipoCedula;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repCedula;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repTipoPago;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repBanco;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repDescBanco;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repActivo;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repFecha;
        public NCQ_V.MyGridControl dtgPerfil;
        private NCQ_V.MyGridView viewPerfil;
        private DevExpress.XtraGrid.Columns.GridColumn colUbicaciones;
        private DevExpress.XtraGrid.Columns.GridColumn colDescripcionUbicacion;
        private DevExpress.XtraGrid.Columns.GridColumn colActivo;
        private DevExpress.XtraGrid.Columns.GridColumn colCreadoPor;
        private DevExpress.XtraGrid.Columns.GridColumn fecha_creacion;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repFechaBodega;
        private DevExpress.XtraGrid.Columns.GridColumn modificado_por;
        private DevExpress.XtraGrid.Columns.GridColumn fecha_modificacion;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repActivoBodega;
        private System.Windows.Forms.ToolStripSplitButton btnExcel;
        private System.Windows.Forms.ToolStripMenuItem btnExcelSubUbicaciones;
        private System.Windows.Forms.ToolStripSplitButton btnImprimir;
        private System.Windows.Forms.ToolStripMenuItem btnImprimirSubUbicaciones;
        private System.Windows.Forms.ToolStripSplitButton btnPreferencias;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem btnCargarSubUbicaciones;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem btnGuardarSubUbicaciones;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem btnGuardarPreferenciasSubUbicaciones;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private NCQ_V.MyXtraTabControl tabGrid;
        private DevExpress.XtraTab.XtraTabPage tabModulos;
        private System.Windows.Forms.ToolStrip tlbarVehiculos;
        private System.Windows.Forms.ToolStripButton btnEditarSubUbicacion;
        private System.Windows.Forms.ToolStripButton btnBorrarSubUbicacion;
        private DevExpress.XtraEditors.GroupControl grpDatos;
        private System.Windows.Forms.Label lblDescripcion;
        private DevExpress.XtraEditors.CheckEdit chkActivo;
        private DevExpress.XtraEditors.TextEdit edtDescripcion;
        private DevExpress.XtraEditors.LabelControl lblBodega;
        private DevExpress.XtraEditors.TextEdit edtUbicaciones;
        private System.Windows.Forms.ToolStripMenuItem btnExcelUbicaciones;
        private System.Windows.Forms.ToolStripMenuItem btnImprimirUbicaciones;
        private System.Windows.Forms.ToolStripMenuItem btnCargarUbicaciones;
        private System.Windows.Forms.ToolStripMenuItem btnGuargarUbicaciones;
        private System.Windows.Forms.ToolStripMenuItem btnGuardarPrefenciaUbicaciones;
        public NCQ_V.MyGridControl dtgModulos;
        private NCQ_V.MyGridView viewModulos;
        private DevExpress.XtraGrid.Columns.GridColumn colSubUbicacion;
        private DevExpress.XtraGrid.Columns.GridColumn colDescripcion;
        private DevExpress.XtraGrid.Columns.GridColumn colUbicacionSubUbicacion;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repBodega;
        private DevExpress.XtraGrid.Columns.GridColumn colActivoUbicacion;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repchk;
        private DevExpress.XtraGrid.Columns.GridColumn colCreadoporUbicacion;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaCreacionUbicacion;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repDate;
        private DevExpress.XtraGrid.Columns.GridColumn colModificadoporUbicacion;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaModificacionBodega;
        private System.Windows.Forms.ToolStripMenuItem btnVistaPredefinidaUbicaciones;
        private System.Windows.Forms.ToolStripMenuItem btnVistaPredefinidaSubUbicaciones;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repTipoLote;
        private System.Windows.Forms.PictureBox imgIcono;
    }
}


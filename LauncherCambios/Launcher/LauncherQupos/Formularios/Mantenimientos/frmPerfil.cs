﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid;
using System.IO;
using DevExpress.XtraPrinting;
using NCQ_V;
using NCQ_B;

namespace Activos
{
    public partial class frmPerfil : Form
    {
        NCQ_B.AccesoDatos oAccesoDatos;
        DataSet dsetUbicaciones = new DataSet();
        DataSet dsetSubUbicaciones = new DataSet();

        BindingSource bindingUbicaciones = new BindingSource();
        BindingSource bindingSubUbicacion = new BindingSource();
        //
        Stream strUbicaciones = new MemoryStream();
        Stream strSubUbicaciones = new MemoryStream();

        public frmPerfil(AccesoDatos poAccesoDatos)
        {
            InitializeComponent();
            this.oAccesoDatos = poAccesoDatos;
            //
            this.strUbicaciones = new MemoryStream();
            this.viewPerfil.SaveLayoutToStream(this.strUbicaciones);
            this.strUbicaciones.Seek(0, SeekOrigin.Begin);
            //
            this.strSubUbicaciones = new MemoryStream();
            this.viewPerfil.SaveLayoutToStream(this.strSubUbicaciones);
            this.strSubUbicaciones.Seek(0, SeekOrigin.Begin);
            //
            new DataGridEvents().CargarPreferencia(this.oAccesoDatos, this.Name, this.viewPerfil, "V", Program.login, "N");
            new DataGridEvents().CargarPreferencia(this.oAccesoDatos, this.Name, this.viewModulos, "V", Program.login, "N");
            viewPerfil.ActiveFilter.Clear();
            viewModulos.ActiveFilter.Clear();
            //
            this.RevisarSeguridad();
            this.RefrescarDatos();
            this.edtDescripcion.Focus();
            this.edtDescripcion.Select();
        }
        //
        private void frmPerfil_Load(object sender, EventArgs e)
        {
            Perfil oPerfil = new Perfil();
            imgNCQ.Image = oPerfil.perfiles_Imagen(oPerfil.getPerfil());
            this.Icon = oPerfil.perfiles_Icono(oPerfil.getPerfil());
            this.WindowState = FormWindowState.Maximized;

            this.viewPerfil.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.view_BeforeLeaveRow);
            this.bindingUbicaciones.BindingComplete += new BindingCompleteEventHandler(this.bindingBodega_BindingComplete);
            this.bindingSubUbicacion.BindingComplete += new BindingCompleteEventHandler(this.bindingUbicacion_BindingComplete);
        }

        //====================================================================================================
        #region Métodos de los botones
        //
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            this.viewPerfil.FocusedColumn = this.colDescripcion;
            this.viewPerfil.ShowEditor();
            //
            if (!this.btnPanel.Checked)
                this.btnPanel_Click(null, null);
            try
            {
                
                this.viewPerfil.BeforeLeaveRow -= new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.view_BeforeLeaveRow);
                this.bindingUbicaciones.AddNew();
                int indice = this.viewPerfil.FocusedRowHandle;
                
                //Seteamos los valores de auditoria   
                this.viewPerfil.SetRowCellValue(this.viewPerfil.FocusedRowHandle,
                    "creado_por", Program.login);
                this.viewPerfil.SetRowCellValue(this.viewPerfil.FocusedRowHandle,
                    "fecha_creacion", this.oAccesoDatos.ObtieneFecha());
                this.viewPerfil.SetRowCellValue(this.viewPerfil.FocusedRowHandle,
                    "modificado_por", Program.login);
                this.viewPerfil.SetRowCellValue(this.viewPerfil.FocusedRowHandle,
                    "fecha_modificacion", this.oAccesoDatos.ObtieneFecha());
                //Seteamos otros valores que consideremos necesarios                
                this.viewPerfil.SetRowCellValue(this.viewPerfil.FocusedRowHandle,
                    "activo", "S");
                this.viewPerfil.SetRowCellValue(this.viewPerfil.FocusedRowHandle,
                    "aux_bd", "N");
                //
                this.chkActivo.Checked = true;

                this.viewPerfil.OptionsSelection.MultiSelect = false;
                this.viewPerfil.FocusedRowHandle = indice;
                this.viewPerfil.OptionsSelection.MultiSelect = true;
                this.viewPerfil.MakeRowVisible(indice, false);
                this.viewPerfil.SelectRow(indice);

                this.edtDescripcion.Focus();
                this.edtDescripcion.Select();

                this.viewPerfil.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.view_BeforeLeaveRow);

            }
            catch (Exception) { }
        }
        //
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (this.viewPerfil.DataRowCount.Equals(0))
                return;

            /*
            if (this.Validacion.Validate())
            {
                DataSet dsetDatos = new DataSet();
                this.viewUbicaciones.FocusedRowHandle = -1;
                if (this.dsetUbicaciones.HasChanges())
                {
                    ActivosB.Ubicaciones oUbicaciones = new ActivosB.Ubicaciones(this.oAccesoDatos, Program.login);
                    dsetDatos = oUbicaciones.Salvar(this.dsetUbicaciones.Tables[0]);

                    if (oUbicaciones.IsError)
                    {
                        new NCQ_V.frmErrores("Ha ocurrido el siguiente error: " + oUbicaciones.ErrorDescripcion + " ", "");
                        return;
                    }
                    else
                    {
                        this.RefrescarDatos();
                        try
                        {
                            this.viewUbicaciones.OptionsSelection.MultiSelect = false;//quita la multiselecion del grid
                            string searchText = dsetDatos.Tables[0].Rows[0][0].ToString();
                            int indice = 0;
                            // obtaining the focused view 
                            ColumnView View = (ColumnView)this.dtgUbicaciones.FocusedView;
                            // obtaining the column bound to the Country field 
                            GridColumn column = View.Columns["activo_ubicacion"];
                            if (column != null)
                            {
                                // locating the row 
                                indice = View.LocateByDisplayText(View.FocusedRowHandle + 1, colUbicaciones, searchText);
                                // focusing the cell 
                                if (indice != GridControl.InvalidRowHandle)
                                {
                                    this.viewUbicaciones.FocusedRowHandle = indice;
                                    //this.viewTransportista.FocusedColumn = column;
                                }
                            }
                            this.viewUbicaciones.OptionsSelection.MultiSelect = true;//activa la multiseleccion
                            this.viewUbicaciones.MakeRowVisible(indice, false);//marca visible la fila
                            this.viewUbicaciones.SelectRow(indice);//selecciona la fila
                            this.viewUbicaciones.Focus();//
                            this.viewUbicaciones.ShowEditor();
                        }
                        catch { }
                    }
                }
            }
            */
        }
        //
        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (this.viewPerfil.IsNewItemRow(this.viewPerfil.FocusedRowHandle))
            {
                this.viewPerfil.CancelUpdateCurrentRow();
                return;
            }
            if (this.viewPerfil.FocusedRowHandle >= 0)
            {
                //obtener los códigos de los items seleccionados para borrar            
                int[] rows_seleccionados = this.viewPerfil.GetSelectedRows();//Obtenemos los rows seleccionados
                DataRow[] rows_borrar = new DataRow[rows_seleccionados.Length];//Se crea un DataRows donde se almacenaran la información de cada row
                for (int n = 0; n < rows_seleccionados.Length; n++)//Recorremos los row seleccionados
                {
                    rows_borrar[n] = this.viewPerfil.GetDataRow(rows_seleccionados[n]);//Seteamos la información de cada row seleccionado al DataRows de borrar 
                }
                foreach (DataRow dr in rows_borrar)//Recorremos el DataRows de borrado y realizar la acción según el estado
                {
                    if (dr["aux_bd"].ToString() == "D")//cancelar el borrado                
                        dr.RejectChanges();
                    else if (dr.RowState == DataRowState.Modified || dr.RowState == DataRowState.Unchanged)//Marcar para borrar
                        dr["aux_bd"] = "D";
                    else if (dr.RowState == DataRowState.Added)//eliminar del grid
                        dr.Delete();
                }
                this.viewPerfil.RefreshData();
            }
        }
        //
        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            int indice = this.viewPerfil.FocusedRowHandle;

            if (this.viewPerfil.UpdateCurrentRow())
                this.viewPerfil.RefreshData();
            if (this.viewPerfil.IsNewItemRow(this.viewPerfil.FocusedRowHandle))
                return;

            if (this.dsetUbicaciones.HasChanges())
            {
                DialogResult dialog = new DialogResult();
                dialog = DevExpress.XtraEditors.XtraMessageBox.Show("Existen transacciones pendientes.\n¿Desea continuar?", "Confirmación",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    this.RefrescarDatos();
                }
                else return;
            }
            else this.RefrescarDatos();
            //
            this.viewPerfil.OptionsSelection.MultiSelect = false;
            this.viewPerfil.FocusedRowHandle = indice;
            this.viewPerfil.OptionsSelection.MultiSelect = true;
            this.viewPerfil.MakeRowVisible(this.viewPerfil.FocusedRowHandle, false);
            this.viewPerfil.SelectRow(this.viewPerfil.FocusedRowHandle);
            this.viewPerfil.Focus();
            
        }
        //
        private void btnAnterior_Click(object sender, EventArgs e)
        {
            this.viewPerfil.MovePrev();
        }
        //
        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            this.viewPerfil.MoveNext();
        }
        //
        private void btnPrimero_Click(object sender, EventArgs e)
        {
            this.viewPerfil.MoveFirst();
        }
        //
        private void btnUltimo_Click(object sender, EventArgs e)
        {
            this.viewPerfil.MoveLast();
        }
        //
        private void btnExcel_ButtonClick(object sender, EventArgs e)
        {
            this.btnExcel.ShowDropDown();
        }
        //
        private void btnImprimir_ButtonClick(object sender, EventArgs e)
        {
            this.btnImprimir.ShowDropDown();
        }
        //
        private void btnPreferencias_ButtonClick(object sender, EventArgs e)
        {
            this.btnPreferencias.ShowDropDown();
        }
        //
        private void btnCargarSubUbicaciones_Click(object sender, EventArgs e)
        {
            new DataGridEvents().CargarPreferencia(this.oAccesoDatos, this.Name, this.viewModulos, "V", Program.login);
        }
        //
        private void btnGuardarSubUbicaciones_Click(object sender, EventArgs e)
        {
            new DataGridEvents().SalvarPreferencia(this.oAccesoDatos, this.Name, this.viewModulos, "V", Program.login);
        }
        //
        private void btnGuardarPreferenciasSubUbicaciones_Click(object sender, EventArgs e)
        {
            new DataGridEvents().SalvarPreferencia(this.oAccesoDatos, this.Name, this.viewModulos, "V", Program.login, "S", true);
        }
        //
        private void btnExcelSubUbicaciones_Click(object sender, EventArgs e)
        {
            new NCQ_V.DataGridEvents().ExportarExcelGrid(this.viewModulos);
        }
        //
        private void btnImprimirSubUbicaciones_Click(object sender, EventArgs e)
        {
            new NCQ_V.DataGridPrinter(this.dtgModulos, "Sub-Ubicaciones");
        }
        //
        private void mnuVistaPredeterminada_Click(object sender, EventArgs e)
        {
            this.viewPerfil.ClearGrouping();
            this.viewPerfil.RestoreLayoutFromStream(strUbicaciones);
            strUbicaciones.Seek(0, SeekOrigin.Begin);
        }
        //
        private void btnVistaPredefinidaUbicaciones_Click(object sender, EventArgs e)
        {
            this.viewModulos.ClearGrouping();
            this.viewModulos.RestoreLayoutFromStream(this.strSubUbicaciones);
            this.strSubUbicaciones.Seek(0, SeekOrigin.Begin);
        }
        //
        private void btnVistaPredefinidaBodega_Click(object sender, EventArgs e)
        {
            this.viewPerfil.ClearGrouping();
            this.viewPerfil.RestoreLayoutFromStream(this.strUbicaciones);
            this.strUbicaciones.Seek(0, SeekOrigin.Begin);
        }
        //
        private void btnExcelUbicaciones_Click(object sender, EventArgs e)
        {
            new NCQ_V.DataGridEvents().ExportarExcelGrid(this.viewPerfil);
        }
        //
        private void btnImprimirUbicaciones_Click(object sender, EventArgs e)
        {
            new NCQ_V.DataGridPrinter(this.dtgPerfil, "Ubicaciones");
        }
        //
        private void btnCargarUbicaciones_Click(object sender, EventArgs e)
        {
            new DataGridEvents().CargarPreferencia(this.oAccesoDatos, this.Name, this.viewPerfil, "V", Program.login);
        }
        //
        private void btnGuargarUbicaciones_Click(object sender, EventArgs e)
        {
            new DataGridEvents().SalvarPreferencia(this.oAccesoDatos, this.Name, this.viewPerfil, "V", Program.login);
        }
        //
        private void btnGuardarPrefenciaUbicaciones_Click(object sender, EventArgs e)
        {
            new DataGridEvents().SalvarPreferencia(this.oAccesoDatos, this.Name, this.viewPerfil, "V", Program.login, "S", true);
        }
        //
        private void btnPanel_Click(object sender, EventArgs e)
        {
            if (this.splTipo.PanelVisibility == DevExpress.XtraEditors.SplitPanelVisibility.Both)
                this.splTipo.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel1;
            else
                this.splTipo.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Both;
            if (sender == null)
                this.btnPanel.Checked = !btnPanel.Checked;
        }
        //
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //
        private void btnEditarSubUbicacion_Click(object sender, EventArgs e)
        {
            if (this.dsetUbicaciones.HasChanges())
            {
                DialogResult resultado = MessageBox.Show("Existen cambios sin salvar.\n¿Desea salvar los cambios antes de continuar?", "Error", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (resultado == DialogResult.Yes)
                    this.btnSalvar.PerformClick();
                if (resultado == DialogResult.Cancel)
                    return;
            }
            /*
            if (this.viewUbicaciones.FocusedRowHandle >= 0)
            {
                DataRow drow = this.viewUbicaciones.GetDataRow(this.viewUbicaciones.FocusedRowHandle);

                int indice = this.viewUbicaciones.FocusedRowHandle;

                frmSubUbicaciones ofrmSubUbicaciones = new frmSubUbicaciones(this.oAccesoDatos, drow["activo_ubicacion"].ToString());
                ofrmSubUbicaciones.ShowDialog();
                this.RefrescarDatos();
                this.viewUbicaciones.OptionsSelection.MultiSelect = false;
                this.viewUbicaciones.FocusedRowHandle = indice;
                this.viewUbicaciones.OptionsSelection.MultiSelect = true;
                this.viewUbicaciones.MakeRowVisible(indice, false);
                this.viewUbicaciones.SelectRow(indice);
                this.viewUbicaciones.Focus();
            }
            */
        }
        //
        private void btnBorrarSubUbicacion_Click(object sender, EventArgs e)
        {
            if (this.viewModulos.DataRowCount.Equals(0))
                return;

            int indice = this.viewPerfil.FocusedRowHandle;
            if (this.viewModulos.FocusedRowHandle >= 0)
            {
                if (this.dsetUbicaciones.HasChanges())
                {
                    DialogResult resultado = MessageBox.Show("Existen cambios sin salvar.\n¿Desea salvar los cambios antes de continuar?", "Error", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (resultado == DialogResult.Yes)
                        this.btnSalvar.PerformClick();
                    if (resultado == DialogResult.Cancel)
                        return;
                }

                DialogResult dialog = new DialogResult();
                dialog = MessageBox.Show("¿Está seguro de borrar estas ubicaciones.?", "Confirmación",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog != DialogResult.Yes)
                {
                    return;
                }

                DataRow[] drSeleccionados = new DataRow[1];

                int[] rows_seleccionados = this.viewModulos.GetSelectedRows();//Obtenemos los rows seleccionados
                drSeleccionados = new DataRow[rows_seleccionados.Length];//Se crea un DataRows donde se almacenaran la información de cada row
                for (int n = 0; n < rows_seleccionados.Length; n++)//Recorremos los row seleccionados
                {
                    drSeleccionados[n] = this.viewModulos.GetDataRow(rows_seleccionados[n]);//Seteamos la información de cada row seleccionado al DataRows de borrar 
                }
                /*
                ActivosB.Ubicaciones oUbicaciones = new ActivosB.Ubicaciones(this.oAccesoDatos, Program.login);
                string error = "";
                oUbicaciones.EliminarSubUbicaciones(drSeleccionados, ref error);
                if (oUbicaciones.IsError)
                {
                    new NCQ_V.frmErrores(oUbicaciones.ErrorDescripcion, "No fue posible borrar la sub-ubicación " + error + ".");
                    return;
                }
                this.RefrescarDatos();
                this.viewUbicaciones.OptionsSelection.MultiSelect = false;
                this.viewUbicaciones.FocusedRowHandle = indice;
                this.viewUbicaciones.OptionsSelection.MultiSelect = true;
                this.viewUbicaciones.MakeRowVisible(indice, false);
                this.viewUbicaciones.SelectRow(indice);
                this.viewUbicaciones.Focus();
                */
            }
        }
        //
        #endregion

        //====================================================================================================
        #region Métodos de los componentes
        //
        private void frmPerfil_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.dsetUbicaciones.HasChanges())
            {
                DialogResult dialog = new DialogResult();
                dialog = MessageBox.Show("Existen transacciones pendientes.\n¿Desea guardar los cambios realizados?", "Confirmación",
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    btnSalvar_Click(sender, e);
                    this.Close();
                }
                else if (dialog == DialogResult.Cancel)
                    e.Cancel = true;
            }
            new DataGridEvents().SalvarPreferencia(this.oAccesoDatos, this.Name, this.viewPerfil, "V", Program.login, "N");
        }
        //
        private void frmPerfil_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{Tab}");
                e.SuppressKeyPress = true;
            }
            if (e.KeyCode == Keys.Escape)
            {
                if (this.viewPerfil.IsNewItemRow(this.viewPerfil.FocusedRowHandle))
                {
                    int indice = this.viewPerfil.FocusedRowHandle;
                    this.RefrescarDatos();
                    this.viewPerfil.OptionsSelection.MultiSelect = false;
                    this.viewPerfil.FocusedRowHandle = indice;
                    this.viewPerfil.OptionsSelection.MultiSelect = true;
                    this.viewPerfil.MakeRowVisible(indice, false);
                    this.viewPerfil.SelectRow(indice);
                }
                this.bindingUbicaciones.CancelEdit();
                this.bindingSubUbicacion.CancelEdit();
            }

        }
        //
        private void bindingBodega_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            try
            {
                //Check if the data source has been updated, and that no error has occured.
                if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate && e.Exception == null)
                {
                    e.Binding.BindingManagerBase.EndCurrentEdit();
                    this.bindingUbicaciones.ResetBindings(false);
                }
            }
            catch (Exception) { }
        }
        //
        private void bindingUbicacion_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            try
            {
                //Check if the data source has been updated, and that no error has occured.
                if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate && e.Exception == null)
                {
                    e.Binding.BindingManagerBase.EndCurrentEdit();
                    this.bindingSubUbicacion.ResetBindings(false);
                }
            }
            catch (Exception) { }
        }
        //
        private void view_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
        {
            if (this.viewPerfil.FocusedRowHandle >= 0)
            {
                if (!this.Validacion.Validate())
                    e.Allow = false;
                else
                    e.Allow = true;
            }
        }
        //
        private void view_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //Actualiza lo que ocurra en el grid en las cajas, el evento se activa 
            //cuando cambia el valor de la celda
            foreach (Control c in tabGeneral.Controls)
            {
                try
                {
                    c.DataBindings[0].ReadValue();
                }
                catch { }
            }
        }
        //
        private void view_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            new NCQ_V.DataGridEvents().view_CustomDrawCell(sender, e);
        }
        //
        private void view_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            new NCQ_V.DataGridEvents().view_CustomDrawGroupRow(sender, e);
        }
        //
        private void view_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            new NCQ_V.DataGridEvents().view_CustomDrawRowIndicator(sender, e);
        }
        //
        private void view_MouseDown(object sender, MouseEventArgs e)
        {
            NCQ_V.MyGridView dgview = sender as NCQ_V.MyGridView;
            if (e.Button == MouseButtons.Left)
                if (dgview.CalcHitInfo(new Point(e.X, e.Y)).HitTest == DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.ColumnButton)
                    new NCQ_V.frmColumnas(dgview);
        }
        //
        #endregion
        
        //====================================================================================================
        #region Métodos Miscelaneos
        //
        public void RefrescarDatos()
        {
            try
            {
                this.bindingUbicaciones.CancelEdit();
                this.bindingSubUbicacion.CancelEdit();
            }
            catch { }

            this.dtgPerfil.DataSource = null;
            this.dsetSubUbicaciones = new DataSet();
            /*
            ActivosB.Ubicaciones oUbicaciones = new ActivosB.Ubicaciones(this.oAccesoDatos, Program.login);
            this.dsetUbicaciones = oUbicaciones.TraerDatos();
            if (oUbicaciones.IsError)
            {
                new NCQ_V.frmErrores(oUbicaciones.ErrorDescripcion, "Error cargando los datos.");
                return;
            }
            */
            //
            try
            {
                this.dsetSubUbicaciones.Relations.Remove("rel_sub_ubicaciones");
            }
            catch { }
            //
            try
            {
                try
                {
                    this.edtUbicaciones.DataBindings.Clear();
                    this.edtDescripcion.DataBindings.Clear();
                    this.chkActivo.DataBindings.Clear();
                }
                catch { }

                try
                {
                    /*
                    this.dsetSubUbicaciones = oUbicaciones.TraerDatosSubUbicaciones("");
                    if (oUbicaciones.IsError)
                    {
                        new NCQ_V.frmErrores(oUbicaciones.ErrorDescripcion, "Error cargando los datos.");
                        return;
                    }
                    */
                    //Creamos la llave primaria de la tabla cliente_contacto
                    //DataColumn[] pk2 = new DataColumn[1];
                    //pk2[0] = this.dsetSubUbicaciones.Tables[0].Columns["activo_sub_ubicacion"];
                    //this.dsetSubUbicaciones.Tables[0].PrimaryKey = pk2;

                    this.dsetUbicaciones.Tables.Add(this.dsetSubUbicaciones.Tables[0].Copy());
                    //creamos la relacion entre "padre e hijo"
                    this.dsetUbicaciones.Relations.Add("rel_sub_ubicaciones",
                            this.dsetUbicaciones.Tables[0].Columns["activo_ubicacion"],
                            this.dsetUbicaciones.Tables[1].Columns["activo_ubicacion"], false);
                }
                catch { }
                //
                this.bindingUbicaciones.DataSource = this.dsetUbicaciones;
                this.bindingUbicaciones.DataMember = this.dsetUbicaciones.Tables[0].TableName;

                this.bindingSubUbicacion.DataSource = this.bindingUbicaciones;
                this.bindingSubUbicacion.DataMember = "rel_sub_ubicaciones";

                this.dtgModulos.DataSource = this.bindingSubUbicacion;
                
            }
            catch (Exception er)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                this.edtUbicaciones.DataBindings.Add("EditValue", this.bindingUbicaciones, "activo_ubicacion", true,
                    DataSourceUpdateMode.OnPropertyChanged);
                this.edtDescripcion.DataBindings.Add("EditValue", this.bindingUbicaciones, "descripcion", true,
                        DataSourceUpdateMode.OnPropertyChanged);
                this.chkActivo.DataBindings.Add("EditValue", this.bindingUbicaciones, "activo", true,
                    DataSourceUpdateMode.OnPropertyChanged);
            }
            catch (Exception e)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.dtgPerfil.DataSource = this.bindingUbicaciones;
            //this.dsetBodega.AcceptChanges();

            this.viewPerfil.FocusedRowHandle = -999997;//Se posiciona en el row filtro
            this.viewPerfil.FocusedRowHandle = 0;
            
        }
        //
        private void RevisarSeguridad()
        {
            NCQ_B.Seguridad oSeguridad = new NCQ_B.Seguridad(this.oAccesoDatos,Program.login,"");
            string msg = oSeguridad.ValidaAccion("activos-mant-ubica-edic");
            if (msg != "") //si no hay permisos
            {
                this.btnBorrar.Enabled = false;
                this.btnSalvar.Enabled = false;
                this.btnNuevo.Enabled = false;
                this.viewPerfil.OptionsBehavior.ReadOnly = true;
                this.viewModulos.OptionsBehavior.ReadOnly = true;
                //
                this.edtUbicaciones.Properties.ReadOnly = true;
                this.edtDescripcion.Properties.ReadOnly = true;
                this.chkActivo.Properties.ReadOnly = true;
                //
                this.btnEditarSubUbicacion.Enabled = false;
                this.btnBorrarSubUbicacion.Enabled = false;
            }
        }
        //        
        #endregion
        
    }//Finaliza la clase
}//Finalizar namespace
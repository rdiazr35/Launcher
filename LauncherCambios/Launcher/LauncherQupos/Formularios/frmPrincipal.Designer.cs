﻿namespace LauncherQupos
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.pnTop = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuDropdown1 = new Bunifu.Framework.UI.BunifuDropdown();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblMaximizar = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblMinimizar = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblClose = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuFormFadeTransition1 = new Bunifu.Framework.UI.BunifuFormFadeTransition(this.components);
            this.backPicture = new System.Windows.Forms.Timer(this.components);
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.pnPictures = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.ptFondos = new DevExpress.XtraEditors.PictureEdit();
            this.pnSearch = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.lblEspere = new DevExpress.XtraEditors.LabelControl();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.bunifuGradientPanel4 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.edtSearch = new DevExpress.XtraEditors.TextEdit();
            this.ptBuscar = new System.Windows.Forms.PictureBox();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btnActualizador = new System.Windows.Forms.PictureBox();
            this.btnAyudaQupos = new System.Windows.Forms.PictureBox();
            this.btnDiagnostico = new System.Windows.Forms.PictureBox();
            this.btnAnydesk = new System.Windows.Forms.PictureBox();
            this.btnPreferencias = new System.Windows.Forms.PictureBox();
            this.btnSoporte = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.pnPictures.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptFondos.Properties)).BeginInit();
            this.pnSearch.SuspendLayout();
            this.bunifuGradientPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptBuscar)).BeginInit();
            this.bunifuGradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnActualizador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAyudaQupos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDiagnostico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAnydesk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPreferencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSoporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnTop;
            this.bunifuDragControl1.Vertical = true;
            // 
            // pnTop
            // 
            this.pnTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.pnTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnTop.BackgroundImage")));
            this.pnTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnTop.Controls.Add(this.bunifuDropdown1);
            this.pnTop.Controls.Add(this.pictureBox4);
            this.pnTop.Controls.Add(this.pictureBox3);
            this.pnTop.Controls.Add(this.lblMaximizar);
            this.pnTop.Controls.Add(this.lblMinimizar);
            this.pnTop.Controls.Add(this.lblClose);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.pnTop.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.pnTop.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.pnTop.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Quality = 10;
            this.pnTop.Size = new System.Drawing.Size(1024, 60);
            this.pnTop.TabIndex = 0;
            this.pnTop.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pnTop_MouseDoubleClick);
            // 
            // bunifuDropdown1
            // 
            this.bunifuDropdown1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuDropdown1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuDropdown1.BorderRadius = 3;
            this.bunifuDropdown1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuDropdown1.ForeColor = System.Drawing.Color.White;
            this.bunifuDropdown1.Items = new string[] {
        "NCQ LOCAL",
        "COOPELIBERTAD"};
            this.bunifuDropdown1.Location = new System.Drawing.Point(781, 5);
            this.bunifuDropdown1.Name = "bunifuDropdown1";
            this.bunifuDropdown1.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.bunifuDropdown1.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(252)))));
            this.bunifuDropdown1.selectedIndex = 0;
            this.bunifuDropdown1.Size = new System.Drawing.Size(173, 32);
            this.bunifuDropdown1.TabIndex = 5;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox4.Image = global::LauncherQupos.Properties.Resources.Nombre_QUPOS;
            this.pictureBox4.Location = new System.Drawing.Point(65, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(103, 60);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox3.Image = global::LauncherQupos.Properties.Resources.QuposERP;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(65, 60);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // lblMaximizar
            // 
            this.lblMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaximizar.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaximizar.ForeColor = System.Drawing.Color.White;
            this.lblMaximizar.Location = new System.Drawing.Point(979, 0);
            this.lblMaximizar.Margin = new System.Windows.Forms.Padding(0);
            this.lblMaximizar.Name = "lblMaximizar";
            this.lblMaximizar.Size = new System.Drawing.Size(20, 30);
            this.lblMaximizar.TabIndex = 2;
            this.lblMaximizar.Text = "□";
            this.lblMaximizar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMaximizar.Click += new System.EventHandler(this.lblMaximizar_Click);
            this.lblMaximizar.MouseLeave += new System.EventHandler(this.lblMaximizar_MouseLeave);
            this.lblMaximizar.MouseHover += new System.EventHandler(this.lblMaximizar_MouseHover);
            // 
            // lblMinimizar
            // 
            this.lblMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMinimizar.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinimizar.ForeColor = System.Drawing.Color.White;
            this.lblMinimizar.Location = new System.Drawing.Point(957, 0);
            this.lblMinimizar.Margin = new System.Windows.Forms.Padding(0);
            this.lblMinimizar.Name = "lblMinimizar";
            this.lblMinimizar.Size = new System.Drawing.Size(20, 30);
            this.lblMinimizar.TabIndex = 1;
            this.lblMinimizar.Text = "─";
            this.lblMinimizar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMinimizar.Click += new System.EventHandler(this.lblMinimizar_Click);
            this.lblMinimizar.MouseLeave += new System.EventHandler(this.lblMinimizar_MouseLeave);
            this.lblMinimizar.MouseHover += new System.EventHandler(this.lblMinimizar_MouseHover);
            // 
            // lblClose
            // 
            this.lblClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblClose.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClose.ForeColor = System.Drawing.Color.White;
            this.lblClose.Location = new System.Drawing.Point(1002, 0);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(20, 30);
            this.lblClose.TabIndex = 0;
            this.lblClose.Text = "x";
            this.lblClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblClose.Click += new System.EventHandler(this.lblClose_Click);
            this.lblClose.MouseLeave += new System.EventHandler(this.lblClose_MouseLeave);
            this.lblClose.MouseHover += new System.EventHandler(this.lblClose_MouseHover);
            // 
            // bunifuFormFadeTransition1
            // 
            this.bunifuFormFadeTransition1.Delay = 1;
            // 
            // backPicture
            // 
            this.backPicture.Interval = 1000;
            // 
            // timer
            // 
            this.timer.Interval = 10000;
            // 
            // pnPictures
            // 
            this.pnPictures.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnPictures.BackgroundImage")));
            this.pnPictures.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnPictures.Controls.Add(this.ptFondos);
            this.pnPictures.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnPictures.GradientBottomLeft = System.Drawing.Color.White;
            this.pnPictures.GradientBottomRight = System.Drawing.Color.White;
            this.pnPictures.GradientTopLeft = System.Drawing.Color.White;
            this.pnPictures.GradientTopRight = System.Drawing.Color.White;
            this.pnPictures.Location = new System.Drawing.Point(349, 60);
            this.pnPictures.Name = "pnPictures";
            this.pnPictures.Quality = 10;
            this.pnPictures.Size = new System.Drawing.Size(675, 610);
            this.pnPictures.TabIndex = 2;
            // 
            // ptFondos
            // 
            this.ptFondos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ptFondos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ptFondos.EditValue = global::LauncherQupos.Properties.Resources.QUPOS_01;
            this.ptFondos.Location = new System.Drawing.Point(0, 0);
            this.ptFondos.Name = "ptFondos";
            this.ptFondos.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.ptFondos.Properties.Appearance.Options.UseBackColor = true;
            this.ptFondos.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ptFondos.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.ptFondos.Size = new System.Drawing.Size(675, 610);
            this.ptFondos.TabIndex = 0;
            // 
            // pnSearch
            // 
            this.pnSearch.AutoScroll = true;
            this.pnSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnSearch.BackgroundImage")));
            this.pnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnSearch.Controls.Add(this.lblEspere);
            this.pnSearch.Controls.Add(this.pnlMenu);
            this.pnSearch.Controls.Add(this.bunifuGradientPanel4);
            this.pnSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnSearch.GradientBottomLeft = System.Drawing.Color.White;
            this.pnSearch.GradientBottomRight = System.Drawing.Color.White;
            this.pnSearch.GradientTopLeft = System.Drawing.Color.White;
            this.pnSearch.GradientTopRight = System.Drawing.Color.White;
            this.pnSearch.Location = new System.Drawing.Point(0, 60);
            this.pnSearch.Name = "pnSearch";
            this.pnSearch.Quality = 10;
            this.pnSearch.Size = new System.Drawing.Size(349, 610);
            this.pnSearch.TabIndex = 1;
            // 
            // lblEspere
            // 
            this.lblEspere.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblEspere.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEspere.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblEspere.Appearance.Options.UseBackColor = true;
            this.lblEspere.Appearance.Options.UseFont = true;
            this.lblEspere.Appearance.Options.UseForeColor = true;
            this.lblEspere.Location = new System.Drawing.Point(115, 223);
            this.lblEspere.Name = "lblEspere";
            this.lblEspere.Size = new System.Drawing.Size(109, 23);
            this.lblEspere.TabIndex = 9;
            this.lblEspere.Text = "Cargando...";
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.White;
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMenu.Location = new System.Drawing.Point(0, 44);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(349, 566);
            this.pnlMenu.TabIndex = 19;
            this.pnlMenu.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.pnlMenu_ControlAdded);
            // 
            // bunifuGradientPanel4
            // 
            this.bunifuGradientPanel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel4.BackgroundImage")));
            this.bunifuGradientPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel4.Controls.Add(this.edtSearch);
            this.bunifuGradientPanel4.Controls.Add(this.ptBuscar);
            this.bunifuGradientPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel4.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel4.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel4.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel4.Name = "bunifuGradientPanel4";
            this.bunifuGradientPanel4.Quality = 10;
            this.bunifuGradientPanel4.Size = new System.Drawing.Size(349, 44);
            this.bunifuGradientPanel4.TabIndex = 22;
            // 
            // edtSearch
            // 
            this.edtSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edtSearch.EditValue = "";
            this.edtSearch.Location = new System.Drawing.Point(42, 0);
            this.edtSearch.Name = "edtSearch";
            this.edtSearch.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(43)))), ((int)(((byte)(55)))));
            this.edtSearch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.edtSearch.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.edtSearch.Properties.Appearance.Options.UseBackColor = true;
            this.edtSearch.Properties.Appearance.Options.UseFont = true;
            this.edtSearch.Properties.Appearance.Options.UseForeColor = true;
            this.edtSearch.Properties.AutoHeight = false;
            this.edtSearch.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.edtSearch.Properties.NullValuePromptShowForEmptyValue = true;
            this.edtSearch.Properties.UseParentBackground = true;
            this.edtSearch.Size = new System.Drawing.Size(307, 44);
            this.edtSearch.TabIndex = 20;
            this.edtSearch.TextChanged += new System.EventHandler(this.edtSearch_TextChanged);
            // 
            // ptBuscar
            // 
            this.ptBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(43)))), ((int)(((byte)(55)))));
            this.ptBuscar.Dock = System.Windows.Forms.DockStyle.Left;
            this.ptBuscar.Image = global::LauncherQupos.Properties.Resources.ic_search_white_24dp2;
            this.ptBuscar.Location = new System.Drawing.Point(0, 0);
            this.ptBuscar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.ptBuscar.Name = "ptBuscar";
            this.ptBuscar.Size = new System.Drawing.Size(42, 44);
            this.ptBuscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ptBuscar.TabIndex = 21;
            this.ptBuscar.TabStop = false;
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackColor = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.btnActualizador);
            this.bunifuGradientPanel1.Controls.Add(this.btnAyudaQupos);
            this.bunifuGradientPanel1.Controls.Add(this.btnDiagnostico);
            this.bunifuGradientPanel1.Controls.Add(this.btnAnydesk);
            this.bunifuGradientPanel1.Controls.Add(this.btnPreferencias);
            this.bunifuGradientPanel1.Controls.Add(this.btnSoporte);
            this.bunifuGradientPanel1.Controls.Add(this.pictureBox2);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuGradientPanel1.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 670);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(1024, 50);
            this.bunifuGradientPanel1.TabIndex = 3;
            // 
            // btnActualizador
            // 
            this.btnActualizador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualizador.Image = global::LauncherQupos.Properties.Resources.update;
            this.btnActualizador.Location = new System.Drawing.Point(844, 12);
            this.btnActualizador.Margin = new System.Windows.Forms.Padding(2);
            this.btnActualizador.Name = "btnActualizador";
            this.btnActualizador.Size = new System.Drawing.Size(25, 25);
            this.btnActualizador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnActualizador.TabIndex = 9;
            this.btnActualizador.TabStop = false;
            this.btnActualizador.Click += new System.EventHandler(this.btnActualizador_Click);
            this.btnActualizador.MouseLeave += new System.EventHandler(this.picture_MouseLeave);
            this.btnActualizador.MouseHover += new System.EventHandler(this.picture_MouseHover);
            // 
            // btnAyudaQupos
            // 
            this.btnAyudaQupos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAyudaQupos.Image = global::LauncherQupos.Properties.Resources.AyudaQupos;
            this.btnAyudaQupos.Location = new System.Drawing.Point(810, 12);
            this.btnAyudaQupos.Margin = new System.Windows.Forms.Padding(2);
            this.btnAyudaQupos.Name = "btnAyudaQupos";
            this.btnAyudaQupos.Size = new System.Drawing.Size(25, 25);
            this.btnAyudaQupos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnAyudaQupos.TabIndex = 8;
            this.btnAyudaQupos.TabStop = false;
            this.btnAyudaQupos.Click += new System.EventHandler(this.btnAyudaQupos_Click);
            this.btnAyudaQupos.MouseLeave += new System.EventHandler(this.picture_MouseLeave);
            this.btnAyudaQupos.MouseHover += new System.EventHandler(this.picture_MouseHover);
            // 
            // btnDiagnostico
            // 
            this.btnDiagnostico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiagnostico.Image = global::LauncherQupos.Properties.Resources.diagnostic;
            this.btnDiagnostico.Location = new System.Drawing.Point(878, 12);
            this.btnDiagnostico.Margin = new System.Windows.Forms.Padding(2);
            this.btnDiagnostico.Name = "btnDiagnostico";
            this.btnDiagnostico.Size = new System.Drawing.Size(25, 25);
            this.btnDiagnostico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnDiagnostico.TabIndex = 7;
            this.btnDiagnostico.TabStop = false;
            this.btnDiagnostico.Click += new System.EventHandler(this.btnDiagnostico_Click);
            this.btnDiagnostico.MouseLeave += new System.EventHandler(this.picture_MouseLeave);
            this.btnDiagnostico.MouseHover += new System.EventHandler(this.picture_MouseHover);
            // 
            // btnAnydesk
            // 
            this.btnAnydesk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnydesk.Image = global::LauncherQupos.Properties.Resources.AnyDesk;
            this.btnAnydesk.Location = new System.Drawing.Point(912, 12);
            this.btnAnydesk.Margin = new System.Windows.Forms.Padding(2);
            this.btnAnydesk.Name = "btnAnydesk";
            this.btnAnydesk.Size = new System.Drawing.Size(25, 25);
            this.btnAnydesk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnAnydesk.TabIndex = 6;
            this.btnAnydesk.TabStop = false;
            this.btnAnydesk.Click += new System.EventHandler(this.btnAnydesk_Click);
            this.btnAnydesk.MouseLeave += new System.EventHandler(this.picture_MouseLeave);
            this.btnAnydesk.MouseHover += new System.EventHandler(this.picture_MouseHover);
            // 
            // btnPreferencias
            // 
            this.btnPreferencias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreferencias.Image = global::LauncherQupos.Properties.Resources.preferencias;
            this.btnPreferencias.Location = new System.Drawing.Point(946, 12);
            this.btnPreferencias.Margin = new System.Windows.Forms.Padding(2);
            this.btnPreferencias.Name = "btnPreferencias";
            this.btnPreferencias.Size = new System.Drawing.Size(25, 25);
            this.btnPreferencias.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnPreferencias.TabIndex = 5;
            this.btnPreferencias.TabStop = false;
            this.btnPreferencias.Click += new System.EventHandler(this.btnPreferencias_Click);
            this.btnPreferencias.MouseLeave += new System.EventHandler(this.picture_MouseLeave);
            this.btnPreferencias.MouseHover += new System.EventHandler(this.picture_MouseHover);
            // 
            // btnSoporte
            // 
            this.btnSoporte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSoporte.Image = global::LauncherQupos.Properties.Resources.support1;
            this.btnSoporte.Location = new System.Drawing.Point(980, 12);
            this.btnSoporte.Margin = new System.Windows.Forms.Padding(2);
            this.btnSoporte.Name = "btnSoporte";
            this.btnSoporte.Size = new System.Drawing.Size(25, 25);
            this.btnSoporte.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnSoporte.TabIndex = 4;
            this.btnSoporte.TabStop = false;
            this.btnSoporte.Click += new System.EventHandler(this.btnSoporte_Click);
            this.btnSoporte.MouseLeave += new System.EventHandler(this.picture_MouseLeave);
            this.btnSoporte.MouseHover += new System.EventHandler(this.picture_MouseHover);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::LauncherQupos.Properties.Resources.banner_abajo;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1024, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1024, 720);
            this.Controls.Add(this.pnPictures);
            this.Controls.Add(this.pnSearch);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Controls.Add(this.pnTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmPrincipal_MouseDown);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.pnPictures.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptFondos.Properties)).EndInit();
            this.pnSearch.ResumeLayout(false);
            this.pnSearch.PerformLayout();
            this.bunifuGradientPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edtSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptBuscar)).EndInit();
            this.bunifuGradientPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnActualizador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAyudaQupos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDiagnostico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAnydesk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPreferencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSoporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuGradientPanel pnTop;
        private Bunifu.Framework.UI.BunifuGradientPanel pnSearch;
        private Bunifu.Framework.UI.BunifuGradientPanel pnPictures;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuCustomLabel lblClose;
        private Bunifu.Framework.UI.BunifuCustomLabel lblMinimizar;
        private Bunifu.Framework.UI.BunifuCustomLabel lblMaximizar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuFormFadeTransition bunifuFormFadeTransition1;
        private System.Windows.Forms.PictureBox btnSoporte;
        private System.Windows.Forms.PictureBox btnAyudaQupos;
        private System.Windows.Forms.PictureBox btnDiagnostico;
        private System.Windows.Forms.PictureBox btnAnydesk;
        private System.Windows.Forms.PictureBox btnPreferencias;
        private System.Windows.Forms.PictureBox btnActualizador;
        private Bunifu.Framework.UI.BunifuDropdown bunifuDropdown1;
        private System.Windows.Forms.Timer backPicture;
        private DevExpress.XtraEditors.PictureEdit ptFondos;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Panel pnlMenu;
        private DevExpress.XtraEditors.LabelControl lblEspere;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel4;
        private System.Windows.Forms.PictureBox ptBuscar;
        private DevExpress.XtraEditors.TextEdit edtSearch;
    }
}


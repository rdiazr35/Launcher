﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Data;

namespace LauncherQupos
{

    public class ItemModulo : Panel
    {

        protected String Number, Titulo;
        protected SolidBrush auxColor;
        protected Pen auxPen, penOriginal, penEnter;
        protected Rectangle rectangle;
        protected Color backColorOriginal, backColorEnter;
        protected SolidBrush brushOriginal, brushEnter;
        protected Font fontTitulo, fontNumber;
        protected Point pointTitulo;
        //
        //
        protected Point pointNumber;
        protected Action backAction;
        protected Action<string> formAction;
        protected frmPrincipal.OutSelect<string, DataRow[]> subModulos;
        protected frmPrincipal.OutSelect<string, DataRow[]> pantallas;
        protected Action<List<Panel>> cargaMenu;
        protected string modulo;
        //public DataSet 

        public ItemModulo(string titulo, string number, ref Color pBackColorOriginal, ref Color pBackColorEnter,
            ref Size pSizePanel, ref SolidBrush pBrushOriginal, ref SolidBrush pBrushEnter, ref Rectangle pRectangle,
            ref Pen pPenOriginal, ref Pen pPenEnter, ref Font pFontTitulo, ref Font pFontNumber, ref Point pPointTitulo,
            Action pbackAction, frmPrincipal.OutSelect<string, DataRow[]> psubModulos, string pModulo, Action<List<Panel>> pcargaMenu,
            frmPrincipal.OutSelect<string, DataRow[]> ppantallas, Action<string> pformAction)
            : base()
        {
            this.Number = number;
            this.Titulo = titulo;
            this.backColorOriginal = pBackColorOriginal;
            this.backColorEnter = pBackColorEnter;
            this.brushOriginal = pBrushOriginal;
            this.brushEnter = pBrushEnter;
            this.rectangle = pRectangle;
            this.penOriginal = pPenOriginal;
            this.penEnter = pPenEnter;
            this.auxColor = brushOriginal;
            this.auxPen = penOriginal;
            //
            this.fontTitulo = pFontTitulo;
            this.fontNumber = pFontNumber;
            //
            this.pointTitulo = pPointTitulo;

            this.BackColor = pBackColorOriginal;
            this.Size = pSizePanel;

            this.MouseEnter += new EventHandler(item_MouseEnter);
            this.MouseLeave += new EventHandler(item_MouseLeave);
            this.Dock = DockStyle.Top;
            this.backAction = pbackAction;
            this.formAction = pformAction;
            this.subModulos = psubModulos;
            this.pantallas = ppantallas;
            this.cargaMenu = pcargaMenu;
            modulo = pModulo;
            //
            this.MouseClick += new MouseEventHandler(MenuSlider_MouseClick);
        }
        //
        private DataRow[] SelectSubModulo(string select)
        {
            DataRow[] dr = null;
            this.subModulos(select, out dr);
            return dr;
        }
        //
        private DataRow[] SelectPantalla(string select)
        {
            DataRow[] dr = null;
            this.pantallas(select, out dr);
            return dr;
        }

        private void MenuSlider_MouseClick(object sender, MouseEventArgs e)
        {
            ((Panel)this.Parent).Controls.Clear();
            //
            List<Panel> paneles = new List<Panel>();
            DataRow[] drSubModulo = this.SelectSubModulo("modulo = " + modulo);
            foreach (DataRow dr in drSubModulo)
            {
                paneles.Add(FlyweightItemMenu.MakeItemSubModulo(Titulo+ " - "+ dr["descripcion"].ToString(), "", backAction));
                DataRow[] drPantallas = this.SelectPantalla("modulo = "+modulo+" AND submodulo = "+dr["submodulo"].ToString());
                foreach (DataRow drp in drPantallas)
                {
                    paneles.Add(FlyweightItemMenu.MakeItemPantalla(drp["descripcion"].ToString(), "", formAction));
                }
            }
            //
            if (paneles.Count > 0)
                this.cargaMenu(paneles);
            else
                this.backAction();
        }
        //
        private void item_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = backColorEnter;
            this.Cursor = Cursors.Hand;
            //
            this.auxColor = brushEnter;
            this.auxPen = penEnter;
        }
        //
        private void item_MouseLeave(object sender, EventArgs e)
        {
            this.BackColor = backColorOriginal;
            this.Cursor = Cursors.Default;
            //
            this.auxColor = brushOriginal;
            this.auxPen = penOriginal;
        }
        //
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //Draw text and number
            g.DrawString(this.Titulo, this.fontTitulo ,
                auxColor, this.pointTitulo);

            int posNX =  rectangle.Location.X + ((Number.Length > 1) ? 4 : 8);
            int posNY = rectangle.Location.Y + 5;
            
            if (this.pointNumber == null)
                this.pointNumber = new Point(posNX , posNY);
            else
            {
                this.pointNumber.X = posNX;
                this.pointNumber.Y = posNY;
            }
                //
            g.DrawString(this.Number, this.fontNumber,
                    auxColor, this.pointNumber);
            //Draw circle
            g.DrawEllipse(auxPen, rectangle);
        }

    }//Finaliza clase



    public class ItemSubModulo : Panel
    {
        protected String Number, Titulo;
        protected SolidBrush auxColor;
        protected Pen auxPen, penOriginal, penEnter;
        protected Rectangle rectangle;
        protected Color backColorOriginal, backColorEnter;
        protected SolidBrush brushOriginal, brushEnter;
        protected Font fontTitulo, fontNumber;
        protected Point pointTitulo;
        //
        //
        protected Point pointNumber;
        //
        protected Action backAction;

        public ItemSubModulo(string titulo, string number, ref Color pBackColorOriginal, ref Color pBackColorEnter,
            ref Size pSizePanel, ref SolidBrush pBrushOriginal, ref SolidBrush pBrushEnter, ref Rectangle pRectangle,
            ref Pen pPenOriginal, ref Pen pPenEnter, ref Font pFontTitulo, ref Font pFontNumber, ref Point pPointTitulo,
            Action pbackAction)
            : base()
        {
            this.Number = number;
            this.Titulo = titulo;
            this.backColorOriginal = pBackColorOriginal;
            this.backColorEnter = pBackColorEnter;
            this.brushOriginal = pBrushOriginal;
            this.brushEnter = pBrushEnter;
            this.rectangle = pRectangle;
            this.penOriginal = pPenOriginal;
            this.penEnter = pPenEnter;
            this.auxColor = brushOriginal;
            this.auxPen = penOriginal;
            //
            this.fontTitulo = pFontTitulo;
            this.fontNumber = pFontNumber;
            //
            this.pointTitulo = pPointTitulo;
            //
            

            this.BackColor = pBackColorOriginal;
            this.Size = pSizePanel;
            this.backAction = pbackAction;
            //

            this.MouseEnter += new EventHandler(item_MouseEnter);
            this.MouseLeave += new EventHandler(item_MouseLeave);
            this.Dock = DockStyle.Top;
            //
            this.MouseClick += new MouseEventHandler(MenuSlider_MouseClick);
        }

        private void MenuSlider_MouseClick(object sender, MouseEventArgs e)
        {
            ((Panel)this.Parent).Controls.Clear();
            this.backAction();
        }
        //
        private void item_MouseEnter(object sender, EventArgs e)
        {
            //this.BackColor = backColorEnter;
            this.Cursor = Cursors.Hand;
            //
            /*this.auxColor = brushEnter;
            this.auxPen = penEnter;*/
        }
        //
        private void item_MouseLeave(object sender, EventArgs e)
        {
            //this.BackColor = backColorOriginal;
            this.Cursor = Cursors.Default;
            //
            /*this.auxColor = brushOriginal;
            this.auxPen = penOriginal;*/
        }
        //
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //Draw text 
            g.DrawString(this.Titulo, this.fontTitulo,
                auxColor, this.pointTitulo);

            //Draw circle
            g.DrawEllipse(penEnter, rectangle);
            g.DrawString("<<", this.fontTitulo,
                auxColor, new Point(2, 9));
            g.DrawLine(auxPen, new Point(0, (this.Height - (int)auxPen.Width)), new Point(this.Width, (this.Height - (int)auxPen.Width)));
        }

    }//Finaliza clase



    public class ItemPantalla : Panel
    {
        protected String Number, Titulo;
        protected SolidBrush auxColor;
        protected Pen auxPen, penOriginal, penEnter;
        protected Rectangle rectangle;
        protected Color backColorOriginal, backColorEnter;
        protected SolidBrush brushOriginal, brushEnter;
        protected Font fontTitulo, fontNumber;
        protected Point pointTitulo;
        //
        //
        protected Point pointNumber;
        //
        protected Action<string> formAction;

        public ItemPantalla(string titulo, string number, ref Color pBackColorOriginal, ref Color pBackColorEnter,
            ref Size pSizePanel, ref SolidBrush pBrushOriginal, ref SolidBrush pBrushEnter, ref Rectangle pRectangle,
            ref Pen pPenOriginal, ref Pen pPenEnter, ref Font pFontTitulo, ref Font pFontNumber, ref Point pPointTitulo,
            Action<string> pformAction)
            : base()
        {
            this.Number = number;
            this.Titulo = titulo;
            this.backColorOriginal = pBackColorOriginal;
            this.backColorEnter = pBackColorEnter;
            this.brushOriginal = pBrushOriginal;
            this.brushEnter = pBrushEnter;
            this.rectangle = pRectangle;
            this.penOriginal = pPenOriginal;
            this.penEnter = pPenEnter;
            this.auxColor = brushOriginal;
            this.auxPen = penOriginal;
            //
            this.fontTitulo = pFontTitulo;
            this.fontNumber = pFontNumber;
            //
            this.pointTitulo = pPointTitulo;
            //


            this.BackColor = pBackColorOriginal;
            this.Size = pSizePanel;
            this.formAction = pformAction;
            //

            this.MouseEnter += new EventHandler(item_MouseEnter);
            this.MouseLeave += new EventHandler(item_MouseLeave);
            this.Dock = DockStyle.Top;
            //
            this.MouseClick += new MouseEventHandler(MenuSlider_MouseClick);
        }

        private void MenuSlider_MouseClick(object sender, MouseEventArgs e)
        {
            this.formAction("");
        }
        //
        private void item_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = backColorEnter;
            this.Cursor = Cursors.Hand;
            //
            this.auxColor = brushEnter;
            this.auxPen = penEnter;
        }
        //
        private void item_MouseLeave(object sender, EventArgs e)
        {
            this.BackColor = backColorOriginal;
            this.Cursor = Cursors.Default;
            //
            this.auxColor = brushOriginal;
            this.auxPen = penOriginal;
        }
        //
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //Draw text
            g.DrawString(this.Titulo, this.fontTitulo,
                auxColor, this.pointTitulo);
        }

    }//Finaliza clase

    public class FlyweightItemMenu
    {
        static Color colorOriginal = Color.Gray;
        static Color colorEnter = Color.White;//ColorTranslator.FromHtml("#E57E31");
        static Color backColorOriginal = Color.White;//ColorTranslator.FromHtml("#293541");
        static Color backColorEnter = Color.LightBlue;//ColorTranslator.FromHtml("#1F2B37");
        static Size sizePanel = new System.Drawing.Size(231, 40);
        static SolidBrush brushOriginal = new SolidBrush(colorOriginal);
        static SolidBrush brushEnter = new SolidBrush(colorEnter);
        static Rectangle rectangle = new Rectangle(15, 5, 30, 30);
        static Pen penOriginal = new Pen(colorOriginal, 2.5F);
        static Pen penEnter = new Pen(colorEnter, 2.5F);
        //
        static Font fontTitulo = new Font("Century Gothic", 12F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
        static Font fontNumber = new Font("Times New Roman", 12F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
        static Point pointTitulo = new Point(50, 9);
        

        //SUBMODULOS
        static Size sizeSubModulo = new System.Drawing.Size(231, 40);
        static Rectangle rectangleSubModulo = new Rectangle(2, 8, 20, 20);
        static Point pointTituloSubModulo = new Point(25, 10);
        static Font fontSubModulo = new Font("Century Gothic", 11F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
        static SolidBrush brushOriginalSubModulo = new SolidBrush(ColorTranslator.FromHtml("#85c2fd"));
        static Pen penOriginalSubModulo = new Pen(colorOriginal, 1F);
        static Pen penEnterSubModulo = new Pen(ColorTranslator.FromHtml("#85c2fd"), 1.5F);

        //PANTALLAS
        static Font fontPantalla = new Font("Century Gothic", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
        static Point pointTituloPantalla = new Point(25, 8);
        static Size sizePantalla = new System.Drawing.Size(231, 30);

        public static ItemModulo MakeItemModulo(string pTitulo, string pNumero, Action backAction, frmPrincipal.OutSelect<string, DataRow[]> subModulos,
            string pModulo, Action<List<Panel>> cargaMenu, frmPrincipal.OutSelect<string, DataRow[]> pantallas,
            Action<string> formAction)
        {
            return new ItemModulo(pTitulo, pNumero, ref backColorOriginal, ref backColorEnter, ref sizePanel, 
                ref brushOriginal, ref brushEnter, ref rectangle, ref penOriginal, ref penEnter, ref fontTitulo,
                ref fontNumber, ref pointTitulo, backAction, subModulos, pModulo, cargaMenu, pantallas, formAction);
        }

        public static ItemSubModulo MakeItemSubModulo(string pTitulo, string pNumero, Action backAction)
        {
            return new ItemSubModulo(pTitulo, pNumero, ref backColorOriginal, ref backColorEnter, ref sizeSubModulo,
                ref brushOriginalSubModulo, ref brushEnter, ref rectangleSubModulo, ref penOriginalSubModulo, ref penEnterSubModulo, ref fontSubModulo,
                ref fontNumber, ref pointTituloSubModulo, backAction);
        }

        public static ItemPantalla MakeItemPantalla(string pTitulo, string pNumero, Action<string> formAction)
        {
            return new ItemPantalla(pTitulo, pNumero, ref backColorOriginal, ref backColorEnter, ref sizePantalla,
                ref brushOriginal, ref brushEnter, ref rectangleSubModulo, ref penOriginalSubModulo, ref penEnterSubModulo, ref fontPantalla,
                ref fontNumber, ref pointTituloPantalla, formAction);
        }
    }

    
}//Finaliza namespace